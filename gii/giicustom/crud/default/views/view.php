<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = <?= "Yii::t('app',Yii::\$app->params['text.view']); "; ?>
$this->params['breadcrumbs'][] = ['label' => <?= strtolower($generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass))))) ?>, 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->params['MenuModule'] = [
[
'label' => Yii::t('app',Yii::$app->params['text.admin']) . ' '.<?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, 
'url' => ['index'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'],'index')
],    
[
'label' => Yii::t('app',Yii::$app->params['text.create']) . ' '.<?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, 
'url' => ['create'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'],'create')
],
[
'label' => Yii::t('app',Yii::$app->params['text.update']) . ' '.<?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, 
'url' => ['update',<?= $urlParams ?>],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'],'update')
]
];

?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-view  panel panel-default">

    <div class="panel-heading navbar-tool">
        <h3 class="panel-title"><?= "<?= " ?>Html::encode($this->title) ?></h3>
        <div class="menu-tool">
            <div class="menu-items">
                <?= "<?= " ?> \app\models\MenuMain::menu(); ?>
            </div>
        </div>          
    </div>
    <div class="panel-body">
        <?= "<?= " ?>DetailView::widget([
        'model' => $model,
        'attributes' => [
        <?php
        if (($tableSchema = $generator->getTableSchema()) === false) {
            foreach ($generator->getColumnNames() as $name) {
                echo "            '" . $name . "',\n";
            }
        } else {
            foreach ($generator->getTableSchema()->columns as $column) {

                $attr_admin = ['fecha_creado', 'fecha_editado', 'fecha_eliminado', 'creado_por', 'editado_por', 'eliminado_por'];
                
                $format = $generator->generateColumnFormat($column);

                $row_custom = false;
                $row_attribute = '';
                $row_format = '';
                $row_value = '';
                if ($format === 'text') {
                    
                } elseif ($format === 'datetime') {
                    $row_custom = true;
                    $row_attribute = $column->name;
                    $row_format = 'datetime';
                } elseif ($column->name === 'estatus') {
                    $row_custom = true;
                    $row_attribute = $column->name;
                    $row_format = 'raw';
                    $row_value = '$model->getActive()';
                }

                if ($row_custom) {
                    echo "[\n" .
                    ($row_attribute ? "'attribute' => '" . $row_attribute . "',\n" : "") .
                    ($row_value ? "'value' =>" . $row_value . ",\n" : "") .
                    ($row_format ? "'format' => '" . $row_format . "',\n" : "") .
                    "],\n";
                } else {
                    if ($column->isPrimaryKey) {
                        
                    } elseif ($column->name == 'estatus') {
                        echo "['attribute' => '".$column->name."', 'format' => 'raw','value' => \$model->getActive()],\n";
                        
                    } elseif(in_array($column->name, $attr_admin)){
                        echo "['attribute' => '".$column->name."', 'visible' => Yii::\$app->params['isAdmin']],\n";
                        
                        } else {
                            
                        echo "['attribute' => '" . $column->name . "'],\n";
                    }
                }
            }
        }
        ?>
        ],
        ]) ?>
    </div>
</div>