<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = <?= "Yii::t('app',Yii::\$app->params['text.update']);" ?>
$this->params['breadcrumbs'][] = ['label' => <?= strtolower($generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass))))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model-><?= $generator->getNameAttribute() ?>, 'url' => ['view', <?= $urlParams ?>]];
$this->params['breadcrumbs'][] = <?= "Yii::t('app',Yii::\$app->params['text.update'])" //$generator->generateString('Update') ?>;

Yii::$app->params['MenuModule'] = [
[
'label' => Yii::t('app',Yii::$app->params['text.admin']) . ' '.<?= strtolower($generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 
'url' => ['index'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'index')
],    
[
'label' => Yii::t('app',Yii::$app->params['text.create']) . ' '.<?= strtolower($generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 
'url' => ['create'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'create')
],
[
'label' => Yii::t('app',Yii::$app->params['text.view']) . ' '.<?= strtolower($generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 
'url' => ['view',<?= $urlParams ?>],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'view')
]
];
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-update panel panel-default">
  <div class="panel-heading navbar-tool">
        <h3 class="panel-title"><?= "<?= " ?>Html::encode($this->title) ?></h3>
        <div class="menu-tool">
                <div class="menu-items">
                   <?= "<?= " ?> \app\models\MenuMain::menu(); ?>
                </div>
            </div>          
    </div>
  <div class="panel-body">


    <?= "<?= " ?>$this->render('_form', [
        'model' => $model,
    ]) ?>

    </div>
</div>