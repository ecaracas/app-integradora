<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use <?= $generator->indexWidgetType === 'grid' ? "yii\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= "Yii::t('app',Yii::\$app->params['text.admin']) . ' ' . " . strtolower($generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass))))) ?>;
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->params['MenuModule'] = [
[
'label' => Yii::t('app',Yii::$app->params['text.create']) . ' '.<?= strtolower($generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 
'url' => ['create'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'create')
]
];
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index panel panel-default">

    <div class="panel-heading navbar-tool">
        <h3 class="panel-title"><?= "<?= " ?>Html::encode($this->title) ?></h3>
        <div class="menu-tool">
            <div class="menu-items">
                <?= "<?= " ?> \app\models\MenuMain::menu(); ?>
            </div>
        </div>          
    </div>
    <div class="panel-body">
        <?php if (!empty($generator->searchModelClass)): ?>
            <?= "    <?php " . ($generator->indexWidgetType === 'grid' ? "// " : "") ?>echo $this->render('_search', ['model' => $searchModel]); ?>
        <?php endif; ?>

        <?= $generator->enablePjax ? '<?php Pjax::begin(); ?>' : '' ?>
        <?php if ($generator->indexWidgetType === 'grid'): ?>
            <?= "<?= " ?>GridView::widget([
            'dataProvider' => $dataProvider,
            <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n        'columns' => [\n" : "'columns' => [\n"; ?>
            // ['class' => 'yii\grid\SerialColumn'],

            <?php
            $count = 0;
            if (($tableSchema = $generator->getTableSchema()) === false) {
                foreach ($generator->getColumnNames() as $name) {
                    if (++$count < 6) {
                        echo "            '" . $name . "',\n";
                    } else {
                        echo "            // '" . $name . "',\n";
                    }
                }
            } else {
                $attr_admin = ['fecha_creado', 'fecha_editado', 'fecha_eliminado', 'creado_por', 'editado_por', 'eliminado_por'];
                
                foreach ($tableSchema->columns as $column) {
                    
                    if(!$column->isPrimaryKey){
                    
                    $format = $generator->generateColumnFormat($column);                   
                    
                    
                    

                        if ($column->name == 'estatus') {
                            echo " [
                        'attribute' => '".$column->name."',
                        'filter' => \$searchModel->getStatusList(),
                        'format' => 'raw',
                        'value' => function(\$model) {
                            return \$model->getActive();
                        }
                    ],";
                        } elseif(in_array($column->name, $attr_admin)){                             
                            
                        
                        } else {
                            echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                        }
                   
                }
            }
            }
            ?>

            // ['class' => 'yii\grid\ActionColumn'],


            ['class' => 'yii\grid\DataColumn',
            'header' => Yii::t('app', Yii::$app->params['text.view']),
            'format' => 'raw',
            'visible' => Yii::$app->auth->check(Yii::$app->params['module'] , 'view'),
            'value' => function ($data) {
            return Html::a(Yii::$app->params['icon.view'], ['view', 'id' => $data-><?= $tableSchema->primaryKey[0] ?>], ['class' =>Yii::$app->params['btn.view']]);
            },
            ],
            ['class' => 'yii\grid\DataColumn',
            'header' => Yii::t('app', Yii::$app->params['text.update']),
            'format' => 'raw',
            'visible' => Yii::$app->auth->check(Yii::$app->params['module'] , 'update'),
            'value' => function ($data) {
            return Html::a(Yii::$app->params['icon.update'], ['update', 'id' => $data-><?= $tableSchema->primaryKey[0] ?>], ['class' => Yii::$app->params['btn.update']]);
            },
            ],
            ['class' => 'yii\grid\DataColumn',
            'header' => Yii::t('app', Yii::$app->params['text.delete']),
            'format' => 'raw',
            'visible' => Yii::$app->auth->check(Yii::$app->params['module'] , 'delete'),
            'value' => function ($data) {
            return Html::a(Yii::$app->params['icon.delete'], ['delete', 'id' => $data-><?= $tableSchema->primaryKey[0] ?>], ['class' =>'option-delete '. Yii::$app->params['btn.delete']]);
            },
            ],







            ],
            ]); ?>
            
            
   <?= "    <?php "  ?>      
if (Yii::$app->auth->check(Yii::$app->params['module'], 'delete')) {
    \Yii::$app->params['show.modal.delete'] = true;
    \Yii::$app->params['show.modal.confirm'] = true;
}
?>
            
            
        <?php else: ?>
            <?= "<?= " ?>ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'item'],
            'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
            },
            ]) ?>
        <?php endif; ?>
        <?= $generator->enablePjax ? '<?php Pjax::end(); ?>' : '' ?>
    </div>
</div>