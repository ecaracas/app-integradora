<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

    <?= "<?php " ?>$form = ActiveForm::begin(); ?>

    <?php
    $attr_admin = ['fecha_creado', 'fecha_editado', 'fecha_eliminado', 'creado_por', 'editado_por', 'eliminado_por'];
    foreach ($generator->getColumnNames() as $attribute) {
        if (in_array($attribute, $safeAttributes)) {

            if (!in_array($attribute, $attr_admin)) {
                echo "    <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
            } 
        }
    }
    ?>
    <div class="form-group">
        <?= "<?= " ?>Html::submitButton(Yii::t('app',Yii::$app->params['text.save']), ['class' => Yii::$app->params['btn.save']]) ?> &nbsp;
        <?= "<?= " ?>Html::a(Yii::t('app', Yii::$app->params['text.cancel']), ['index'], ['class' => Yii::$app->params['btn.cancel']]) ?>
    </div>

    <?= "<?php " ?>ActiveForm::end(); ?>

</div>
