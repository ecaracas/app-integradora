<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\data\ActiveDataProvider;
use yii\web\Response;
use yii\web\HttpException;
use yii\helpers\Url;
use app\models\ChangePassword;
use app\modules\accesos\models\RBACUsuarios;
use app\models\FormRecoverPass;
use yii\web\Session;


class SiteController extends \app\components\CController
{
   

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','view','create','update','delete'],
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete',],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['get'],
                    'view' => ['get'],
                    'create' => ['get', 'post'],
                    'update' => ['get', 'put', 'post'],
                    'delete' => ['post', 'delete'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex() {
        $this->layout = 'complete/main';
        self::logNavegacion();
        if (Yii::$app->user->isGuest) {
            $this->redirect('login');
        } else {
            return $this->render('index');
        }

    }
    public function actionLogin() {
        
       
        $this->layout = 'login/login';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            self::logNavegacion();
            return $this->goHome();
        }
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    public function actionLogout() {
        self::logNavegacion();
        Yii::$app->user->logout();
        //$this->redirect('site/login');
        return $this->goHome();
    }

    public function actionCambiar_contrasena() {

        if (Yii::$app->user->isGuest) {
            $this->redirect('login');
        }
        $this->layout = 'complete/main';
        Yii::$app->params['layout.title'] = Yii::t('app', 'myaccount');
        $model = new ChangePassword();
        if ($model->load(Yii::$app->request->post())) {
            if($model->validate()) {
                
                $user = RBACUsuarios::find()->where('id_rbac_usuario='.Yii::$app->user->identity->id_rbac_usuario)->one();
                $user->contrasena = Yii::$app->getSecurity()->generatePasswordHash($model->password_new);
                if($user->save()){
                    Yii::$app->session->setFlash('success', Yii::t('app', Yii::$app->params['message.success']));
                }else{               
                    Yii::$app->session->setFlash('warning', Yii::t('app',Yii::$app->params['message.warning']));
                }
                return $this->redirect('perfil');
            } else {
                Yii::$app->session->setFlash('warning', Yii::t('app',Yii::$app->params['message.warning']));
            }
        }
        return $this->render('change_password', [
                    'model' => $model,
        ]);
    }

    


    public function actionPerfil() {
        self::logNavegacion();
        $this->layout = 'complete/main';

        if (Yii::$app->user->isGuest) {
            $this->redirect('login');
        } else {
            Yii::$app->params['layout.title'] = Yii::t('app', 'myaccount');
            $model = \app\modules\accesos\models\RBACUsuarios::find()->where(['id_rbac_usuario'=>\Yii::$app->user->identity->id_rbac_usuario])->one();

            return $this->render('perfil', [
                        'model' => $model,
            ]);

        }
    }

    public function actionError()
    {
        if (Yii::$app->user->isGuest) {
        }
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception]);
        }
    }
}
