<?php
namespace app\models;
use yii;
use yii\base\Model;
use yii\widgets\Menu;

class MenuMain extends yii\base\Model {

    public static function menu() {

        return Menu::widget([
            'items' => Yii::$app->params['MenuModule'],
            'activeCssClass' => 'active',
            'encodeLabels' => false,
            'options' => array('class' => 'cmenuhorizontal', 'id' => 'menu-main'),
             'linkTemplate' => '<a href="{url}" class="btn btn-primary btn-sm">{label}</a>'
        ]);
    }

}
