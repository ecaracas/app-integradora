<?php

namespace app\models;

use Yii;

class User extends \app\components\CActiveRecord implements \yii\web\IdentityInterface {

    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    public static function tableName() {
        return 'rbac_usuarios';
    }

    public static function getDb() {
        return Yii::$app->db2;
    }

    public static function findIdentity($id) {

         return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {

        return static::findOne(['access_token' => $token]);

    }

    public static function findByUsername($username) {

        return static::findOne(['nombreusuario' => $username]);
        
    }

    public function getId() {
        return $this->getPrimaryKey();
    }

    public function getAuthKey() {
        return $this->authKey;
    }

    public function validateAuthKey($authKey) {
        return $this->authKey === $authKey;
    }

    public function validatePassword($password) {
        return \Yii::$app->getSecurity()->validatePassword($password,$this->contrasena);
    }

    public function getNameFull(){
        return $this->nombre.' '.$this->apellido;
    }

}
