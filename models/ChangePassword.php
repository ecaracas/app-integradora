<?php

namespace app\models;

use Yii;
use yii\base\Model;


class ChangePassword extends Model
{
    public $password;
    public $password_new;
    public $password_repeat;


    private $_user;


    public function rules()
    {
        return [
            [['password_repeat','password_new', 'password'], 'required'],
            ['password_new', 'string', 'min' => 8,'tooShort' => Yii::t('app','password_character')],
            ['password_repeat', 'compare', 'compareAttribute'=>'password_new', 'skipOnEmpty' => false, 'message'=>Yii::t('app','password_equal')],
            ['password', 'validatePassword'],
            ['password_new', 'validateSecurity'],
        ];
    }

    public function attributeLabels() {
        return [
            'password' => yii::t('app', 'password'),
            'password_new' => yii::t('app', 'new_') . ' ' .yii::t('app', 'password'),
            'password_repeat' => yii::t('app', 'confirm_password'),
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if ($user) {
                if (!$user->validatePassword($this->password)) {
                    $this->addError('password', yii::t('app', 'password_invalid'));
                }
            }
        }
    }
    
    public function validateSecurity($attribute, $params)
    {
        if ( ! preg_match('/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/', $this->$attribute) ) {
            $this->addError($attribute, Yii::t('app','password_insecure')); // Listo
        }
    }
    
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(['nombreusuario' => Yii::$app->user->identity->nombreusuario]);
        }
        return $this->_user;
    }
    
    public function savePassword()
    {
        $model = \app\modules\accesos\models\RBACUsuarios::find(Yii::$app->user->identity->id_rbac_usuario)->one();
        $model->setAttribute('contrasena', \Yii::$app->getSecurity()->generatePasswordHash($this->password_new));
        return $model->save();
    }
}
