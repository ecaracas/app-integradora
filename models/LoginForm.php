<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user;


    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['username'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['username' => 'nombreusuario'],'message' => yii::t('app', 'incorrect_username_password')],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels() {
        return [
            'username' => yii::t('app', 'Usuario'),
            'password' => yii::t('app', 'Contraseña'),
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if ($user) {
                if (!$user->validatePassword($this->password)) {
                    $this->addError($attribute, yii::t('app', 'Usuario').' o '.yii::t('app', 'Contraseña').' Incorrecta');
                }
            }
        }
    }

    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(['nombreusuario' => $this->username]);
        }
        return $this->_user;
    }
}
