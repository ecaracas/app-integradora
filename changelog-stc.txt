# Changelog
	Todos los cambios notables en este proyecto SGEF se documentarán en este archivo.
	
## [DWEB-PORTAL-STC-VE-1.1.1 20181121] 
### Changed
-eliminado modulo de firmware
-Cambiado GPRS por Protocolo TXT
-Cambiar todos los descriptores que hagan referencia al IMOBILE y sustituir por Protocolo XML y GPRS por Protocolo TXT

### Added
-Se agregan en los servicios la recepcion de los nuevos campos de la providencia 1401 para gprs, y se agrega campo faltante para imobile de ultimo numero de rmf 