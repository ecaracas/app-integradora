<?php


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


Yii::$app->params['MenuModule'] = [
    [
        'label' => '<i class="fa fa-user"></i> ' . Yii::t('app','myaccount'),
        'encodeLabels' => false,
        'url' => ['site/perfil'],
    ],    
];
?>
<div class="col-lg-6">
    <section class="box nobox">
            <div class="content-body">    
                <div class="row">
                    <div class="col-md-12" style="padding: 0 15px">
                        <div class="usuario-perfil panel panel-default">
                            <div class="panel-heading navbar-tool">
                                <h3 class="panel-title"><?= Yii::t('app','change_password') ?></h3>
                                <div class="menu-tool">
                                    <div class="menu-items">
                                        <?=  \app\models\MenuMain::menu(); ?>
                                    </div>
                                </div>          
                            </div>
                            <div class="panel-body">



                                        <?php $form = ActiveForm::begin(['id' => 'changepassword-form']); ?>

                                            <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>

                                            <?= $form->field($model, 'password_new')->passwordInput() ?>

                                            <?= $form->field($model, 'password_repeat')->passwordInput() ?>

                                            <div class="form-group">
                                                <?= Html::submitButton(Yii::t('app',Yii::$app->params['text.save']), ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                                            </div>

                                        <?php ActiveForm::end(); ?>

                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
<div class="col-lg-6">
    <section class="box nobox">
            <div class="content-body">    
                <div class="row">
                    <div class="col-md-12" style="padding: 0 15px">
                        <div class="usuario-perfil panel panel-default">
                            <div class="panel-heading navbar-tool">
                                <h3 class="panel-title"><?= Yii::t('app','security_advice') ?></h3>         
                            </div>
                            <div class="panel-body">
                                <div class="alert alert-info alert-dismissible fade in">
                                    <p class="text-left"><?= Yii::t('app','password_character') ?></p>
                                    <p class="text-left"><?= Yii::t('app','password_letter') ?></p>
                                    <p class="text-left"><?= Yii::t('app','password_number') ?></p>
                                </div>
                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
