<?php 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

echo '<h1>Módulos Principales</h1>';
?>
<div class="col-lg-12">
    <section class="box nobox">
            <div class="content-body">    
                <div class="row">
                    <div class="col-md-12" >
                        <div class="usuario-perfil panel panel-default">
                            
                                <h3 class="panel-title"><?= Yii::t('app','title_home') ?></h3>
                                <div class="menu-tool">
                                    <div class="menu-items">
                                        <?=  \app\models\MenuMain::menu(); ?>

                                    </div>
                                </div>          
                           
                            <div class="panel-body">
								<div class="col-lg-6 col-xs-6">
								          <div class="small-box bg-default">
								            <div class="inner">

								              <p class="ico-home"><?= Yii::t('app','title_maquina') ?></p>
								            </div>
								            <div class="icon">
								              <i class="g glyphicon glyphicon-hdd"></i>
								            </div>
								            <a href="../admin/maquina/create" class="small-box-footer">Registrar Nueva <i class="fa fa-arrow-circle-right"></i></a>
								          </div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>