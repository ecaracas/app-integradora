<?php


use yii\helpers\Html;
$this->title = @$exception->statusCode;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <section class="box nobox ">
                <div class="content-body">    
                    <div class="row">
                        <div class="col-xs-12">

                            <h1 class="page_error_code text-primary"><?= Html::encode($this->title) ?></h1>
                            <h1 class="page_error_info"><?= nl2br(Html::encode($exception->getMessage())) ?></h1>
                             <div class="row">
                                <div class="col-md-offset-3 col-sm-offset-3 col-xs-offset-2 col-xs-8 col-sm-6">
                                    <form action="javascript:;" method="post" class="page_error_search">
                                        <div class="text-center page_error_btn">
                                            <a class="btn btn-primary btn-lg" href='<?php echo Yii::getAlias('@web/') ?>'><i class='fa fa-location-arrow'></i> &nbsp; Inicio</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>