<?php


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

 <div class="login-wrapper row">
        <div id="login" class="login loginpage col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-xs-offset-0 col-xs-12 col-sm-6 col-lg-4">
            <h1><a href="#" title="Login Page" tabindex="-1"></a></h1>

          <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'fieldConfig' => [
            'labelOptions' => ['class' => ''],
        ],
    ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class'=>'input']) ?>

        <?= $form->field($model, 'password')->passwordInput(['class'=>'input']) ?>


        <div class="form-group">
            <div >
                <?= Html::submitButton('Ingresar', ['class' => 'btn btn-accent btn-block', 'name' => 'login-button', 'id'=>'wp-submit']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
              


        </div>
    </div>

 

   
</div>
