<?php

use yii\helpers\Html;


Yii::$app->params['MenuModule'] = [
    [
        'label' => '<i class="fa fa-key"></i> ' . Yii::t('app','change_password'),
        'encodeLabels' => false,
        'url' => ['site/cambiar_contrasena'],
    ],
];
?>

<div class="col-lg-12">
    <section class="box nobox">
        <div class="content-body">
            <div class="row">
                <div class="col-md-12" style="padding: 0 15px">
                    <div class="usuario-perfil panel panel-default">
                        <div class="panel-heading navbar-tool">
                            <h3 class="panel-title"><?= Yii::t('app','profile') ?></h3>
                            <div class="menu-tool">
                                <div class="menu-items">
                                    <?=  \app\models\MenuMain::menu(); ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <th rowspan="7" width="35%" style="border-top: none;">
                                        <div class="uprofile-image">
                                            <img alt="" src="<?= Yii::getAlias('@web/images/avatar1.jpg') ?>" class="img-responsive">
                                        </div>
                                        <div class="uprofile-name">
                                            <h3>
                                                <a href="#"><?= $model->nombre?> <?= $model->apellido?></a>
                                                <span class="uprofile-status online"></span>
                                            </h3>
                                            <p class="uprofile-title"><?= $model->rol->descripcion?></p>
                                        </div>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="border-top: none;"><?= Yii::t('app','username') ?></th>
                                    <td style="border-top: none;"><?= $model->nombreusuario ?></td>
                                </tr>
                                <tr>
                                    <th style="border-top: none;"><?= Yii::t('app','last_login') ?></th>
                                    <td style="border-top: none;">
                                        <?php $fecha = $model->lastlogin_time == null ? Yii::$app->session['lastlogin'] :  $model->lastlogin_time;?>
                                        <?= Yii::$app->formatter->asDate($fecha, Yii::$app->params['format.date']) ?> a las
                                        <?= Yii::$app->formatter->asDate($fecha. ' ' .Yii::$app->getTimeZone() ,'php:h:iA'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th style="border-top: none;"><?= Yii::t('app','name') ?> <?= Yii::t('app','last_name') ?></th>
                                    <td style="border-top: none;"><?= $model->nombre ?> <?= $model->apellido ?></td>
                                </tr>
                                <tr>
                                    <th style="border-top: none;"><?= Yii::t('app','dni_number') ?>/<?= Yii::t('app','tax_id_number') ?></th>
                                    <td style="border-top: none;"><?= $model->dni_letra ?>-<?= $model->dni_numero ?></td>
                                </tr>
                                <tr>
                                    <th style="border-top: none;"><?= Yii::t('app','email') ?></th>
                                    <td style="border-top: none;"><?= $model->email ?></td>
                                </tr>
                                <tr>
                                    <th style="border-top: none;"><?= Yii::t('app','phone') ?></th>
                                    <td style="border-top: none;"><?= $model->telf_local_numero ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
