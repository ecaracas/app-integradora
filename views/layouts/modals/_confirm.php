<?php use yii\helpers\Html; ?>
<?php if (\Yii::$app->params['show.modal.confirm']) { ?>
    <div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirmar</h4>
                </div>
                <div class="modal-body">
                    <p></p>
                </div>
                <div class="modal-footer">
                    <?=
                    Html::beginForm("#", "post", ['id' => "form-confirm"]) .
                    Html::submitButton(
                            Yii::t('app', Yii::$app->params['icon.save']) . ' ' .
                            Yii::t('app', Yii::$app->params['text.ok']), ['class' => Yii::$app->params['btn.save'], 'id' => 'btn-confirm-success'])
                    . "&nbsp;" .
                    Html::submitButton(
                            Yii::t('app', Yii::$app->params['icon.cancel']) . ' ' .
                            Yii::t('app', Yii::$app->params['text.cancel']), ['class' => Yii::$app->params['btn.cancel'], "data-dismiss" => "modal"]) .
                    Html::endForm()
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>