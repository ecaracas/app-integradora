<!DOCTYPE html>
<html class=" ">
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <title>Integracion Intranet Extranet</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <link rel="shortcut icon" href="<?php echo Yii::getAlias('@web/images/favicon.ico" type="image/x-icon')?>" />   
        <link rel="apple-touch-icon-precomposed" href="<?php echo Yii::getAlias('@web/images/apple-touch-icon-57-precomposed.png')?>">  
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo Yii::getAlias('@web/images/apple-touch-icon-114-precomposed.png')?>">    
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo Yii::getAlias('@web/images/apple-touch-icon-72-precomposed.png')?>">    
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo Yii::getAlias('@web/images/apple-touch-icon-144-precomposed.png')?>">   




        <link href="<?php echo Yii::getAlias('@web/plugins/pace/pace-theme-flash.css')?>" rel="stylesheet" type="text/css" media="screen"/>
        <link href="<?php echo Yii::getAlias('@web/plugins/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo Yii::getAlias('@web/plugins/bootstrap/css/bootstrap-theme.min.css')?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo Yii::getAlias('@web/fonts/font-awesome/css/font-awesome.css')?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo Yii::getAlias('@web/css/animate.min.css" rel="stylesheet')?>" type="text/css"/>
        <link href="<?php echo Yii::getAlias('@web/plugins/perfect-scrollbar/perfect-scrollbar.css')?>" rel="stylesheet" type="text/css"/>
      
        
        
<link href="<?php echo Yii::getAlias('@web/plugins/icheck/skins/all.css')?>" rel="stylesheet" type="text/css" media="screen"/>

        <link href="<?php echo Yii::getAlias('@web/css/style.css')?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo Yii::getAlias('@web/css/responsive.css')?>" rel="stylesheet" type="text/css"/>


    </head>
    <body class=" login_page">

<div class="container-fluid">
       <?= $content ?>
</div>


<script src="<?php echo Yii::getAlias('@web/js/jquery-1.11.2.min.js')?>" type="text/javascript"></script> 
<script src="<?php echo Yii::getAlias('@web/js/jquery.easing.min.js')?>" type="text/javascript"></script> 
<script src="<?php echo Yii::getAlias('@web/plugins/bootstrap/js/bootstrap.min.js')?>" type="text/javascript"></script> 
<script src="<?php echo Yii::getAlias('@web/plugins/pace/pace.min.js')?>" type="text/javascript"></script>  
<script src="<?php echo Yii::getAlias('@web/plugins/perfect-scrollbar/perfect-scrollbar.min.js')?>" type="text/javascript"></script> 
<script src="<?php echo Yii::getAlias('@web/plugins/viewport/viewportchecker.js')?>" type="text/javascript"></script>  
<script>window.jQuery||document.write('<script src="<?php echo Yii::getAlias('@web/js/jquery-1.11.2.min.js')?>"><\/script>');</script>


<script src="<?php echo Yii::getAlias('@web/plugins/icheck/icheck.min.js')?>" type="text/javascript"></script>



<script src="<?php echo Yii::getAlias('@web/js/scripts.js')?>" type="text/javascript"></script> 


<div class="modal" id="section-settings" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog animated bounceInDown">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Section Settings</h4>
            </div>
            <div class="modal-body">


            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                <button class="btn btn-success" type="button">Save changes</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>