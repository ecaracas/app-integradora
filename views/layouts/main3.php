<?php


use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<script type="text/javascript" src="../../js/verific.js"></script>-->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'ITAX',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Inicio', 'url' => ['/site/index'],'visible'=>Yii::$app->user->isGuest ?false:true],
            ['label' => 'Reportes Z', 'url' => ['/reports/taxpayers'],'visible'=>Yii::$app->user->isGuest ?false:true],
                       ['label' => 'Configuracion', 'url' => ['/configuration/configuration/'],'visible'=>Yii::$app->user->isGuest ?false:true],
            ['label' => 'Registrar Protocolo XML', 'url' => ['/recordimobile/contimobile'],'visible'=>Yii::$app->user->isGuest ?false:true],
            ['label' => 'Registrar Maquina', 'url' => ['/recordmachine/contmachine'],'visible'=>Yii::$app->user->isGuest ?false:true],
            ['label' => 'Logs', 'url' => ['/logs/operaciones'],'visible'=>Yii::$app->user->isGuest ?false:true],
            Yii::$app->user->isGuest ? (
                ['label' => 'Ingregar', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Salir (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
