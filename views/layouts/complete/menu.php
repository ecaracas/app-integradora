

<ul class="wraplist" style="min-height: 92%;height: auto;">	

    <li class='menusection'>Menú </li>

    <li class=""> 
        <a href="javascript:;">
            <i class="fa fa-spinner"></i>
            <span class="title">Logs Protocolo TXT</span>
            <span class="arrow "></span>                                
        </a>
        <ul class="sub-menu" >
            <li>
                <a class="<?php echo \Yii::$app->controller->id == 'logpartes' ? 'active' : '' ?>" href="<?php echo Yii::getAlias('@web/logpartes/') ?>"  >Paquetes Recibidos</a>
            </li>
            <li>
                <a class="<?php echo \Yii::$app->controller->id == 'log' ? 'active' : '' ?>" href="<?php echo Yii::getAlias('@web/log/') ?>" >Paquetes Descifrados</a>
            </li>
            <li>
                <a class="<?php echo \Yii::$app->controller->id == 'logerror' ? 'active' : '' ?>" href="<?php echo Yii::getAlias('@web/logerror/') ?>"  >Paquetes Con Error</a>
            </li>



        </ul>
    </li>

    <li class=""> 
        <a href="javascript:;">
            <i class="fa fa-spinner"></i>
            <span class="title">Logs Protocolo XML</span>
            <span class="arrow "></span>                                
        </a>
        <ul class="sub-menu" >
            <li>
                <a class="<?php echo \Yii::$app->controller->id == 'logoperacion' ? 'active' : '' ?>" href="<?php echo Yii::getAlias('@web/logs/logoperacion') ?>"  >Operaciones Protocolo XML</a>


            </li>


        </ul>
    </li>
    <li class="<?php echo \Yii::$app->controller->id == 'reportez' ? 'open' : '' ?>"> 
        <a href="<?php echo Yii::getAlias('@web/reportez') ?>">
            <i class="fa fa-file-text"></i>
            <span class="title">Reportes Z</span>
        </a>
    </li>

    <li class="<?php echo \Yii::$app->controller->id == 'transacciones' ? 'open' : '' ?>"> 
        <a href="<?php echo Yii::getAlias('@web/transacciones') ?>">
            <i class="fa fa-arrows-h"></i>
            <span class="title">Transacciones</span>
        </a>
    </li>

    <li class="<?php echo \Yii::$app->controller->id == 'peticiones' ? 'open' : '' ?>"> 
        <a href="<?php echo Yii::getAlias('@web/peticiones/peticiones/index') ?>">
            <i class="fa fa-exchange"></i>
            <span class="title"><?= Yii::t('app', 'requests'); ?></span>

        </a>

    </li>


    

    <li class=""> 
        <a href="javascript:;">
            <i class="fa fa-suitcase"></i>
            <span class="title">Administración</span>
            <span class="arrow "></span>                                
        </a>
        <ul class="sub-menu" >
           
            <li>
                <a class="<?php echo \Yii::$app->controller->id == 'configuracion' ? 'active' : '' ?>" href="<?php echo Yii::getAlias('@web/configuracion/configuracion/index') ?>"  >Configuración Protocolo XML</a>
            </li>

            <li>
                <a class="<?php echo \Yii::$app->controller->id == 'contribuyentes' ? 'active' : '' ?>" href="<?php echo Yii::getAlias('@web/contribuyentes/contribuyentes/') ?>" >Contribuyentes</a>
            </li>
            
             <li>
                <a class="<?php echo \Yii::$app->controller->id == 'firmware' ? 'active' : '' ?>" href="<?php echo Yii::getAlias('@web/version/firmware/index') ?>"  >Firmware</a>
            </li>            
             
            <li>
                <a class="<?php echo \Yii::$app->controller->id == 'imobiles' ? 'active' : '' ?>" href="<?php echo Yii::getAlias('@web/admin/imobiles/index') ?>"  >Protocolo XML</a>
            </li>

            <li>
                <a class="<?php echo \Yii::$app->controller->id == 'maquina' ? 'active' : '' ?>" href="<?php echo Yii::getAlias('@web/admin/maquina/') ?>"  >Maquinas Fiscales</a>
            </li>

            <li>
                <a class="<?php echo \Yii::$app->controller->id == 'sucursales' ? 'active' : '' ?>" href="<?php echo Yii::getAlias('@web/sucursales/sucursales/') ?>"  >Sucursales</a>
            </li>
            
             
    

        </ul>
    </li>


















</ul>
