<?php 
use yii\helpers\Html;
use app\modules\accesos\models\RBACMenus;
?>
<?php
    $menuSQL = RBACMenus::find()
                    ->where([
                        'categoria' => 'main_user',
                    ])
                    ->orderBy(['jerarquia' => SORT_ASC, 'orden' => SORT_ASC])
                    ->all();
    $menus = [];
    foreach ($menuSQL as $m) {
        $menus[$m->id_rbac_menu]['data'] = NULL;
        $menus[$m->id_rbac_menu]['menu_id'] = $m->id_rbac_menu;
        $menus[$m->id_rbac_menu]['menu_descripcion'] = $m->descripcion;
        $menus[$m->id_rbac_menu]['menu_url'] = $m->url;
        $menus[$m->id_rbac_menu]['menu_url_tipo'] = $m->url_tipo;
        $menus[$m->id_rbac_menu]['menu_icono'] = $m->icono;
        $menus[$m->id_rbac_menu]['menu_modulo'] = $m->modulo;
        $menus[$m->id_rbac_menu]['menu_jerarquia'] = $m->jerarquia;
    }
    $contador = count($menus);
    $i = 1;
?>
<ul class="dropdown-menu profile animated fadeIn">
    <?php foreach ($menus as $m) : ?>
    <li <?= $contador == $i ? 'class="last"': ''; ?>>
        <?= Html::a($m['menu_icono'] . Yii::t('app',$m['menu_descripcion']), Yii::getAlias('@web/'.$m['menu_url'])) ?>
    </li>
    <?php $i++;?>
    <?php endforeach; ?>
</ul>