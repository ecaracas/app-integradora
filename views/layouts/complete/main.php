<?php

use app\assets\AppAsset;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;

AppAsset::register($this);
$this->beginPage()
?>
<!DOCTYPE html>
<html class=" ">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title><?= Yii::$app->name ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?php $this->head() ?>
</head>
    <body class=" ">
<?php $this->beginBody() ?>
        <div class='page-topbar '>
            <div class='logo-area'>
            </div>
            <div class='quick-area'>
                <div class='pull-left'>
                    <ul class="info-menu left-links list-inline list-unstyled">
                        <li class="sidebar-toggle-wrap">
                            <a href="#" data-toggle="sidebar" class="sidebar_toggle">
                                <i class="fa fa-bars"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class='pull-right'>
                    <ul class="info-menu right-links list-inline list-unstyled">
                        <li class="profile">
                            <a href="#" data-toggle="dropdown" class="toggle">
                                <img src="<?php echo Yii::getAlias('@web/images/avatar1.jpg') ?>" alt="user-image" class="img-circle img-inline">
                                <span><?= \Yii::$app->user->identity->nameFull ?> <i class="fa fa-angle-down"></i></span>
                            </a>
                            <?php echo $this->render('_menu_user'); ?>
                        </li>
                        <li class="chat-toggle-wrapper">
                            <a href="#" data-toggle="chatbar" class="toggle_chat">
                                <i class="fa fa-comments"></i>
                                <span class="badge badge-accent">9</span>
                                <i class="fa fa-times"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="page-container row-fluid container-fluid">

            <div class="page-sidebar fixedscroll">

                <div class="page-sidebar-wrapper ps-container ps-active-y " id="main-menu-wrapper">




                    <?php echo $this->render('_menu'); ?>

                    <div class="version_">

                        <h6>
                            <abbr title="Version">
                                <?php echo Yii::$app->params['version'];?>
                            </abbr>
                    </div>

                </div>

            </div>
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper row" style=''>
<?php if (!empty(Yii::$app->params['layout.title'])): ?>
                        <div class="col-xs-12">
                            <div class="page-title">

                                <div class="pull-left">
                                    <h1 class="title"><?= Html::encode(Yii::$app->params['layout.title']) ?></h1>
                                                             </div>

                                <div class="pull-right hidden-xs">
                                </div>

                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
<?= $content ?>

                </section>
            </section>

            <div class="chatapi-windows ">

            </div>    </div>


        <?php if (\Yii::$app->params['show.modal.delete']) { ?>

            <?php
            $this->registerJs('
                    $("body").on("click",".option-delete",function(){
                        var urlDelete = $(this).attr("href");
                        $("#form-confirm").attr("action",urlDelete);
                        $("#modal-confirm .modal-body").html("Desea eliminar este elemento?");
                        $("#modal-confirm").modal("show");
                        return false;
                    });
                    
                    $("body").on("click",".option-available",function(){
                        var urlAvailable = $(this).attr("href");
                        $("#form-confirm").attr("action",urlAvailable);
                        $("#modal-confirm .modal-body").html("Desea colocar disponible este elemento?");
                        $("#modal-confirm").modal("show");
                        return false;
                    });
                    
                    $("body").on("click",".option-clean",function(){
                        var urlClean = $(this).attr("href");
                        $("#form-confirm").attr("action",urlClean);
                        $("#modal-confirm .modal-body").html("Desea eliminar los reporte z de la máquina?");
                        $("#modal-confirm").modal("show");
                        return false;
                    });
                    
                    $("#btn-confirm-success").click(function(){
                        var url = $("#form-confirm").attr("action");
                        var _csrf="'.Yii::$app->request->getCsrfToken() .'";
                        
                        $.post( url,{ _csrf:_csrf})
                        .done(function( res ) {
                            console.log("listo1: "+url);
                        })
                        .fail(function() {
                            console.log("listo2: "+url);
                        });
                        console.log(url);
                        return false;
                    });
                            '
            );
            echo $this->render('../modals/_confirm');
        }
        $mens = '';
        foreach (Yii::$app->session->getAllFlashes() as $key => $value) {
            if (strpos($key, 'success') === false) {

            } else {
                $mens .= "showSuccess('" . $value . "');";
            }
            if (strpos($key, 'warning') === false) {

            } else {
                $mens .= "showErrorMessage('" . $value . "');";
            }
        }

        $this->registerJs($mens);

            $this->registerJs($mens);

            $this->registerJs("var dashboard_url='".Url::to(['/contribuyentes/filtro'])."'",View::POS_BEGIN);
            $this->registerJs("var social_reason_minimumInputLength = '".Yii::$app->params['filtro.social_reason.minimumInputLength']."'",View::POS_BEGIN);

        ?>
<?php $this->endBody() ?>
    </body>
</html>

<?php $this->endPage() ?>
