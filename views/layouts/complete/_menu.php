<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\accesos\models\RBACRolesOpciones;
use app\modules\accesos\models\RBACRolesOpcionesUsuarios;
?>
<?php
    $menuSQL = RBACRolesOpcionesUsuarios::find()
                    ->alias('t')
                    ->where([
                        'm.categoria' => 'main',
                        't.estatus' => \app\components\CActiveRecord::STATUS_ACTIVE,
                        'o.estatus' => \app\components\CActiveRecord::STATUS_ACTIVE,
                        'm.estatus' => \app\components\CActiveRecord::STATUS_ACTIVE,
                        'r.estatus' => \app\components\CActiveRecord::STATUS_ACTIVE,
                        't.id_rbac_usuario' => \Yii::$app->user->identity->id_rbac_usuario,
                    ])
                    ->joinWith(['idRbacRolOpcion r',])
                    ->joinWith(['idRbacRolOpcion.idRbacMenuOpcion o'])
                    ->joinWith(['idRbacRolOpcion.idRbacMenuOpcion.idMenu m'])
                    ->orderBy(['m.jerarquia' => SORT_ASC, 'm.orden' => SORT_ASC, 'o.orden' => SORT_ASC])
                    ->all();

    $menuSQL = RBACRolesOpciones::find()
                    ->alias('t')
                    ->where([
                        'm.categoria' => 'main',
                        't.estatus' => \app\components\CActiveRecord::STATUS_ACTIVE,
                        'o.estatus' => \app\components\CActiveRecord::STATUS_ACTIVE,
                        'm.estatus' => \app\components\CActiveRecord::STATUS_ACTIVE,
                        't.id_rbac_rol' => \Yii::$app->user->identity->id_rbac_rol,
                    ])
                    ->joinWith(['idRbacMenuOpcion o','idRbacMenuOpcion.idMenu m'])
                    ->orderBy(['m.jerarquia' => SORT_ASC, 'm.orden' => SORT_ASC, 'o.orden' => SORT_ASC])
                    ->all();

    $menus = [];
    $menul1 = [];
    $menul2 = [];
    $menul3 = [];

    foreach ($menuSQL as $m) {
        $usuariositem = RBACRolesOpcionesUsuarios::find()
            ->where([
                'estatus' => \app\components\CActiveRecord::STATUS_INACTIVE,
                'id_rbac_usuario' => \Yii::$app->user->identity->id_rbac_usuario,
                'id_rbac_rol_opcion' => $m->id_rbac_rol_opcion
            ])->one();

        if(!$usuariositem) {
            $id_rbac_menu = $m->idRbacMenuOpcion->idMenu->id_rbac_menu;
            $jerarquia = $m->idRbacMenuOpcion->idMenu->jerarquia;
            if ($m->idRbacMenuOpcion->idMenu->nivel == 1) {
                $menul1[$id_rbac_menu]['data'] = NULL;
                $menul1[$id_rbac_menu]['menu_id'] = $m->idRbacMenuOpcion->idMenu->id_rbac_menu;
                $menul1[$id_rbac_menu]['menu_descripcion'] = $m->idRbacMenuOpcion->idMenu->descripcion;
                $menul1[$id_rbac_menu]['menu_url'] = $m->idRbacMenuOpcion->idMenu->url;
                $menul1[$id_rbac_menu]['menu_url_tipo'] = $m->idRbacMenuOpcion->idMenu->url_tipo;
                $menul1[$id_rbac_menu]['menu_icono'] = $m->idRbacMenuOpcion->idMenu->icono;
                $menul1[$id_rbac_menu]['menu_modulo'] = $m->idRbacMenuOpcion->idMenu->modulo;
                $menul1[$id_rbac_menu]['menu_jerarquia'] = $m->idRbacMenuOpcion->idMenu->jerarquia;
            }
            if ($m->idRbacMenuOpcion->idMenu->nivel == 2) {
                $menul2[$jerarquia]['data'][$id_rbac_menu]['menu_id'] = $m->idRbacMenuOpcion->idMenu->id_rbac_menu;
                $menul2[$jerarquia]['data'][$id_rbac_menu]['menu_descripcion'] = $m->idRbacMenuOpcion->idMenu->descripcion;
                $menul2[$jerarquia]['data'][$id_rbac_menu]['menu_url'] = $m->idRbacMenuOpcion->idMenu->url;
                $menul2[$jerarquia]['data'][$id_rbac_menu]['menu_url_tipo'] = $m->idRbacMenuOpcion->idMenu->url_tipo;
                $menul2[$jerarquia]['data'][$id_rbac_menu]['menu_icono'] = $m->idRbacMenuOpcion->idMenu->icono;
                $menul2[$jerarquia]['data'][$id_rbac_menu]['menu_modulo'] = $m->idRbacMenuOpcion->idMenu->modulo;
                $menul2[$jerarquia]['data'][$id_rbac_menu]['menu_jerarquia'] = $m->idRbacMenuOpcion->idMenu->jerarquia;
            }
            if ($m->idRbacMenuOpcion->idMenu->nivel == 3) {
                $menul3[$jerarquia]['data'][$id_rbac_menu]['menu_descripcion'] = $m->idRbacMenuOpcion->idMenu->descripcion;
                $menul3[$jerarquia]['data'][$id_rbac_menu]['menu_url'] = $m->idRbacMenuOpcion->idMenu->url;
                $menul3[$jerarquia]['data'][$id_rbac_menu]['menu_url_tipo'] = $m->idRbacMenuOpcion->idMenu->url_tipo;
                $menul3[$jerarquia]['data'][$id_rbac_menu]['menu_icono'] = $m->idRbacMenuOpcion->idMenu->icono;
                $menul3[$jerarquia]['data'][$id_rbac_menu]['menu_modulo'] = $m->idRbacMenuOpcion->idMenu->modulo;
                $menul3[$jerarquia]['data'][$id_rbac_menu]['menu_jerarquia'] = $m->idRbacMenuOpcion->idMenu->jerarquia;
            }
        }
    }
?> 
<ul class='wraplist' style="min-height: 92%;height: auto;">   
    <li class="menusection"><?= Yii::t('app','menu') ?></li>
        <?php  foreach ($menul1 as $m) : ?>
            <?php if (count(@$menul2[$m['menu_id']]['data']) > 0) {
                    $row = '<span class="arrow "></span> ';
                    $url = '#';
                } else {
                    $url = Yii::getAlias('@web/'.$m['menu_url']);
                    $row = '';
                }
                $url_index = explode('/', $m['menu_url']);
            ?>
            <li class="<?= \Yii::$app->controller->id == $url_index[0] ? 'open' : '' ?>">
                <?= Html::a($m['menu_icono'] . ' <span class="title">' . $m['menu_descripcion']. '</span>' . $row, $url) ?>
                <?php if (count(@$menul2[$m['menu_id']]['data']) > 0) : ?>
                <ul class="sub-menu" >
                    <?php foreach ($menul2[$m['menu_id']]['data'] as $ml2) : ?>
                        <?php if (count(@$menul3[$ml2['menu_id']]['data']) > 0) {
                            $rowL2 = '<span class="arrow "></span>';
                            $urlL2 = 'javascript:;';
                        } else {
                            $rowL2 = '';
                            $urlL2 = $ml2['menu_url'];
                        } 
                        $url_index2 = explode('/', $ml2['menu_url']);
                        $active_url = \Yii::$app->controller->id == $url_index2[1] ? 'active' : '';
                        ?>
                        <li>
                            <?= Html::a($ml2['menu_icono'] . ' <span class="title">' . $ml2['menu_descripcion']. '</span>' . $rowL2, Yii::getAlias('@web/'.$urlL2),['class' => $active_url]) ?>
                            <?php if (count(@$menul3[$ml2['menu_id']]['data']) > 0) :?>
                                <ul class="sub-menu">
                                    <?php foreach ($menul3[$ml2['menu_id']]['data'] as $ml3) : ?>
                                        <li>
                                            <?php $class_active = 'active'; ?>
                                            <?= Html::a($ml3['menu_icono'] . ' <span class="title">' . $ml3['menu_descripcion']. '</span>' , Yii::getAlias('@web/'.$ml3['menu_url'])) ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </li>
                    <?php endforeach;?>
                </ul>
                <?php endif;?>
            </li>
        <?php endforeach; ?>
</ul>