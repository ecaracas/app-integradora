<?php

namespace app\components;

use Yii;

class CController extends \yii\web\Controller {

    public function init() {
        parent::init();
    }

    public function __construct($id, $module, $config = array()) {

        if (!Yii::$app->user->isGuest){
    
        // \app\modules\admin\models\OfiRegiones::getRegionesPerfil();
        
        
         Yii::$app->params['grid.pagination.pagesize'] = 
                 
                 (isset(Yii::$app->request->queryParams['per-page2']) &&  !empty(Yii::$app->request->queryParams['per-page2']) )  
                 ?(Yii::$app->request->queryParams['per-page2']):                 
                 (empty(Yii::$app->request->queryParams['per-page']) ? Yii::$app->params['grid.pagination.pagesize'] : Yii::$app->request->queryParams['per-page']) ;
        }
        
        return parent::__construct($id, $module, $config);
    }
    
    public function logNavegacion($id=null, $module=null)
    {
        $nombre_fecha = date("Y-m-d");
        $nombreusuario = str_replace(";", "", Yii::$app->user->identity->attributes['nombreusuario']);
        $carpeta = Yii::getAlias('@app'). DIRECTORY_SEPARATOR .'log_navegacion' . DIRECTORY_SEPARATOR . $nombre_fecha . DIRECTORY_SEPARATOR . $nombreusuario . DIRECTORY_SEPARATOR;
        $nombretxt = 'log';
        $row = [
            'nombreusuario'=> $nombreusuario,
            'id_rbac_usuario'=> Yii::$app->user->identity->attributes['id_rbac_usuario'],
            'fecha_creado'=> date("Y-m-d H:i:s"),
            'url'=> str_replace(";", "", Yii::$app->request->url),
            'modulo'=> Yii::$app->controller->module->id,
            'vista'=> Yii::$app->controller->action->id,
            'controlador'=> Yii::$app->controller->id,
            'ip'=>str_replace(";", "", Yii::$app->request->getUserIP()),
        ];

        $log = implode(";", $row);
        if (!file_exists($carpeta)) {
            if(!mkdir($carpeta, 0777, true)) {
                return;
            }
        }

        $file = fopen($carpeta. $nombretxt.'.txt', "a");
        fwrite($file, $log. PHP_EOL);
        fclose($file);      
    }


}
