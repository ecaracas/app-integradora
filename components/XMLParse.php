<?php
namespace app\components;

use Yii;
use yii\base\Component;

class XMLParse extends Component
{
    public function parseXmlFile($path) {
        libxml_use_internal_errors(true);
        $xml=simplexml_load_file($path);
        if ($xml === false) {
            $errors = [];
            foreach(libxml_get_errors() as $error) {
                $errors[] = $error->message;
            }
            return $errors;
        } else {
            return $xml;
        }
    }
    
    public function validateFileSchema($xmlFile,$xsdFile) {
        libxml_use_internal_errors(true);
        $xml = new \DOMDocument();
        $xml->load($xmlFile);
        if(!$xml->schemaValidate($xsdFile)) {
            return false;
        } else {
            return true;
        }
    }
    
    public function validateStringSchema($xmlString, $xsdFile) {
        libxml_use_internal_errors(true);
        $xml = new \DOMDocument();
        $xml->loadXML($xmlString);
        if(!$xml->schemaValidate($xsdFile)) {
            return false;
        } else {
            return true;
        }
    }

    public function parseXmlString($string) {
        libxml_use_internal_errors(true);
        $xml=simplexml_load_string($string);
        if ($xml === false) {
            $errors = [];
            foreach(libxml_get_errors() as $error) {
                $errors[] = $error->message;
            }
            return $errors;
        } else {
            //$this->setOutputFormat();
            return $xml;
        }  
    }
    
    public function response($code, $numberz = null, $sequence = null,$cmd = null) {
        $domXML = new \DOMDocument("1.0", "UTF-8");
        $domXML->formatOutput = true;
        $resp = $domXML->createElement("resp");
        $rp = $domXML->appendChild($resp);
        $rp->appendChild($domXML->createElement('cod',$code));
        if($numberz != null) {
            $rp->appendChild($domXML->createElement('nz',$numberz));
        }
        if($sequence != null) {
            $rp->appendChild($domXML->createElement('seq',$sequence));
        }
        if($cmd != null) {
            $rp->appendChild($domXML->createElement('cmd',$cmd));
        }
        $this->setOutputFormat();
        return $domXML->saveXML();
    }
    
    public function setOutputFormat() {
        Yii::$app->getResponse()->format = \yii\web\Response::FORMAT_RAW;
        $response = Yii::$app->getResponse();
        $response->getHeaders()->set('Content-Type', 'application/xml; charset=' . $response->charset);
    }
    
    public function responseConfig($code,$FlagTransaction=0,$fOldZ=0,$tOldZ,$timeRequest=5,$timeStatus=5,$timeOut=1,$timeConfig=5) {
        $domXML = new \DOMDocument('1.0', 'UTF-8');
        $domXML->formatOutput = true;
        $resp = $domXML->createElement("resp");
        $rp = $domXML->appendChild($resp);
        $rp->appendChild($domXML->createElement('cod',$code));
        $rp->appendChild($domXML->createElement('fTrans',$FlagTransaction));
        $rp->appendChild($domXML->createElement('fOldZ',$fOldZ)); //Numero de
        $rp->appendChild($domXML->createElement('tOldZ',$tOldZ)); // Tiempo de envio de Z
        $rp->appendChild($domXML->createElement('tRequest',$timeRequest));
        $rp->appendChild($domXML->createElement('tStatus',$timeStatus));
        $rp->appendChild($domXML->createElement('tOut',$timeOut));
        $rp->appendChild($domXML->createElement('tConfig',$timeConfig));
        $this->setOutputFormat();
        return $domXML->saveXML();
    }
    
    public function getXmlContent($xmlFile = null) {
        if($xmlFile == null) {
            return false;
        }
        return file_get_contents($xmlFile);
    }
   public function FormatAmount($monto = null) {

        $monto = number_format($monto, '2', ',', '.');

        return $monto;
    }
}