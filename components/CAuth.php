<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use app\modules\accesos\models\RBACMenus;
use app\modules\accesos\models\RBACMenuOpciones;
use app\modules\accesos\models\RBACRolesOpciones;
use app\modules\accesos\models\RBACRolesOpcionesUsuarios;

class CAuth extends Component {

    public function check($modulo,$option) {
        //return true;
        // Verificamos si lo tiene desactivado el Usuario
        $usuarioitem = RBACRolesOpcionesUsuarios::find()
            ->alias('t')
            ->where([
                't.estatus' => \app\components\CActiveRecord::STATUS_INACTIVE,
                't.id_rbac_usuario' => \Yii::$app->user->identity->id_rbac_usuario,
                'o.opcion' => $option,
                'm.modulo' => $modulo,
            ])
            ->joinWith(['idRbacRolOpcion.idRbacMenuOpcion o','idRbacRolOpcion.idRbacMenuOpcion.idMenu m'])
            ->one();
        
        
        if($usuarioitem) {
            // No tiene permiso para ingresar
            return false;
        } else {
            // Verificamos si el rol tiene permiso 
            $rolitem = RBACRolesOpciones::find()
                            ->alias('t')
                            ->where([
                                't.estatus' => CActiveRecord::STATUS_ACTIVE,
                                'o.estatus' => CActiveRecord::STATUS_ACTIVE,
                                'r.estatus' => CActiveRecord::STATUS_ACTIVE,
                                'm.estatus' => CActiveRecord::STATUS_ACTIVE,
                                'o.opcion' => $option,
                                'm.modulo' => $modulo,
                                't.id_rbac_rol' => \Yii::$app->user->identity->id_rbac_rol
                            ])
                            ->joinWith(['idRbacMenuOpcion o','idRbacRol r','idRbacMenuOpcion.idMenu m'])
                            ->one();
        
            
            if($rolitem) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static function getMenu() {

        $menuList = RBACRolesOpciones::find()
                ->select(['m.modulo as m_modules', 'o.opcion as m_id'])
                ->alias('t')
                ->where([
                    'o.estatus' => \app\components\CActiveRecord::STATUS_ACTIVE,
                    'r.estatus' => \app\components\CActiveRecord::STATUS_ACTIVE,
                    'm.estatus' => \app\components\CActiveRecord::STATUS_ACTIVE,
                    't.id_rbac_rol' => \Yii::$app->user->identity->id_rbac_rol,
                ])
                ->joinWith(['idRbacMenuOpcion o', 'idRbacRol r'])
                ->all();
        $menu = [];
        foreach ($menuList as $row) {
            $menu[$row['m_modules']][$row['m_id']] = TRUE;
        }
        return $menu;
    }

}
