<?php

namespace app\components;

use Yii;
use yii\base\Component;

class util extends Component{


    public function numberConvert($num) {
        $aux = $num;
        $num = floatval($num);
        $arNum = array(
            1 => array(
                "UNIT" => "T",
                "VALUE" => 1000000000000000000
            ),
            2 => array(
                "UNIT" => "MB",
                "VALUE" => 1000000000000000
            ),
            3 => array(
                "UNIT" => "B",
                "VALUE" => 1000000000000
            ),
            4 => array(
                "UNIT" => "MM",
                "VALUE" => 1000000000
            ),
            5 => array(
                "UNIT" => "M",
                "VALUE" => 1000000
            ),
            6 => array(
                "UNIT" => "K",
                "VALUE" => 1000
            ),
            7 => array(
                "UNIT" => "",
                "VALUE" => 1
            ),
        );

        $flag = 0;
        foreach ($arNum as $arItem) {
            if ($num >= $arItem["VALUE"]) {
                $result = $num / $arItem["VALUE"];
                $result = strval(round($result, 0)) . $arItem["UNIT"];
                $flag = 1;
                break;
            }
        }

        if(!$flag){
            $result = $this->formatearNumero($num);
        }

        if (isset($result)) {
            return $result;
        }
        return $aux;
    }

    public static function formatearNumero($numero, $separadorMiles = '.', $separadorDecimal = ',', $moneda = ''){
        $arr_numero = explode(".",(string)$numero);
        if(count($arr_numero)>0){
            if(is_numeric($arr_numero[0])){
                $longitud = strlen($arr_numero[0]);
                $contMiles = 3;
                $numeroFinal = "";
                for($cont = $longitud-1; $cont >= 0; $cont--){
                    $contMiles--;
                    if($contMiles == 0 && $cont > 0){
                        $numeroFinal = $separadorMiles.substr($numero, $cont, 1).$numeroFinal;
                        $contMiles = 3;
                    }else{
                        $numeroFinal = substr($numero, $cont, 1).$numeroFinal;
                    }
                }
                if(count($arr_numero)>1){
                    if(strlen($arr_numero[1])<2){
                        return $moneda."".$numeroFinal.$separadorDecimal.$arr_numero[1]."0";
                    }elseif(strlen($arr_numero[1])>2){
                        return $moneda."".$numeroFinal.$separadorDecimal.substr($arr_numero[1],0,2);
                    }else{
                        return $moneda."".$numeroFinal.$separadorDecimal.$arr_numero[1];
                    }
                }else{
                    return $moneda."".$numeroFinal.$separadorDecimal."00";
                }
            }else{
                return "0,00";
            }
        }
    }

}