<?php

namespace app\components;

use Yii;
use yii\db\ActiveRecord;

class CActiveRecord extends ActiveRecord {

    const STATUS_ACTIVE = 1;
    const STATUS_ACTIVE_LABEL = 'Activo';
    const STATUS_ACTIVE_CLASS = 'label label-success';

    const STATUS_INACTIVE = 0;
    const STATUS_INACTIVE_LABEL = 'Inactivo';
    const STATUS_INACTIVE_CLASS = 'label label-default';

    const STATUS_UNSUBSCRIBED = 2;
    const STATUS_UNSUBSCRIBED_LABEL = 'Dado de baja';
    const STATUS_UNSUBSCRIBED_CLASS = 'label label-info';

    const STATUS_DELETED = 9;
    const STATUS_DELETED_LABEL = 'deleted';
    const STATUS_DELETED_CLASS = 'label label-danger';

    const STATUS_NOTFOUND_LABEL = 'not_found';
    const STATUS_NOTFOUND_CLASS = 'label label-info';

    const STATUS_ENABLE = 1;
    const STATUS_ENABLE_LABEL = 'Activo';
    const STATUS_ENABLE_CLASS = 'label label-success';

    const STATUS_DISABLE = 0;
    const STATUS_DISABLE_LABEL = 'Inactivo';
    const STATUS_DISABLE_CLASS = 'label label-default';


    public function delete() {

        $this->estatus = self::STATUS_DELETED;
        $this->fecha_eliminado = $this->getFecha();
        $this->eliminado_por = intval(\Yii::$app->user->id);
        if ($this->save()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function status($val = TRUE) {

        if ($val) {
            $this->status = self::STATUS_ACTIVE;
        } else {
            $this->status = self::STATUS_INACTIVE;
        }
    }


    public function getStatus($value) {

        $label = self::getStatusLabel($value);
        $class = self::getStatusClass($value);

        $response = '<label class="' . $class . '" >' . Yii::t('app', $label) . '</label>';

        return $response;
    }

    public static function getStatusList3() {

        return [
            self::STATUS_ACTIVE => Yii::t('app', self::STATUS_ACTIVE_LABEL),
            self::STATUS_INACTIVE => Yii::t('app', self::STATUS_INACTIVE_LABEL),
            self::STATUS_UNSUBSCRIBED => Yii::t('app', self::STATUS_UNSUBSCRIBED_LABEL),
        ];
    }

    public static function getStatusList() {

        return [
            self::STATUS_ACTIVE => Yii::t('app', self::STATUS_ACTIVE_LABEL),
            self::STATUS_INACTIVE => Yii::t('app', self::STATUS_INACTIVE_LABEL),
        ];
    }

    public static function getStatusLabel($value) {

        if ($value == self::STATUS_ACTIVE) {
            return self::STATUS_ACTIVE_LABEL;
        } elseif ($value == self::STATUS_INACTIVE) {
            return self::STATUS_INACTIVE_LABEL;
        } elseif ($value == self::STATUS_DELETED) {
            return self::STATUS_DELETED_LABEL;
        } elseif ($value == self::STATUS_UNSUBSCRIBED) {
            return self::STATUS_UNSUBSCRIBED;
        } else {
            return self::STATUS_NOTFOUND_LABEL;
        }
    }

    public static function getStatusClass($value) {

        if ($value == self::STATUS_ACTIVE) {
            return self::STATUS_ACTIVE_CLASS;
        } elseif ($value == self::STATUS_INACTIVE) {
            return self::STATUS_INACTIVE_CLASS;
        } elseif ($value == self::STATUS_DELETED) {
            return self::STATUS_DELETED_CLASS;
        } else {
            return self::STATUS_NOTFOUND_CLASS;
        }
    }


    public function getActive() {

        $label = self::getActiveLabel($this->estatus);
        $class = self::getActiveClass($this->estatus);

        $response = '<label class="' . $class . '" >' . Yii::t('app', $label) . '</label>';

        return $response;
    }

    public static function getActiveLabel($value) {

        if ($value == self::STATUS_ENABLE) {
            return self::STATUS_ENABLE_LABEL;
        } elseif ($value == self::STATUS_DISABLE) {
            return self::STATUS_DISABLE_LABEL;
        } elseif ($value == self::STATUS_UNSUBSCRIBED) {
            return self::STATUS_UNSUBSCRIBED_LABEL;
        } else {
            return self::STATUS_NOTFOUND_LABEL;
        }
    }

    public static function getActiveClass($value) {

        if ($value == self::STATUS_ENABLE) {
            return self::STATUS_ENABLE_CLASS;
        } elseif ($value == self::STATUS_DISABLE) {
            return self::STATUS_DISABLE_CLASS;
        } elseif ($value == self::STATUS_UNSUBSCRIBED) {
            return self::STATUS_UNSUBSCRIBED_CLASS;
        } else {
            return self::STATUS_NOTFOUND_CLASS;
        }
    }


    public function beforeSave($insert) {
        if ($insert) {

            $this->fecha_creado = $this->getFecha();
            $this->fecha_editado = $this->fecha_creado;

            $this->creado_por = intval(\Yii::$app->user->id);
            $this->editado_por = intval($this->creado_por);
        } else {
            $this->fecha_editado = $this->getFecha();
            if ($this->editado_por != null) {
                $this->editado_por = intval(\Yii::$app->user->id);
            }
        }
        return parent::beforeSave($insert);
    }

    public static function FormatPercentage($monto) {

        $monto = number_format($monto, '2', ',', '.');
        return $monto . '%';
    }

    public static function FormatAmount($monto) {

        $monto = number_format($monto, '2', ',', '.');
        return $monto;
    }

    public function getFecha() {

        date_default_timezone_set('America/Caracas');
        $fecha = new \DateTime(date('His', time()));
        return $fecha->format('Y-m-d H:i:s');
    }
    
     public function validateDate($date)
    {
        $format = Yii::$app->params['format.date.validate'];
        $d = \DateTime::createFromFormat($format, $date);
        if($d && $d->format($format) == $date){
          return  Yii::$app->formatter->asDate($date, Yii::$app->params['format.date.db']);
        }else{
            return false;
        }
    }
    
    
     public function getFormatoFecha($fecha) {

        $date = date_create($fecha);
        return (!empty($fecha)) ? (date_format($date, 'Y-m-d') ) : '';
    }
    
    public function getFullfecha($fecha_hora) {

        $date = date_create($fecha_hora);

        return (!empty($fecha_hora)) ? date_format($date, 'd-m-Y H:i:s') : '';
       
    }
    
    
    public function getFormatoFechaVista($fecha_vista) {

        $date = date_create($fecha_vista);

        return (!empty($fecha_vista)) ? date_format($date, 'd-m-Y') : '';
       
    }
    
      public function exportarTabla($dataProvider,$columns,$titulo) {
        
        ini_set("memory_limit", "900M");
        ini_set("max_execution_time", "900");
        $title= (!empty($titulo)) ? $titulo : 'Reporte';
        \app\components\ExcelGrid::widget([

            'dataProvider' => $dataProvider,
            'filename'=> Yii::t('app',$title),
            'properties' => [
            'title'   => Yii::t('app',$title),
            ],
            'columns' => $columns,
        ]);
        
        
        
        
    }

}
