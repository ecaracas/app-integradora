CREATE TABLE cont_distribuidor(
	id_cont_distribuidor serial PRIMARY KEY,
	razon_social VARCHAR (255) NOT NULL,
	dni_letra VARCHAR (2) NOT NULL,
	dni_numero VARCHAR (20) NOT NULL,
	telefono VARCHAR (20) NOT NULL,
	email VARCHAR (255) NOT NULL,
	estatus VARCHAR (1) NOT NULL,
	fecha_creado TIMESTAMP,
	fecha_editado TIMESTAMP,
	fecha_eliminado TIMESTAMP,
	creado_por FLOAT,
	editado_por FLOAT,
	eliminado_por FlOAT
);

CREATE TABLE cont_marca(
	id_cont_marca serial PRIMARY KEY,
	descripcion VARCHAR (255) NOT NULL,
	estatus VARCHAR (1) NOT NULL,
	fecha_creado TIMESTAMP,
	fecha_editado TIMESTAMP,
	fecha_eliminado TIMESTAMP,
	creado_por FLOAT,
	editado_por FLOAT,
	eliminado_por FlOAT,
);

CREATE TABLE cont_marca_distribuidor(
	id_cont_marca_distribuidor serial PRIMARY KEY,
	id_cont_marca FLOAT NOT NULL,
	id_cont_distribuidor FLOAT NOT NULL,
	fecha_creado TIMESTAMP,
	fecha_editado TIMESTAMP,
	fecha_eliminado TIMESTAMP,
	creado_por FLOAT,
	editado_por FLOAT,
	eliminado_por FlOAT
);

CREATE TABLE cont_modelo(
	id_cont_modelo serial PRIMARY KEY,
	id_cont_marca_distribuidor FLOAT NOT NULL,
	descripcion VARCHAR (255) NOT NULL,
	estatus VARCHAR (1) NOT NULL,
	fecha_creado TIMESTAMP,
	fecha_editado TIMESTAMP,
	fecha_eliminado TIMESTAMP,
	creado_por FLOAT,
	editado_por FLOAT,
	eliminado_por FlOAT
);

ALTER TABLE cont_maquinas_fiscales ADD COLUMN modelo FLOAT;
