-- Actual parameter values may differ, what you see is a default string representation of values
UPDATE public.rbac_menus
SET descripcion='Logs Protocolo TXT'
WHERE id_rbac_menu=2;
UPDATE public.rbac_menus
SET descripcion='Logs Protocolo XML'
WHERE id_rbac_menu=3;
-- Actual parameter values may differ, what you see is a default string representation of values
UPDATE public.rbac_menus
SET descripcion='Operaciones Protocolo XML'
WHERE id_rbac_menu=10;
UPDATE public.rbac_menus
SET descripcion='Configuración Protocolo XML'
WHERE id_rbac_menu=41;
-- Actual parameter values may differ, what you see is a default string representation of values
UPDATE public.rbac_menu_opciones
SET descripcion='Logs Protocolo TXT'
WHERE id_rbac_menu_opcion=2;
UPDATE public.rbac_menu_opciones
SET descripcion='Logs Protocolo XML'
WHERE id_rbac_menu_opcion=3;
UPDATE public.rbac_menu_opciones
SET descripcion='Operaciones Protocolo XML'
WHERE id_rbac_menu_opcion=13;
UPDATE public.rbac_menu_opciones
SET descripcion='Configuración Protocolo XML'
WHERE id_rbac_menu_opcion=20;
-- Actual parameter values may differ, what you see is a default string representation of values
UPDATE public.rbac_menus
SET descripcion='Protocolo XML'
WHERE id_rbac_menu=25;
UPDATE public.rbac_menus
SET descripcion='Protocolo XML Interno'
WHERE id_rbac_menu=29;
-- Actual parameter values may differ, what you see is a default string representation of values
UPDATE public.rbac_menu_opciones
SET descripcion='Protocolo XML Interno'
WHERE id_rbac_menu_opcion=87;
-- Actual parameter values may differ, what you see is a default string representation of values
UPDATE public.rbac_menu_opciones
SET descripcion='Protocolo XML'
WHERE id_rbac_menu_opcion=21;


-- Actual parameter values may differ, what you see is a default string representation of values
UPDATE public.rbac_menus
SET jerarquia=2
WHERE id_rbac_menu=10;

-- Actual parameter values may differ, what you see is a default string representation of values
UPDATE public.rbac_menus
SET estatus=9,fecha_eliminado='2018-11-21 14:29:51.000',eliminado_por=33
WHERE id_rbac_menu=3;

-- Actual parameter values may differ, what you see is a default string representation of values
UPDATE public.rbac_menus
SET descripcion='Logs Protocolos'
WHERE id_rbac_menu=2;

-- Actual parameter values may differ, what you see is a default string representation of values
UPDATE public.rbac_menus
SET descripcion='Paquetes con Error TXT'
WHERE id_rbac_menu=8;
