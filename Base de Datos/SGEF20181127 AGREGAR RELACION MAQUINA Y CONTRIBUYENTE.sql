

/*crear columna relacion contribuyente maquina*/

 AlTER TABLE public.cont_maquinas_fiscales ADD  id_cont_contribuyente float8;
/* crear llave foreanea contribuyente maquina */

ALTER TABLE public.cont_maquinas_fiscales ADD CONSTRAINT cont_maquinas_fiscales_id_cont_contribuyente_fkey FOREIGN KEY (id_cont_contribuyente)
 REFERENCES public.cont_contribuyentes(id_cont_contribuyente) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION;

/* eliminar clave foranea sucursal maquina fiscal */
ALTER TABLE public.cont_maquinas_fiscales DROP CONSTRAINT cont_maquinas_fiscales_id_cont_sucursal_fkey;


/* eliminar campo id_cont_sucursal de cont_maquina_fiscal  pruebas*/
ALTER TABLE public.cont_maquinas_fiscales DROP id_cont_sucursal;





