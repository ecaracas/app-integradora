CREATE TABLE cont_hist_fiscalizacion(
	id_cont_hist_fiscalizacion serial PRIMARY KEY,
	id_cont_maquina_fiscal FLOAT,
	fecha_fiscalizacion TIMESTAMP,
	fecha_creado TIMESTAMP
);


ALTER TABLE cont_hist_fiscalizacion ADD CONSTRAINT fk_id_cont_maquina_fiscal FOREIGN KEY (id_cont_maquina_fiscal) REFERENCES cont_maquinas_fiscales(id_cont_maquina_fiscal);