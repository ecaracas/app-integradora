INSERT INTO public.rbac_menus
(id_rbac_menu, descripcion, url, url_tipo, categoria, estatus, orden, icono, modulo, jerarquia, fecha_creado, fecha_editado, fecha_eliminado, creado_por, editado_por, eliminado_por, nivel)
VALUES(29, 'Imobile Interno', 'invoice/invoice', 0, 'main', 1, 1, '', 'invoice', 14, '2018-09-17 13:43:08.000', '2018-09-17 13:44:59.000', NULL, 32, 32, NULL, 2);

INSERT INTO public.rbac_menus
(id_rbac_menu, descripcion, url, url_tipo, categoria, estatus, orden, icono, modulo, jerarquia, fecha_creado, fecha_editado, fecha_eliminado, creado_por, editado_por, eliminado_por, nivel)
VALUES(30, 'Otros', 'transacciones/default', 0, 'main', 1, 2, '', 'transacciones', 14, '2018-09-17 13:43:08.000', '2018-09-17 13:44:48.000', NULL, 32, 32, NULL, 2);

INSERT INTO public.rbac_menus
(id_rbac_menu, descripcion, url, url_tipo, categoria, estatus, orden, icono, modulo, jerarquia, fecha_creado, fecha_editado, fecha_eliminado, creado_por, editado_por, eliminado_por, nivel)
VALUES(14, 'Transacciones', '#', 0, 'main', 1, 5, '<i class="fa fa-arrows-h"></i>', 'invoice', 0, '2018-09-11 09:28:19.000', '2018-09-14 15:41:03.000', NULL, 32, 34, NULL, 1);

UPDATE public.rbac_menus
SET descripcion='Transacciones', url='#', url_tipo=0, categoria='main', estatus=1, orden=5, icono='<i class="fa fa-arrows-h"></i>', modulo='invoice', jerarquia=0, fecha_creado='2018-09-11 09:28:19.000', fecha_editado='2018-09-14 15:41:03.000', fecha_eliminado=NULL, creado_por=32, editado_por=34, eliminado_por=NULL, nivel=1
WHERE id_rbac_menu=14;

UPDATE public.rbac_menu_opciones
SET id_rbac_menu=14, descripcion='Transacciones', opcion='transacciones', url='/', url_tipo=0, categoria='main', estatus=1, orden=1, icono='', jerarquia=1, fecha_creado='2017-02-12 00:00:00.000', fecha_editado='2017-02-12 00:00:00.000', fecha_eliminado=NULL, creado_por=0, editado_por=0, eliminado_por=NULL
WHERE id_rbac_menu_opcion=4;

INSERT INTO public.rbac_menu_opciones
(id_rbac_menu_opcion, id_rbac_menu, descripcion, opcion, url, url_tipo, categoria, estatus, orden, icono, jerarquia, fecha_creado, fecha_editado, fecha_eliminado, creado_por, editado_por, eliminado_por)
VALUES(85, 29, 'Listar', 'listar_factura', '/', 0, 'main', 1, 1, '', 0, '2018-06-26 00:00:00.000', '2018-06-26 00:00:00.000', '2018-06-26 00:00:00.000', 0, 0, 0);

INSERT INTO public.rbac_menu_opciones
(id_rbac_menu_opcion, id_rbac_menu, descripcion, opcion, url, url_tipo, categoria, estatus, orden, icono, jerarquia, fecha_creado, fecha_editado, fecha_eliminado, creado_por, editado_por, eliminado_por)
VALUES(86, 29, 'Detalle', 'ver_factura', '/', 0, 'main', 1, 2, '', 0, '2018-06-26 00:00:00.000', '2018-06-26 00:00:00.000', '2018-06-26 00:00:00.000', 0, 0, 0);

INSERT INTO public.rbac_menu_opciones
(id_rbac_menu_opcion, id_rbac_menu, descripcion, opcion, url, url_tipo, categoria, estatus, orden, icono, jerarquia, fecha_creado, fecha_editado, fecha_eliminado, creado_por, editado_por, eliminado_por)
VALUES(88, 30, 'Otros', 'transacciones', '/', 0, 'main', 1, 0, '', 1, '2018-06-26 00:00:00.000', '2018-06-26 00:00:00.000', '2018-06-26 00:00:00.000', 0, 0, 0);

INSERT INTO public.rbac_roles_opciones
(id_rbac_rol_opcion, id_rbac_rol, estatus, fecha_creado, fecha_editado, fecha_eliminado, creado_por, editado_por, eliminado_por, id_rbac_menu_opcion)
VALUES(86, 1, 1, '2017-08-01 16:22:38.000', '2018-09-17 14:43:16.000', NULL, 1, 32, NULL, 85);

INSERT INTO public.rbac_roles_opciones
(id_rbac_rol_opcion, id_rbac_rol, estatus, fecha_creado, fecha_editado, fecha_eliminado, creado_por, editado_por, eliminado_por, id_rbac_menu_opcion)
VALUES(88, 1, 1, '2018-09-11 09:42:02.000', '2018-09-17 14:43:16.000', NULL, 32, 32, NULL, 84);

INSERT INTO public.rbac_roles_opciones
(id_rbac_rol_opcion, id_rbac_rol, estatus, fecha_creado, fecha_editado, fecha_eliminado, creado_por, editado_por, eliminado_por, id_rbac_menu_opcion)
VALUES(89, 1, 1, '2018-09-11 09:42:02.000', '2018-09-17 14:43:16.000', NULL, 32, 32, NULL, 83);

INSERT INTO public.rbac_roles_opciones
(id_rbac_rol_opcion, id_rbac_rol, estatus, fecha_creado, fecha_editado, fecha_eliminado, creado_por, editado_por, eliminado_por, id_rbac_menu_opcion)
VALUES(92, 1, 1, '2017-08-01 16:22:38.000', '2018-09-17 14:43:16.000', NULL, 1, 32, NULL, 86);

INSERT INTO public.rbac_roles_opciones
(id_rbac_rol_opcion, id_rbac_rol, estatus, fecha_creado, fecha_editado, fecha_eliminado, creado_por, editado_por, eliminado_por, id_rbac_menu_opcion)
VALUES(93, 1, 1, '2017-08-01 16:22:38.000', '2018-09-17 14:43:16.000', NULL, 1, 32, NULL, 87);

INSERT INTO public.rbac_roles_opciones
(id_rbac_rol_opcion, id_rbac_rol, estatus, fecha_creado, fecha_editado, fecha_eliminado, creado_por, editado_por, eliminado_por, id_rbac_menu_opcion)
VALUES(94, 1, 1, '2018-09-17 14:43:16.000', '2018-09-17 14:43:16.000', NULL, 32, 32, NULL, 88);

