-- Actual parameter values may differ, what you see is a default string representation of values
UPDATE public.cat_tipo_producto_imobile
SET producto='DISPOSITIVO PROTOCOLO XML'
WHERE id_cat_tipo_producto_imobile=1;
UPDATE public.cat_tipo_producto_imobile
SET producto='MODULO PROTOCOLO XML'
WHERE id_cat_tipo_producto_imobile=2;
UPDATE public.cat_tipo_producto_imobile
SET producto='MODULO PROTOCOLO TXT'
WHERE id_cat_tipo_producto_imobile=3;
