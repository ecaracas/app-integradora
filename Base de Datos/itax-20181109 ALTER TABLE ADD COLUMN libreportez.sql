ALTER TABLE lib_reportez ADD COLUMN fecha_ultima_nc timestamp;
ALTER TABLE lib_reportez ADD COLUMN hora_ultima_nc time;

ALTER TABLE lib_reportez ADD COLUMN fecha_ultima_nd timestamp;
ALTER TABLE lib_reportez ADD COLUMN hora_ultima_nd time;

ALTER TABLE lib_reportez ADD COLUMN fecha_ultima_dnf timestamp;
ALTER TABLE lib_reportez ADD COLUMN hora_ultima_dnf time;

ALTER TABLE lib_reportez ADD COLUMN fecha_ultima_rmf timestamp;
ALTER TABLE lib_reportez ADD COLUMN hora_ultima_rmf time;


ALTER TABLE lib_reportez ADD COLUMN cant_factura NUMERIC(12,2);
ALTER TABLE lib_reportez ADD COLUMN cant_dnf NUMERIC(12,2);
ALTER TABLE lib_reportez ADD COLUMN cant_rmf NUMERIC(12,2);

ALTER TABLE lib_reportez ADD COLUMN num_ultima_rmf NUMERIC(12,2);

ALTER TABLE lib_reportez ADD COLUMN fecha_fiscalizacion timestamp;