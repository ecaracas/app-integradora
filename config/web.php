<?php

$_ini = @parse_ini_file(__DIR__ . '/env.txt', true);

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'layout' => 'complete/main.php',
    'language' => 'es',
    'sourceLanguage' => 'es',
    'timezone' => 'UTC',
    'name' => 'Integracion Intranet-Extranet',
    'modules' => [
        'accesos' => [
            'class' => 'app\modules\accesos\Module',
        ],
    ],
    'components' => [
        'session' => [
            'name' => 'PHPVEHTTPSESSID',
            'savePath' => __DIR__ . '/../runtime',
        ],
        'request' => [
            'cookieValidationKey' => '5p1dh6Ivh1cauUixEPX8eSZWhUi97lyy',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'thousandSeparator' => '.',
            'decimalSeparator' => ',',
            'dateFormat' => 'dd-MM-yyyy',
            'nullDisplay' => '',
            'defaultTimeZone' => date_default_timezone_get(),
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        
        'db2' => require(__DIR__ . '/db2.php'),
       
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<module><controller:\w+>/<id:\d+>' => '<module><controller>/view',
                '<module><controller:\w+>/<action:\w+>/<id:\d+>' => '<module><controller>/<action>',
                '<module><controller:\w+>/<action:\w+>' => '<module><controller>/<action>',
                '<module>/<controller>/<action>/<id:\d+>' => '<module>/<controller>/<action>',
                'defaultRoute' => '/site/index',
            ],
        ],
        'xmlparse' => [
            'class' => 'app\components\XMLParse',
        ],
        'siteApi' => [
            'class' => 'mongosoft\soapclient\Client',
            'url' => 'http://127.0.0.1/ws-imobile/web/index.php/api/xml',
            'options' => [
            ],
        ],
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'es-Es',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],
        'auth' => [
            'class' => 'app\components\CAuth',
        ],
    ],
    'on beforeRequest' => function ($event) {
Yii::$container->set('yii\grid\GridView', [

    'pager' => [
        'options' => ['class' => 'pagination pagination-sm'],
        'hideOnSinglePage' => true,
        'lastPageLabel' => '>>',
        'firstPageLabel' => '<<',
    ],
    'filterSelector' => 'select[name="per-page"]',
]);

Yii::$container->set('nterms\pagesize\PageSize', [
    'options' => ['id' => 'select_widget'],
    'labelOptions' => ['id' => 'items_widget'],
     'defaultPageSize' => 5,
    'pageSizeParam' => 'per-page',
]);
},
    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => [
                    'giicustom' => '@app/gii/giicustom/crud/default',
                ]
            ],
            'model' => [
                'class' => 'app\gii\giicustom\model\Generator',
                'templates' => [
                    'giicustom' => '@app/gii/giicustom/model/default',
                ]
            ]

        ],
    ];
}

return $config;
