/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function isChar(event) {

    var regex = new RegExp("^[a-zA-Z0-9]+$");
    if (event.keyCode != 9) {
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            if (event.keyCode != 8) {
                event.preventDefault();
                return false;
            }
        }
    }

}

function isLet(event) {

    var regex = new RegExp("^[a-zA-Z]+$");
    if (event.keyCode != 9) {
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            if (event.keyCode != 8) {
                event.preventDefault();
                return false;
            }
        }
    }
}

function isNum(event) {

    var regex = new RegExp("^[0-9]+$");
    if (event.keyCode != 9) {
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            if (event.keyCode != 8) {
                event.preventDefault();
                return false;
            }
        }
    }

}
function cambiaStatus(valor) {

    if (valor == 'Reporte Z') {
        document.getElementById('select_rango').style.display = "block";
    } else {
        document.getElementById('rango_drop').value = '';
        document.getElementById('peticiones-numero_inicio').value = '';
        document.getElementById('peticiones-numero_fin').value = '';
        document.getElementById('peticiones-fecha_inicio').value = '';
        document.getElementById('peticiones-fecha_fin').value = '';
        document.getElementById('select_rango').style.display = "none";
        document.getElementById('fecha').style.display = "none";
        document.getElementById('numero').style.display = "none";
    }
    return true;
}

function cambiaRango(valor_r) {

    if (valor_r == 2) {
        document.getElementById('numero').style.display = "none";
        document.getElementById('fecha').style.display = "block";
        document.getElementById('petpeticionesz-num_ini').value = '';
        document.getElementById('petpeticionesz-num_fin').value = '';

    } else if (valor_r == 1) {
        document.getElementById('fecha').style.display = "none";
        document.getElementById('numero').style.display = "block";
        document.getElementById('petpeticionesz-fecha_inicio').value = '';
        document.getElementById('petpeticionesz-fecha_fin').value = '';
    } else {
        document.getElementById('fecha').style.display = "none";
        document.getElementById('numero').style.display = "none";
        document.getElementById('petpeticionesz-num_ini').value = '';
        document.getElementById('petpeticionesz-num_fin').value = '';
        document.getElementById('petpeticionesz-fecha_inicio').value = '';
        document.getElementById('petpeticionesz-fecha_fin').value = '';
    }
    return true;



}



