/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    
	menu = function(){
		var act = $('.active').parents('li').eq(1);
		act.addClass('open')
		//act.find('.arrow').addClass('open')
		//act.find('.sub-menu').css({display: 'inline-block'})

		if (act.parents('li')!=undefined){
			var act1 = act.parents('li');
			act1.addClass('open');
			act1.find('.arrow').addClass('open');
			act1.find('.sub-menu').css({display: 'inline-block'});

		}
	}

	menu();
	if (document.querySelector('.js-switch1')) {
		$('.js-switch1').each(function(index,value){
			var switchery = new Switchery(value, { color: '#03a9f4',});
		})
	}

    
    $("#filter_razon").select2({
            minimumInputLength: social_reason_minimumInputLength,
            ajax: {
                url: dashboard_url,
                dataType: 'json',
                quietMillis: 100,
                data: function(term, page) {
                    return {
                        limit: -1,
                        q: term
                    };
                },
                results: function(data, page) {
                    return {
                        results: data
                    }
                }
            },
            formatResult: function(empre) {
            	if (!empre.url) {
                	return "<div class='select2-user-result'>" + empre.name + "<br><span class='rif'>"+empre.dni_numero+"</span></div>";
            	}
            },
            formatSelection: function(empre) {
                return empre.name;
            }

        }).on("change", function(e) {
        	location.href=$(this).select2('data')[0];
        });

        
	$('#s2id_filter_razon').find('#select2-chosen-1').prepend('<i class="fa fa-search"></i> &nbsp;');

	if ($.isFunction($.fn.select2)) {

        $(".s2_filtros").select2({
            placeholder: '',
            allowClear: true
        }).on('select2-open', function() {
            // Adding Custom Scrollbar
            $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
        });
        
        $(".select-multiple").select2({
            placeholder: '',
            allowClear: true
        }).on('select2-open', function() {
            // Adding Custom Scrollbar
            $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
        });

	}
});