$('#razon_social').select2({
    minimumInputLength: social_reason_minimumInputLength,
    allowClear: true,
    ajax: {
        url: dashboard_url,
        dataType: 'json',
        quietMillis: 100,
        data: function(term, page) {
            return {
                limit: -1,
                q: term
            };
        },
        results: function(data, page) {
            return {
                results: data
            }
        }
    },
    formatResult: function(empre) {
    	if (!empre.url) {
        	return '<div class=\"select2-user-result\">' + empre.name + '<br><span class=\"rif\">'+empre.dni_numero+'</span></div>';
    	}
    },
    formatSelection: function(empre) {
        return empre.name;
    }

}).on('change', function(e) {
    seriales();
});



$('#filter_seriales').select2({
    allowClear: true,
    minimumInputLength: 1,
    ajax: {
        url: serial_url,
        dataType: 'json',
        quietMillis: 100,
        data: function(term, page) {
            var v_tipobusqueda = $(tipobusqueda).val();
            var v_fecha = $(fecha_reportez).val();
            if (v_tipobusqueda=='' || v_tipobusqueda==2) {
                v_fecha = null;
            }

            return {
                limit: -1,
                q: term,
                contribuyente: $('#razon_social').val(),
                fecha_libro_diario: v_fecha,
            };
        },
        results: function(data, page) {
            return {
                results: data
            }
        }
    },
    formatResult: function(empre) {
    	if (!empre.url) {
        	return '<div class=\"select2-user-result\">' + empre.name + '<br><span class=\"rif\">'+empre.nombre+'</span></div>';
    	}
    },
    formatSelection: function(empre) {
        return empre.name;
    }

}).on('change', function(e) {
    reporteZ();
});

var boton_buscar = function(){

    var v_num_reportez =  $(num_reportez).val();
    var v_tipobusqueda = $(tipobusqueda).val();
    var v_fecha_reportez = $(fecha_reportez).val();
    var contribuyente_id = $('#razon_social').val();
    var v_serial_equipo = $('#filter_seriales').val();

    if (v_serial_equipo=="") {
        v_serial_equipo =  $(serial_equipo).val();
    }

    disabled_boton = false;
    
    if (v_tipobusqueda == '' && v_serial_equipo) {
        disabled_boton = true;
    }else if (v_tipobusqueda == 1 && v_serial_equipo && v_fecha_reportez) {
        disabled_boton = true;
    }else if (v_tipobusqueda == 2 && v_serial_equipo && v_num_reportez) {
        disabled_boton = true;
    }
    
    if (disabled_boton) {
        $('#buscar').prop("disabled", false)
    }else {
        $('#buscar').prop("disabled", true)
    }

}

$(num_reportez).on('keyup', function(){
    boton_buscar();
}).keyup();


var reporteZ = function(){
    var v_fecha_reportez = $(fecha_reportez).val();
    var contribuyente_id = $('#razon_social').val();
    var v_serial_equipo = $('#filter_seriales').val();

    if (v_serial_equipo=="") {
        v_serial_equipo =  $(serial_equipo).val();
    }
    if (v_serial_equipo) {
        
        $.post( serial_info_url,{ _csrf:_csrf,serial_maquina_fiscal:v_serial_equipo,fecha_informe_diario:v_fecha_reportez})
            .done(function( res ) {
                if (!jQuery.isEmptyObject(res)) {
                    $(fecha_reportez).val(res.fecha_reportez);
                    $(num_reportez).val(res.z);
                    boton_buscar();
                }
            })
            .fail(function() {
                
            });
    }

    boton_buscar();

}

var seriales = function(){

    $('#filter_seriales').select2("val", "");
    $('#s2id_filter_seriales').css({display:'none'})
    $(serial_equipo).parent().css({display:''});

    v_num_reportez = $(num_reportez).val();

    $(serial_equipo).prop("disabled", true).empty();
    $("<option/>", {"value": "","text": v_cargando+"..."}).appendTo(serial_equipo);
    $(serial_equipo).select2();
    var contribuyente_id = $('#razon_social').val();
    var v_fecha_reportez = $(fecha_reportez).val();
    boton_buscar();
    if (contribuyente_id  || v_num_reportez) {
        
        if (contribuyente_id) {  
            $.post( url_serial,{ _csrf:_csrf,contribuyente_id:contribuyente_id,fecha_libro_diario:v_fecha_reportez})
            .done(function( res ) {
                if (!jQuery.isEmptyObject(res)) {
                    $(serial_equipo).prop("disabled", false).empty();

                    $("<option/>", {"value": "","text": selec}).appendTo(serial_equipo);
                    $(serial_equipo).select2();
                    jQuery.each(res, function(index,value){
                        $("<option/>", {
                            "value": index,
                            "text": value
                        }).appendTo(serial_equipo);
                        $(serial_equipo).select2();
                    });
                }else {
                    $(serial_equipo).empty().select2();
                }
            })
            .fail(function() {
                $(serial_equipo).empty().select2();
            });
        }else {
            $('#s2id_filter_seriales').css({display:''});
            $(serial_equipo).parent().css({display:'none'});

        }
    }else {
        $('#s2id_filter_seriales').css({display:''});
        $(serial_equipo).parent().css({display:'none'});
    }

}

buscar = function(e){
    var v_serial_equipo = $('#filter_seriales').val();
    regresar();
    $('#p0').append(v_cargando+"...");
    $('#p0 #w1').hide();
    v_tipobusqueda = $(tipobusqueda).val();



    if (v_serial_equipo=="") {
        v_serial_equipo =  $(serial_equipo).val();
    }
    v_contribuyente_id = $('#razon_social').val();
    v_fecha_reportez = $(fecha_reportez).val();
    v_num_reportez = $(num_reportez).val();

    if (v_tipobusqueda == '' || v_tipobusqueda == 2) {
        v_fecha_reportez = null;
    }

    if (v_tipobusqueda == '' || v_tipobusqueda == 1) {
        v_num_reportez = null;
    }

    $('#form-hidden-rz '+serial_equipo).val(v_serial_equipo);
    $('#form-hidden-rz '+contribuyente_id).val(v_contribuyente_id);
    $('#form-hidden-rz '+fecha_reportez).val(v_fecha_reportez);
    $('#form-hidden-rz '+num_reportez).val(v_num_reportez);
    /*if (v_num_reportez) {
        $.post( url_reportez,{ _csrf:_csrf,serial_equipo:v_serial_equipo,contribuyente_id:v_contribuyente_id,fecha_reportez:v_fecha_reportez,num_reportez:v_num_reportez})
            .done(function( res ) {
                ventana(res);
            })
            .fail(function() {
                $( "#form-hidden-rz" ).submit();
            });
    }else{*/
        $( "#form-hidden-rz" ).submit();
    //}
}

ventana =  function(v_num_reportez){

    $('#form-hidden-diario '+id_form).val(v_num_reportez);
    $('.lib-reportez-1').hide();
    $('#p1').append(v_cargando+"...");
    $('#p1 .diarios-1').hide();
    $('.lib-reportez-2').show();
    $( "#form-hidden-diario" ).submit();
}

AjaxModal = function(url)
{
    jQuery('#cmpltadminModal-6').modal('show', {backdrop: 'static'});
    jQuery('#cmpltadminModal-6 .modal-body').html(v_cargando+"...");
    jQuery.ajax({
        url: url,
        success: function(response)
        {
            jQuery('#cmpltadminModal-6 .modal-body').html(response);
        }
    });
}

tipo = function(){

    var tipo = $(tipobusqueda).val();
    boton_buscar()
    if(tipo == 1)
    {
        $("#busqueda_z").hide();
        $("#busqueda_fecha").show();
    }else if(tipo == 2){
         $("#busqueda_fecha").hide();
         $("#busqueda_z").show();
    } else {
        $("#busqueda_fecha").hide();
        $("#busqueda_z").hide();

    }
}


$(fecha_reportez).on("change", function() {
    boton_buscar();
});

regresar = function(){
    $('.lib-reportez-1').show();
    $('.lib-reportez-2').hide();
}

