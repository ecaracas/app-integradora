<?php

namespace app\modules\accesos\controllers;

use Yii;
use app\modules\accesos\models\RBACUsuarios;
use app\modules\accesos\models\RBACMenuOpciones;
use app\modules\accesos\models\RBACRolesOpciones;
use app\modules\accesos\models\RBACRolesOpcionesUsuarios;
use app\components\CActiveRecord;
use yii\filters\AccessControl;
use app\modules\accesos\models\search\RBACUsuariosSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use yii\helpers\ArrayHelper;


class UsuariosController extends \app\components\CController 
{

    public $moduleID = 'configuracion';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['get'],
                    'view' => ['get'],
                    'create' => ['get', 'post'],
                    'menu' => ['get', 'post','delete'],
                    'update' => ['get', 'put', 'post'],
                    'delete' => ['post', 'delete'],
                ],
            ],
        ];
    }
    
        public function __construct($id, $module, $config = array()) {
        
        Yii::$app->params['layout.title'] = Yii::t('app','configuration'); 
        
        Yii::$app->params['module'] = $this->moduleID;
        
       return parent::__construct($id, $module, $config);
    }
    
    

    public function actionIndex()
    {
    
    if (Yii::$app->auth->check(Yii::$app->params['module'] , 'listar_usuarios')) {
    
        $searchModel = new RBACUsuariosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider            
        ]);
        
        
         } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }
    }

    public function actionView($id = null)
    {
         if (Yii::$app->auth->check(Yii::$app->params['module'] , 'ver_usuario')) {

            if (is_numeric($id)) {

                try {
                    $model = $this->findModel($id);

                    return $this->render('view', [
                                'model' => $model                                
                    ]);
                } catch (ErrorException $e) {
                    throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.runningApp']));
                }
            } else {
                throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.paramsurl']));
            }
        } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }       
        
    }
    


    public function actionCreate()
    {   
         if (Yii::$app->auth->check(Yii::$app->params['module'] , 'crear_usuario')) {

                $model = new RBACUsuarios();
                $model->scenario = 'insert';
                if ($model->load(Yii::$app->request->post())) {
                    try {
                        if ($model->validate()) {
                            $model->contrasena = Yii::$app->security->generatePasswordHash($model->contrasena);
                            $model->contrasena_confirmar = $model->contrasena;
                            if ($model->save()) {
                                Yii::$app->session->setFlash('success', Yii::t('app', Yii::$app->params['message.success']));
                                return $this->redirect(['usuarios/menu', 'id' => $model->id_rbac_usuario]);
                            } else {
                                Yii::$app->session->setFlash('warning', Yii::t('app',Yii::$app->params['message.warning']));
                            }
                        } else {
                            Yii::$app->session->setFlash('warning', Yii::t('app',Yii::$app->params['message.validate']));
                        }
                    
                    } catch (ErrorException $e) {
                        throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.runningApp']));
                    }
                }
                return $this->render('create', [
                            'model' => $model                            
                ]);
            
        } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }
        
    }

    public function actionUpdate($id = null)
    {
                if (Yii::$app->auth->check(Yii::$app->params['module'] , 'editar_usuario')) {

            if (is_numeric($id)) {

                    $model = $this->findModel($id);
                    $model->scenario = 'update';
                    $last_password = $model->contrasena;
                    if ($model->load(Yii::$app->request->post())) {
                        if ($model->validate()) {
                            $password = Yii::$app->request->post('RBACUsuarios');
                            if($password['contrasena'] != '' || !empty($password['contrasena'])) {
                              $model->contrasena = \Yii::$app->getSecurity()->generatePasswordHash($model->contrasena);
                              $model->contrasena_confirmar = $model->contrasena;
                            } else {
                                $model->contrasena_confirmar = $last_password;
                                $model->contrasena = $last_password;
                            }
                            if ($model->save()) {
                                
                                Yii::$app->session->setFlash('success', Yii::t('app', Yii::$app->params['message.success']));
                                return $this->redirect(['view', 'id' => $model->id_rbac_usuario]);
                            } else {
                                Yii::$app->session->setFlash('warning', Yii::t('app',Yii::$app->params['message.warning']));
                            }
                        } else {
                            Yii::$app->session->setFlash('warning', Yii::t('app',Yii::$app->params['message.validate']));
                        }
                    }
                    $model->contrasena = '';
                    return $this->render('update', [
                                'model' => $model                            
                    ]);
            } else {
                throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.paramsurl']));
            }
        } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }

        
        
    }

    public function actionDelete($id)
    {        
        if (Yii::$app->auth->check(Yii::$app->params['module'] , 'eliminar_usuario')) {
            if (is_numeric($id)) {
                try {
                    $model = $this->findModel($id);
                    if($model->delete()) {
                        Yii::$app->session->setFlash('success',Yii::t('app', Yii::$app->params['message.success.delete']));
                    } else {
                        Yii::$app->session->setFlash('warning', Yii::t('app',Yii::$app->params['message.danger']));
                    }
                    return $this->redirect(['index']);
               
                } catch (ErrorException $e) {
                    throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.runningApp']));
                }
            } else {
                throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.paramsurl']));
            }
        } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        } 
    }
    
    public function actionMenu($id = null) {

        if (Yii::$app->auth->check(Yii::$app->params['module'], 'menu_usuario')) {
            
            if (is_numeric($id)) {
                if($id != Yii::$app->user->identity->id_rbac_usuario) {
                    $model = $this->findModel($id);
                    $rollback = false;
                    $transaction = Yii::$app->db->beginTransaction();
                    $MenuData = RBACRolesOpciones::find()
                            ->where([
                                'm.estatus' => CActiveRecord::STATUS_ACTIVE,
                                't.estatus' => CActiveRecord::STATUS_ACTIVE,
                                'o.estatus' => CActiveRecord::STATUS_ACTIVE,
                                't.id_rbac_rol' => $model->id_rbac_rol
                            ])
                            ->alias('t')
                            ->joinWith('idRbacMenuOpcion o')
                            ->joinWith(['idRbacMenuOpcion.idMenu m'])
                            ->orderBy(['m.jerarquia' => SORT_ASC, 'm.orden' => SORT_ASC, 'o.orden' => SORT_ASC])
                            ->all();
                    $menuL1     = [];
                    $menuL2     = [];
                    $menuL3     = [];
                    $opcionL1   = [];
                    $opcionL2   = [];
                    $opcionL3   = [];

                    foreach ($MenuData as $row) {
                        if ($row->idRbacMenuOpcion->idMenu->nivel == 1 AND $row->idRbacMenuOpcion->jerarquia == 1) {
                            $menuL1[$row->idRbacMenuOpcion->idMenu->id_rbac_menu]['menu_id']      = $row->idRbacMenuOpcion->id_rbac_menu;
                            $menuL1[$row->idRbacMenuOpcion->idMenu->id_rbac_menu]['menu']         = $row->idRbacMenuOpcion->descripcion;
                            $menuL1[$row->idRbacMenuOpcion->idMenu->id_rbac_menu]['menuicon']     = $row->idRbacMenuOpcion->icono;
                            $menuL1[$row->idRbacMenuOpcion->idMenu->id_rbac_menu]['opcion_id']    = $row->id_rbac_rol_opcion;
                            $menuL1[$row->idRbacMenuOpcion->idMenu->id_rbac_menu]['value']        = TRUE;
                            $menuL1[$row->idRbacMenuOpcion->idMenu->id_rbac_menu]['jerarquia']    = $row->idRbacMenuOpcion->jerarquia;
                        }
                        if ($row->idRbacMenuOpcion->idMenu->nivel == 2 AND $row->idRbacMenuOpcion->jerarquia == 1) {
                            $menuL2[$row->idRbacMenuOpcion->idMenu->jerarquia][$row->idRbacMenuOpcion->idMenu->id_rbac_menu]['menu_id'] = $row->idRbacMenuOpcion->id_rbac_menu;
                            $menuL2[$row->idRbacMenuOpcion->idMenu->jerarquia][$row->idRbacMenuOpcion->idMenu->id_rbac_menu]['menu'] = $row->idRbacMenuOpcion->descripcion;
                            $menuL2[$row->idRbacMenuOpcion->idMenu->jerarquia][$row->idRbacMenuOpcion->idMenu->id_rbac_menu]['menuicon'] = $row->idRbacMenuOpcion->icono;
                            $menuL2[$row->idRbacMenuOpcion->idMenu->jerarquia][$row->idRbacMenuOpcion->idMenu->id_rbac_menu]['opcion_id'] = $row->id_rbac_rol_opcion;
                            $menuL2[$row->idRbacMenuOpcion->idMenu->jerarquia][$row->idRbacMenuOpcion->idMenu->id_rbac_menu]['value'] = TRUE;
                            $menuL2[$row->idRbacMenuOpcion->idMenu->jerarquia][$row->idRbacMenuOpcion->idMenu->id_rbac_menu]['jerarquia'] = $row->idRbacMenuOpcion->jerarquia;
                        }
                        if ($row->idRbacMenuOpcion->idMenu->nivel == 3 AND $row->idRbacMenuOpcion->jerarquia == 1) {
                            $menuL3[$row->idRbacMenuOpcion->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['menu_id'] = $row->idRbacMenuOpcion->id_rbac_menu;
                            $menuL3[$row->idRbacMenuOpcion->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['menu'] = $row->idRbacMenuOpcion->descripcion;
                            $menuL3[$row->idRbacMenuOpcion->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['menuicon'] = $row->idRbacMenuOpcion->icono;
                            $menuL3[$row->idRbacMenuOpcion->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['opcion_id'] = $row->id_rbac_rol_opcion;
                            $menuL3[$row->idRbacMenuOpcion->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['value'] = TRUE;
                            $menuL3[$row->idRbacMenuOpcion->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['jerarquia'] = $row->idRbacMenuOpcion-jerarquia;
                        }
                        if ($row->idRbacMenuOpcion->idMenu->nivel == 1 AND $row->idRbacMenuOpcion->jerarquia == 0) {
                            $opcionL1[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['menu_id'] = $row->idRbacMenuOpcion->id_rbac_menu;
                            $opcionL1[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['menu'] = $row->idRbacMenuOpcion->descripcion;
                            $opcionL1[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['menuicon'] = $row->idRbacMenuOpcion->icono;
                            $opcionL1[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['opcion_id'] = $row->id_rbac_rol_opcion;
                            $opcionL1[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['opcion'] = $row->idRbacMenuOpcion->opcion;
                            $opcionL1[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['value'] = TRUE;
                            $opcionL1[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['jerarquia'] = $row->idRbacMenuOpcion->jerarquia;
                        }
                        if ($row->idRbacMenuOpcion->idMenu->nivel == 2 AND $row->idRbacMenuOpcion->jerarquia == 0) {
                            $opcionL2[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['menu_id'] = $row->idRbacMenuOpcion->id_rbac_menu;
                            $opcionL2[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['menu'] = $row->idRbacMenuOpcion->descripcion;
                            $opcionL2[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['menuicon'] = $row->idRbacMenuOpcion->icono;
                            $opcionL2[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['opcion_id'] = $row->id_rbac_rol_opcion;
                            $opcionL2[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['opcion'] = $row->idRbacMenuOpcion->opcion;
                            $opcionL2[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['value'] = TRUE;
                            $opcionL2[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['jerarquia'] = $row->idRbacMenuOpcion->jerarquia;
                        }
                        if ($row->idRbacMenuOpcion->idMenu->nivel == 3 AND $row->idRbacMenuOpcion->jerarquia == 0) {
                            $opcionL3[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['menu_id'] = $row->idRbacMenuOpcion->id_rbac_menu;
                            $opcionL3[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['menu'] = $row->idRbacMenuOpcion->descripcion;
                            $opcionL3[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['menuicon'] = $row->idRbacMenuOpcion->icono;
                            $opcionL3[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['opcion_id'] = $row->id_rbac_rol_opcion;
                            $opcionL3[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['opcion'] = $row->idRbacMenuOpcion->opcion;
                            $opcionL3[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['value'] = TRUE;
                            $opcionL3[$row->idRbacMenuOpcion->idMenu->id_rbac_menu][$row->id_rbac_rol_opcion]['jerarquia'] = $row->idRbacMenuOpcion->jerarquia;
                        }
                    }
                    if(isset($_POST['Items'])){

                        foreach($_POST['Items'] as $key => $value){
                            $usuarioOpcionActivo = RBACRolesOpcionesUsuarios::find()
                                ->where([
                                    'id_rbac_usuario' => $model->id_rbac_usuario,
                                    'id_rbac_rol_opcion' => $key
                                ])
                                ->one();
                            if($usuarioOpcionActivo){

                                $connection = Yii::$app->getDb();
                                $sql = "DELETE FROM rbac_roles_opciones_usuarios WHERE id_rbac_rol_opcion_usuario = $usuarioOpcionActivo->id_rbac_rol_opcion_usuario";
                                $command = $connection->createCommand($sql);
                                $result = $command->queryAll(); 
                                $rollback = false;
                            } 
                        }         

                        $permisosRolInactivo = RBACRolesOpciones::find()
                                                ->select('id_rbac_rol_opcion')
                                                ->leftJoin('rbac_usuarios', 'rbac_usuarios.id_rbac_rol=rbac_roles_opciones.id_rbac_rol')
                                                ->where([
                                                    'NOT IN', 'id_rbac_rol_opcion', array_keys($_POST['Items'])
                                                ])
                                                ->andWhere(['id_rbac_usuario' => $model->id_rbac_usuario])
                                                ->all();

                        if($permisosRolInactivo){
                            foreach($permisosRolInactivo as $pri){
                                $usuarioOpcionInactivo = RBACRolesOpcionesUsuarios::find()
                                                            ->where([
                                                                'id_rbac_usuario' => $model->id_rbac_usuario,
                                                                'id_rbac_rol_opcion' => $pri->id_rbac_rol_opcion
                                                            ])
                                                            ->one();
                                
                                if($usuarioOpcionInactivo){
                                    $usuarioOpcionInactivo->estatus = 0;
                                }else{
                                    $usuarioOpcionInactivo = new RBACRolesOpcionesUsuarios();
                                    $usuarioOpcionInactivo->id_rbac_rol_opcion = $pri->id_rbac_rol_opcion;
                                    $usuarioOpcionInactivo->id_rbac_usuario = $model->id_rbac_usuario;
                                    $usuarioOpcionInactivo->estatus = 0;
                                }    

                                if($usuarioOpcionInactivo->validate()) {
                                    if(!$usuarioOpcionInactivo->save()){
                                        $rollback = true;
                                        break;
                                    }else{
                                        $rollback = false;
                                    }
                                } else {
                                    $rollback = true;
                                    break;
                                }
                            }
                        }   

                        if($rollback == FALSE) {
                            $transaction->commit();
                            Yii::$app->session->setFlash('success', Yii::t('app', Yii::$app->params['message.success']));
                        }                     

                    }

                    $usuariositems = RBACRolesOpcionesUsuarios::find()
                        ->alias('t')
                        ->where([
                            't.estatus' => \app\components\CActiveRecord::STATUS_INACTIVE,
                            't.id_rbac_usuario' => intval($model->id_rbac_usuario),
                        ])
                        ->all();
                    foreach ($usuariositems as $row) {
                        $jerarquia              = (int) $row->idRbacRolOpcion->idRbacMenuOpcion->idMenu->jerarquia;
                        $id_rbac_menu           = (int) $row->idRbacRolOpcion->idRbacMenuOpcion->idMenu->id_rbac_menu;
                        $id_rbac_menu_opcion    = (int) $row->id_rbac_rol_opcion;
                        if ($row->idRbacRolOpcion->idRbacMenuOpcion->idMenu->nivel == 1 AND $row->idRbacRolOpcion->idRbacMenuOpcion->jerarquia == 1) {
                            if (array_key_exists($id_rbac_menu, $menuL1)) {
                                $menuL1[$id_rbac_menu]['value'] = FALSE;
                            }
                        }
                        if ($row->idRbacRolOpcion->idRbacMenuOpcion->idMenu->nivel == 2 AND $row->idRbacRolOpcion->idRbacMenuOpcion->jerarquia == 1) {
                            if (array_key_exists($jerarquia, $menuL2) && array_key_exists($id_rbac_menu, $menuL2[$jerarquia])) {
                                $menuL2[$jerarquia][$id_rbac_menu]['value'] = FALSE;
                            }
                        }

                        if ($row->idRbacRolOpcion->idRbacMenuOpcion->idMenu->nivel == 3 AND $row->idRbacRolOpcion->idRbacMenuOpcion->jerarquia == 1) {
                            if (array_key_exists($jerarquia, $menuL3) && array_key_exists($id_rbac_menu, $menuL3[$jerarquia])) {
                                $menuL3[$jerarquia][$id_rbac_menu]['value'] = FALSE;
                            }
                        }

                        if ($row->idRbacRolOpcion->idRbacMenuOpcion->idMenu->nivel == 1 AND $row->idRbacRolOpcion->idRbacMenuOpcion->jerarquia == 0) {
                            if (array_key_exists($id_rbac_menu, $opcionL1) && array_key_exists($id_rbac_menu_opcion, $opcionL1[$id_rbac_menu])) {
                                $opcionL1[$id_rbac_menu][$id_rbac_menu_opcion]['value'] = FALSE;
                            }
                        }
                        if ($row->idRbacRolOpcion->idRbacMenuOpcion->idMenu->nivel == 2 AND $row->idRbacRolOpcion->idRbacMenuOpcion->jerarquia == 0) {
                            if (array_key_exists($id_rbac_menu, $opcionL2) && array_key_exists($id_rbac_menu_opcion, $opcionL2[$id_rbac_menu])) {
                                $opcionL2[$id_rbac_menu][$id_rbac_menu_opcion]['value'] = FALSE;
                            }
                        }
                        if ($row->idRbacRolOpcion->idRbacMenuOpcion->idMenu->nivel == 3 AND $row->idRbacRolOpcion->idRbacMenuOpcion->jerarquia == 0) {
                            if (array_key_exists($id_rbac_menu, $opcionL3) && array_key_exists($id_rbac_menu_opcion, $opcionL3[$id_rbac_menu])) {
                                $opcionL3[$id_rbac_menu][$id_rbac_menu_opcion]['value'] = FALSE;
                            }
                        }
                    }
                    $data = [];
                    $data['menuL1'] = $menuL1;
                    $data['menuL2'] = $menuL2;
                    $data['menuL3'] = $menuL3;
                    $data['opcionL1'] = $opcionL1;
                    $data['opcionL2'] = $opcionL2;
                    $data['opcionL3'] = $opcionL3;

                    return $this->render('menu', [
                                'data' => $data,
                                'model'=>$model
                    ]);
                } else {
                    throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
                }
            } else {
                throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
            }
        } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }
    }

    protected function findModel($id)
    {
        if (($model = RBACUsuarios::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
