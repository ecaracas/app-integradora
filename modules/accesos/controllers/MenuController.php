<?php

namespace app\modules\accesos\controllers;

use Yii;
use app\modules\accesos\models\RBACMenus;
use app\modules\accesos\models\RBACRoles;
use app\modules\accesos\models\RBACRolesOpciones;
use app\modules\accesos\models\search\RBACMenuSearch;
USE \app\modules\accesos\models\RBACMenuOpciones;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use yii\helpers\ArrayHelper;


class MenuController extends \app\components\CController
{

    public $moduleID = 'configuracion';

   
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['get'],
                    'view' => ['get'],
                    'create' => ['get', 'post'],
                    'update' => ['get', 'put', 'post'],
                    'updatemenu' => ['get', 'post'],
                    'updateorder' => ['get', 'put', 'post'],
                    'delete' => ['post', 'delete'],
                ],
            ],
        ];
    }

    public function __construct($id, $module, $config = array()) {

        Yii::$app->params['layout.title'] = Yii::t('app','configuration');

        Yii::$app->params['module'] = $this->moduleID;

       return parent::__construct($id, $module, $config);
    }

    
    public function actionIndex()
    {

        if (Yii::$app->auth->check(Yii::$app->params['module'] , 'listar_menu')) {


            $modelCreate = new RBACMenus();

            return $this->render('menu_', [
                'modelCreate' => $modelCreate,
            ]);

         } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }
    }


    public function actionView($id = null)
    {
         if (Yii::$app->auth->check(Yii::$app->params['module'] , 'ver_menu')) {

            if (is_numeric($id)) {

                try {
                    $model = $this->findModel($id);

                    return $this->render('view', [
                                'model' => $model
                    ]);
                } catch (ErrorException $e) {
                    throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.runningApp']));
                }
            } else {
                throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.paramsurl']));
            }
        } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }

    }

    
    public function actionListmenu()
    {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $categoria = Yii::$app->request->get('categoria');

                $model = $this->findMenus($categoria);

                foreach ($model as $key => $menu) :
                    $json[$key] =  (array) $menu->attributes;
                    if ($menu->rbacMenu):
                        foreach ($menu->rbacMenu as $keySubMenu => $subMenu):
                            $json[$key]['sub'][$keySubMenu] =  (array) $subMenu->attributes;
                        endforeach;
                    endif;
                endforeach;

                return $json;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }

    }

    
    public function actionCreate()
    {

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $model = new RBACMenus();
            if ($model->load(Yii::$app->request->post())) {
                $postValues = Yii::$app->request->post();
                try {
                        $model->url_tipo = 0;
                        $model->orden = 0;
                        $flagTipo = 1;
                        if ($model->validate()) {

                            if ($model->modulo) {
                                $menu = $this->findModel($model->modulo);
                                $model->jerarquia = $menu->id_rbac_menu;
                                $model->orden = $menu->menuMaxOrder;
                                $model->modulo = $menu->descripcion;
                                $model->nivel = 2;
                                $flagTipo = 2;
                            } else {
                                $model->modulo = strtolower($model->descripcion);
                                $model->nivel = 1;
                                $model->orden = $model->menuMaxOrder;
                                $model->jerarquia = 0;
                                $flagTipo = 1;
                            }

                            $model_opcion = new RBACMenuOpciones();
                            $model_opcion->url = '/';
                            $model_opcion->url_tipo = 0;
                            $model_opcion->categoria = $model->categoria;
                            $model_opcion->estatus = 1;
                            $model_opcion->icono='';
                            $model_opcion->jerarquia = 1;
                            $model_opcion->fecha_creado = date('YYYY-mm-dd H:ii:ss');
                            $model_opcion->creado_por = 0;
                            
                            if($postValues['RBACMenus']['rbacMenuOpciones'] == 0){
                                if ($model->save()) {
                                    if($flagTipo == 1){
                                        $model_opcion->id_rbac_menu = $model->id_rbac_menu;
                                        $model_opcion->descripcion = $model->descripcion;
                                        $model_opcion->opcion = strtolower($model->descripcion);
                                        $model_opcion->orden = 0;
                                        $model_opcion->jerarquia = 1; 
                                    }else{                        
                                        $model_opcion->id_rbac_menu = $model->id_rbac_menu;
                                        $model_opcion->descripcion = $model->descripcion;
                                        $model_opcion->opcion = $postValues['RBACMenus_opcion']['opcion'];
                                        $model_opcion->orden = $model_opcion->menuOpcionMaxOrder;
                                        $model_opcion->jerarquia = 1; 
                                    }
                                    if($model_opcion->save()){
                                        $rbacRoles = RBACRoles::find()->all();
                                        foreach($rbacRoles as $rr){
                                            $rbacRolOpcion = new RBACRolesOpciones();
                                            $rbacRolOpcion->id_rbac_rol = $rr->id_rbac_rol;
                                            $rbacRolOpcion->estatus = 0;
                                            $rbacRolOpcion->fecha_creado = date('YYYY-mm-dd H:ii:ss');
                                            $rbacRolOpcion->fecha_editado = date('YYYY-mm-dd H:ii:ss');
                                            $rbacRolOpcion->creado_por = 0;
                                            $rbacRolOpcion->editado_por = 0;
                                            $rbacRolOpcion->id_rbac_menu_opcion = $model_opcion->id_rbac_menu_opcion;
        
                                            $rbacRolOpcion->save();
                                        }
                                        return ['mensaje' => Yii::t('app', Yii::$app->params['message.success'])];
                                    }else{
                                        return ['mensaje' => Yii::t('app', Yii::$app->params['message.warning'])];
                                    }
                                } else {
                                    return ['mensaje' => Yii::t('app', Yii::$app->params['message.warning'])];
                                }
                            }else{ 
                                       
                                $model_opcion->id_rbac_menu = $postValues['RBACMenus']['rbacMenuOpciones'];
                                $model_opcion->descripcion = $model->descripcion;
                                $model_opcion->opcion = $postValues['RBACMenus_opcion']['opcion'];
                                $model_opcion->orden = 0;
                                $model_opcion->jerarquia = 0; 
                                
                                if($model_opcion->save()){
                                    $rbacRoles = RBACRoles::find()->all();
                                    foreach($rbacRoles as $rr){
                                        $rbacRolOpcion = new RBACRolesOpciones();
                                        $rbacRolOpcion->id_rbac_rol = $rr->id_rbac_rol;
                                        $rbacRolOpcion->estatus = 0;
                                        $rbacRolOpcion->fecha_creado = date('YYYY-mm-dd H:ii:ss');
                                        $rbacRolOpcion->fecha_editado = date('YYYY-mm-dd H:ii:ss');
                                        $rbacRolOpcion->creado_por = 0;
                                        $rbacRolOpcion->editado_por = 0;
                                        $rbacRolOpcion->id_rbac_menu_opcion = $model_opcion->id_rbac_menu_opcion;
        
                                        $rbacRolOpcion->save();
                                    }
                                    return ['mensaje' => Yii::t('app', Yii::$app->params['message.success'])];
                                }else{
                                    return ['mensaje' => Yii::t('app', Yii::$app->params['message.warning'])];
                                }
                            }

                        } else {
                            Yii::$app->response->statusCode = 500;
                            return \yii\widgets\ActiveForm::validate($model);
                        }

                } catch (ErrorException $e) {
                    throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.runningApp']));
                }
            }

    }

    public function actionUpdate($id)
    {

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if (is_numeric($id)) {

                $model = $this->findModel($id);

                if ($model->load(Yii::$app->request->post())) {

                    if ($model->validate()) {

                        if ($model->save()) {

                            return ['mensaje' => Yii::t('app', Yii::$app->params['message.success'])];

                        } else {

                            return ['mensaje' => Yii::t('app', Yii::$app->params['message.warning'])];

                        }
                    } else {
                        Yii::$app->response->statusCode = 500;
                        return \yii\widgets\ActiveForm::validate($model);//$model->errors;
                    }
                }
                return $this->render('update', [
                            'model' => $model
                ]);
            } else {
                throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.paramsurl']));
            }
    }

    public function actionUpdatemenu($id = null)
    {


            if (is_numeric($id)) {

                $model = $this->findModel($id);

                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                return (array) $model->attributes;

            } else {
                throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.paramsurl']));
            }



    }

    public function actionUpdateorder()
    {

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $menus = Yii::$app->request->post();
            foreach (json_decode($menus['menu']) as $key => $menu) {
                $parent = $this->findModel($menu->itemId);
                $parent->orden = $menu->itemOrder;
                $parent->jerarquia = 0;
                $parent->nivel = 1;
                $parent->save();
                if (isset($menu->children)) {
                    foreach ($menu->children as $key => $children) {
                        $child = $this->findModel($children->itemId);
                        $child->orden = $children->itemOrder;
                        $child->jerarquia = $menu->itemId;
                        $child->nivel = 2;
                        $child->save();
                    }
                }
            }

            return ['mensaje'=>Yii::t('app', Yii::$app->params['message.success'])];

        



    }
    
    public function actionDelete($id)
    {
       
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (is_numeric($id)) {

                try {

                    $model = $this->findModel($id);

                    if ($model->rbacMenu):
                        foreach ($model->rbacMenu as $keySubMenu => $subMenu):
                            $subMenu->delete();
                        endforeach;
                    endif;

                    if($model->delete()) {
                        return ['mensaje' => Yii::t('app', Yii::$app->params['message.success.delete'])];
                    } else {
                        return ['mensaje' => Yii::t('app', Yii::$app->params['message.danger'])];
                    }

                } catch (ErrorException $e) {
                    throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.runningApp']));
                }
            } else {
                throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.paramsurl']));
            }

    }

    public function actionModulos()
    {

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $categoria = Yii::$app->request->get('categoria');

                $modulo = $this->findMenus($categoria);

                $modulos = ArrayHelper::map($modulo, 'id_rbac_menu', 'descripcion');

                return $modulos;
            }
            throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.paramsurl']));
        
    }

    
    protected function findModel($id)
    {
        if (($model = RBACMenus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findMenus($categoria = null)
    {

        $model = RBACMenus::find()
                ->andWhere(['jerarquia'=>0])
                ->orderBy(['orden'=> SORT_ASC])
                ->andWhere(['<>','estatus','9'])
                ->andWhere(['categoria'=>$categoria?$categoria:''])
                ->all();

        if (($model) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionOpciones(){
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $id = Yii::$app->request->get('menu_id');

            return RBACMenuOpciones::find()
                    ->select("rbac_menu_opciones.id_rbac_menu as id_rbac_menu, rbac_menu_opciones.descripcion as descripcion")
                    ->leftJoin('rbac_menus', 'rbac_menus.id_rbac_menu=rbac_menu_opciones.id_rbac_menu')
                    ->andWhere(['rbac_menus.jerarquia' => $id])
                    ->andWhere(['rbac_menu_opciones.jerarquia' => 1])
                    ->orderBy(['rbac_menu_opciones.orden' => SORT_ASC])->all();
        }
    }
}
