<?php

namespace app\modules\accesos\controllers;

use Yii;
use app\modules\accesos\models\RBACRoles;
use app\modules\accesos\models\search\RBACRolesSearch;
use app\modules\accesos\models\RBACRolesOpciones;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\CActiveRecord;
use yii\filters\AccessControl;
use app\modules\accesos\models\RBACMenuOpciones;
use yii\web\HttpException;


class RolesController extends \app\components\CController {

    public $moduleID = 'configuracion';

    
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],

                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['get'],
                    'view' => ['get'],
                    'create' => ['get', 'post'],
                    'menu' => ['get', 'post'],
                    'update' => ['get', 'put', 'post'],
                    'delete' => ['post', 'delete'],
                ],
            ],
        ];
    }

    public function __construct($id, $module, $config = array()) {

        Yii::$app->params['layout.title'] = Yii::t('app', 'configuration');

        Yii::$app->params['module'] = $this->moduleID;

        return parent::__construct($id, $module, $config);
    }

    
    public function actionIndex($id = null){
        self::logNavegacion();
        if (Yii::$app->auth->check(Yii::$app->params['module'], 'listar_roles')) {

            if (intval($id) > 0) {
                Yii::$app->params['oficina'] = intval($id);
            }

            $searchModel = new RBACRolesSearch();

            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider
            ]);
        } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }
    }

   
    public function actionView($id = null) {
        self::logNavegacion();
        if (Yii::$app->auth->check(Yii::$app->params['module'], 'ver_rol')) {

            if (is_numeric($id)) {

                try {
                    $model = $this->findModel($id);

                    return $this->render('view', [
                                'model' => $model
                    ]);
                } catch (ErrorException $e) {
                    throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.runningApp']));
                }
            } else {
                throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.paramsurl']));
            }
        } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }
    }

    public function actionCreate() {
        self::logNavegacion();
        if (Yii::$app->auth->check(Yii::$app->params['module'], 'crear_rol')) {
            $model = new RBACRoles();
            $model->scenario='insert';

            $registro_anterior = $model->attributes;
            if ($model->load(Yii::$app->request->post())) {
                try {
                    if ($model->validate()) {
                        if ($model->save()) {
                            $registro_nuevo = $model->attributes;
                            Yii::$app->session->setFlash('success', Yii::t('app', Yii::$app->params['message.success']));
                            return $this->redirect(['roles/menu', 'id' => $model->id_rbac_rol]);
                        } else {
                            Yii::$app->session->setFlash('warning', Yii::$app->params['message.warning']);
                        }
                    } else {
                        Yii::$app->session->setFlash('warning', Yii::$app->params['message.validate']);
                    }
                } catch (ErrorException $e) {
                    throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.runningApp']));
                }
            }
            return $this->render('create', [
                        'model' => $model
            ]);
        } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }
    }

    
    public function actionUpdate($id = null) {
        self::logNavegacion();
        if (Yii::$app->auth->check(Yii::$app->params['module'], 'editar_rol')) {

            if (is_numeric($id)) {

                $model = $this->findModel($id);
                $model->scenario='update';
                if ($model->load(Yii::$app->request->post())) {

                    if ($model->validate()) {

                        if ($model->save()) {
                            Yii::$app->session->setFlash('success', Yii::t('app', Yii::$app->params['message.success']));
                            return $this->redirect(['view', 'id' => $model->id_rbac_rol]);
                        } else {
                            Yii::$app->session->setFlash('warning', Yii::$app->params['message.warning']);
                        }
                    } else {
                        Yii::$app->session->setFlash('warning', Yii::$app->params['message.validate']);
                    }
                }
                return $this->render('update', [
                            'model' => $model
                ]);
            } else {
                throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.paramsurl']));
            }
        } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }
    }

    public function actionDelete($id) {
        if (Yii::$app->auth->check(Yii::$app->params['module'] , 'eliminar_rol')) {
            if (is_numeric($id)) {
                try {
                    $model = $this->findModel($id);
                    $model->scenario='delete';
                    if($model->delete()) {
                        Yii::$app->session->setFlash('success',Yii::t('app', Yii::$app->params['message.success.delete']));
                    } else {
                        Yii::$app->session->setFlash('warning', Yii::t('app',Yii::$app->params['message.danger']));
                    }
                    return $this->redirect(['index']);

                } catch (ErrorException $e) {
                    throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.runningApp']));
                }
            } else {
                throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.paramsurl']));
            }
        } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }
    }

    protected function findModel($id) {
        if (($model = RBACRoles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionMenu($id = null) {
        self::logNavegacion();
        if (Yii::$app->auth->check(Yii::$app->params['module'], 'menu_rol')) {

            if (is_numeric($id)) {

                $model = $this->findModel($id);
                $roolback = false;
                $transaction = Yii::$app->db->beginTransaction();
                $MenuData = RBACMenuOpciones::find()
                        ->where([
                            'm.estatus' => CActiveRecord::STATUS_ACTIVE,
                            't.estatus' => CActiveRecord::STATUS_ACTIVE
                        ])
                        ->alias('t')
                        ->joinWith(['idMenu m'])
                        ->orderBy(['m.jerarquia' => SORT_ASC, 'm.orden' => SORT_ASC, 't.orden' => SORT_ASC])
                        ->all();

                $menuL1 = [];
                $menuL2 = [];
                $menuL3 = [];
                $opcionL1 = [];
                $opcionL2 = [];
                $opcionL3 = [];

                foreach ($MenuData as $row) {
                    if ($row->idMenu->nivel == 1 AND $row->jerarquia == 1) {
                        $menuL1[$row->idMenu->id_rbac_menu]['menu_id']      = $row->idMenu->id_rbac_menu;
                        $menuL1[$row->idMenu->id_rbac_menu]['menu']         = $row->idMenu->descripcion;
                        $menuL1[$row->idMenu->id_rbac_menu]['menuicon']     = $row->idMenu->icono;
                        $menuL1[$row->idMenu->id_rbac_menu]['opcion_id']    = $row->id_rbac_menu_opcion;
                        $menuL1[$row->idMenu->id_rbac_menu]['value']        = FALSE;
                        $menuL1[$row->idMenu->id_rbac_menu]['jerarquia']    = $row->idMenu->jerarquia;
                    }
                    if ($row->idMenu->nivel == 2 AND $row->jerarquia == 1) {
                        $menuL2[$row->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['menu_id'] = $row->idMenu->id_rbac_menu;
                        $menuL2[$row->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['menu'] = $row->descripcion;
                        $menuL2[$row->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['menuicon'] = $row->idMenu->icono;
                        $menuL2[$row->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['opcion_id'] = $row->id_rbac_menu_opcion;
                        $menuL2[$row->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['value'] = FALSE;
                        $menuL2[$row->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['jerarquia'] = $row->idMenu->jerarquia;
                    }
                    if ($row->idMenu->nivel == 3 AND $row->jerarquia == 1) {
                        $menuL3[$row->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['menu_id'] = $row->idMenu->id_rbac_menu;
                        $menuL3[$row->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['menu'] = $row->descripcion;
                        $menuL3[$row->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['menuicon'] = $row->idMenu->icono;
                        $menuL3[$row->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['opcion_id'] = $row->id_rbac_menu_opcion;
                        $menuL3[$row->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['value'] = FALSE;
                        $menuL3[$row->idMenu->jerarquia][$row->idMenu->id_rbac_menu]['jerarquia'] = $row->idMenu->parent_id;
                    }
                    if ($row->idMenu->nivel == 1 AND $row->jerarquia == 0) {
                        $opcionL1[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['menu_id'] = $row->id_rbac_menu;
                        $opcionL1[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['menu'] = $row->descripcion;
                        $opcionL1[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['menuicon'] = $row->icono;
                        $opcionL1[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['opcion_id'] = $row->id_rbac_menu_opcion;
                        $opcionL1[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['opcion'] = $row->opcion;
                        $opcionL1[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['value'] = FALSE;
                        $opcionL1[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['jerarquia'] = $row->jerarquia;
                    }
                    if ($row->idMenu->nivel == 2 AND $row->jerarquia == 0) {
                        $opcionL2[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['menu_id'] = $row->id_rbac_menu;
                        $opcionL2[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['menu'] = $row->descripcion;
                        $opcionL2[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['menuicon'] = $row->icono;
                        $opcionL2[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['opcion_id'] = $row->id_rbac_menu_opcion;
                        $opcionL2[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['opcion'] = $row->opcion;
                        $opcionL2[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['value'] = FALSE;
                        $opcionL2[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['jerarquia'] = $row->jerarquia;
                    }
                    if ($row->idMenu->nivel == 3 AND $row->jerarquia == 0) {
                        $opcionL3[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['menu_id'] = $row->id_rbac_menu;
                        $opcionL3[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['menu'] = $row->descripcion;
                        $opcionL3[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['menuicon'] = $row->icono;
                        $opcionL3[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['opcion_id'] = $row->id_rbac_menu_opcion;
                        $opcionL3[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['opcion'] = $row->opcion;
                        $opcionL3[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['value'] = FALSE;
                        $opcionL3[$row->idMenu->id_rbac_menu][$row->id_rbac_menu_opcion]['jerarquia'] = $row->jerarquia;
                    }
                }

                if(isset($_POST['Items'])){

                    RBACRolesOpciones::updateAll([
                            'estatus' => 0,
                            ],'id_rbac_rol = '.$model->id_rbac_rol);
                    foreach($_POST['Items'] as $a => $b) {
                        $rolOpcion = RBACRolesOpciones::find()
                            ->where([
                                'id_rbac_rol' => $model->id_rbac_rol,
                                'id_rbac_menu_opcion' => $a
                            ])
                            ->one();

                        if($rolOpcion) {
                            $rolOpcion->estatus = 1;
                        } else {
                            $rolOpcion = new RBACRolesOpciones();
                            $rolOpcion->id_rbac_menu_opcion = $a;
                            $rolOpcion->id_rbac_rol = $model->id_rbac_rol;
                            $rolOpcion->estatus = 1;
                        }
                        if($rolOpcion->validate()) {
                            if($rolOpcion->save()){
                            } else {
                                $roolback = true;
                            }
                        } else {
                            $roolback = true;
                        }
                    }
                    if($roolback == FALSE) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t('app', Yii::$app->params['message.success']));
                    }
                }

                $rolesitems = RBACRolesOpciones::find()
                    ->alias('t')
                    ->where([
                        't.estatus' => CActiveRecord::STATUS_ACTIVE,
                        'o.estatus' => CActiveRecord::STATUS_ACTIVE,
                        'r.estatus' => CActiveRecord::STATUS_ACTIVE,
                        't.id_rbac_rol' => intval($id)
                    ])
                    ->joinWith(['idRbacMenuOpcion o', 'idRbacRol r'])
                    ->all();

                foreach ($rolesitems as $row) {
                    $jerarquia              = (int) $row->idRbacMenuOpcion->idMenu->jerarquia;
                    $id_rbac_menu           = (int) $row->idRbacMenuOpcion->idMenu->id_rbac_menu;
                    $id_rbac_menu_opcion    = (int) $row->idRbacMenuOpcion->id_rbac_menu_opcion;
                    if ($row->idRbacMenuOpcion->idMenu->nivel == 1 AND $row->idRbacMenuOpcion->jerarquia == 1) {
                        if (array_key_exists($id_rbac_menu, $menuL1)) {
                            $menuL1[$id_rbac_menu]['value'] = TRUE;
                        }
                    }
                    if ($row->idRbacMenuOpcion->idMenu->nivel == 2 AND $row->idRbacMenuOpcion->jerarquia == 1) {
                        if (array_key_exists($jerarquia, $menuL2) && array_key_exists($id_rbac_menu, $menuL2[$jerarquia])) {
                            $menuL2[$jerarquia][$id_rbac_menu]['value'] = TRUE;
                        }
                    }

                    if ($row->idRbacMenuOpcion->idMenu->nivel == 3 AND $row->idRbacMenuOpcion->jerarquia == 1) {
                        if (array_key_exists($jerarquia, $menuL3) && array_key_exists($id_rbac_menu, $menuL3[$jerarquia])) {
                            $menuL3[$jerarquia][$id_rbac_menu]['value'] = TRUE;
                        }
                    }

                    if ($row->idRbacMenuOpcion->idMenu->nivel == 1 AND $row->idRbacMenuOpcion->jerarquia == 0) {
                        if (array_key_exists($id_rbac_menu, $opcionL1) && array_key_exists($id_rbac_menu_opcion, $opcionL1[$id_rbac_menu])) {
                            $opcionL1[$id_rbac_menu][$id_rbac_menu_opcion]['value'] = TRUE;
                        }
                    }
                    if ($row->idRbacMenuOpcion->idMenu->nivel == 2 AND $row->idRbacMenuOpcion->jerarquia == 0) {
                        if (array_key_exists($id_rbac_menu, $opcionL2) && array_key_exists($id_rbac_menu_opcion, $opcionL2[$id_rbac_menu])) {
                            $opcionL2[$id_rbac_menu][$id_rbac_menu_opcion]['value'] = TRUE;
                        }
                    }
                    if ($row->idRbacMenuOpcion->idMenu->nivel == 3 AND $row->idRbacMenuOpcion->jerarquia == 0) {
                        if (array_key_exists($id_rbac_menu, $opcionL3) && array_key_exists($id_rbac_menu_opcion, $opcionL3[$id_rbac_menu])) {
                            $opcionL3[$id_rbac_menu][$id_rbac_menu_opcion]['value'] = TRUE;
                        }
                    }
                }

                $data = [];
                $data['menuL1'] = $menuL1;
                $data['menuL2'] = $menuL2;
                $data['menuL3'] = $menuL3;
                $data['opcionL1'] = $opcionL1;
                $data['opcionL2'] = $opcionL2;
                $data['opcionL3'] = $opcionL3;

                return $this->render('menu', [
                            'data' => $data,
                            'model'=>$model
                ]);
            }
        } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }
    }

}
