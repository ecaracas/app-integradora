<?php

namespace app\modules\accesos\controllers;

use Yii;
use app\modules\accesos\models\RBACMenuOpciones;
use app\modules\accesos\models\search\RBACMenuOpcionesSearch;
use app\components\CController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\HttpException;

class MenuopcionesController extends CController
{

public $moduleID = 'configuracion';

    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['get'],
                    'view' => ['get'],
                    'create' => ['get', 'post'],
                    'update' => ['get', 'put', 'post'],
                    'delete' => ['post', 'delete'],
                ],
            ],
        ];
    }
    
        public function __construct($id, $module, $config = array()) {
        
        Yii::$app->params['layout.title'] = Yii::t('app','configuration'); 
        
        Yii::$app->params['module'] = $this->moduleID;
        
       return parent::__construct($id, $module, $config);
    }
    
    

    public function actionIndex()
    {
    
    if (Yii::$app->auth->check(Yii::$app->params['module'] , 'index')) {
    
        $searchModel = new RBACMenuOpcionesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider            
        ]);
        
        
         } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }
    }

    
    public function actionView($id = null)
    {
         if (Yii::$app->auth->check(Yii::$app->params['module'] , 'view')) {

            if (is_numeric($id)) {

                try {
                    $model = $this->findModel($id);

                    return $this->render('view', [
                                'model' => $model                                
                    ]);
                } catch (ErrorException $e) {
                    throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.runningApp']));
                }
            } else {
                throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.paramsurl']));
            }
        } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }       
        
    }

  
    public function actionCreate()
    {   
         if (Yii::$app->auth->check(Yii::$app->params['module'] , 'create')) {

            

                $model = new RBACMenuOpciones();

                if ($model->load(Yii::$app->request->post())) {
                
                    try {

                    if ($model->validate()) {

                        if ($model->save()) {

                            Yii::$app->session->setFlash('success', Yii::t('app', Yii::$app->params['message.success']));
                            return $this->redirect(['view', 'id' => $model->id_rbac_menu_opcion]);
                        } else {
                            Yii::$app->session->setFlash('warning', Yii::$app->params['message.warning']);
                        }
                    } else {
                        Yii::$app->session->setFlash('warning', Yii::$app->params['message.validate']);
                    }
                    
                    } catch (ErrorException $e) {
                throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.runningApp']));
            }
                }
                return $this->render('create', [
                            'model' => $model                            
                ]);
            
        } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }
        
    }

    public function actionUpdate($id = null)
    {
        
                if (Yii::$app->auth->check(Yii::$app->params['module'] , 'update')) {

            if (is_numeric($id)) {

                $model = $this->findModel($id);

                if ($model->load(Yii::$app->request->post())) {

                    if ($model->validate()) {

                        if ($model->save()) {

                            Yii::$app->session->setFlash('success', Yii::t('app', Yii::$app->params['message.success']));
                            return $this->redirect(['view', 'id' => $model->id_rbac_menu_opcion]);
                        } else {
                            Yii::$app->session->setFlash('warning', Yii::$app->params['message.warning']);
                        }
                    } else {
                        Yii::$app->session->setFlash('warning', Yii::$app->params['message.validate']);
                    }
                }
                return $this->render('update', [
                            'model' => $model                            
                ]);
            } else {
                throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.paramsurl']));
            }
        } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }

        
        
    }

    public function actionDelete($id)
    {        
        if (Yii::$app->auth->check(Yii::$app->params['module'] , 'delete')) {
            if (is_numeric($id)) {
                try {
                    $model = $this->findModel($id)->delete();
                    Yii::$app->session->setFlash('success', Yii::$app->params['message.success.delete']);
                    return $this->redirect(['index']);
                } catch (ErrorException $e) {
                    throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.runningApp']));
                }
            } else {
                throw new NotFoundHttpException(Yii::t('app', Yii::$app->params['message.paramsurl']));
            }
        } else {
            throw new HttpException(403, Yii::t('app', Yii::$app->params['message.403']));
        }
    }

    
    protected function findModel($id)
    {
        if (($model = RBACMenuOpciones::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
