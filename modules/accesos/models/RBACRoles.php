<?php

namespace app\modules\accesos\models;

use Yii;
use app\modules\contribuyentes\models\EstatusAdministracion;

/**
 * This is the model class for table "rbac_roles".
 *
 * @property double $id_rbac_rol
 * @property string $descripcion
 * @property string $estatus
 * @property string $fecha_creado
 * @property string $fecha_editado
 * @property string $fecha_eliminado
 * @property string $creado_por
 * @property string $editado_por
 * @property string $eliminado_por
 * @property double $principal
 * @property integer $id_ofi_oficina
 * @property string $nombre
 *
 * @property RbacRolesOpciones[] $rbacRolesOpciones
 * @property RbacUsuarios[] $rbacUsuarios
 */
class RBACRoles extends \app\components\CActiveRecord {

    public static function tableName() {
        return 'rbac_roles';
    }

    public static function getDb(){
        return Yii::$app->db2;
    }
    
    public function rules() {
        return [
            [['descripcion'], 'required'],
            ['descripcion', 'unique'],
            [['estatus', 'creado_por', 'editado_por', 'eliminado_por'], 'number'],
            [['fecha_creado', 'fecha_editado', 'fecha_eliminado'], 'safe'],
            [['descripcion', 'nombre'], 'string', 'max' => 100],
            ['estatus', 'in', 'range' => [0, 1], 'on' => ['update', 'insert']],
            ['estatus', 'in', 'range' => [9,9], 'on'=>'delete'],
        ];
    }

    public function attributeLabels() {
        return [
            'id_rbac_rol' => yii::t('app', 'id'),
            'descripcion' => yii::t('app', 'description'),
            'nombre' => yii::t('app', 'name'),
            'estatus' => yii::t('app', 'status'),
            'fecha_creado' => yii::t('app', 'created_date'),
            'fecha_editado' => yii::t('app', 'date_edited'),
            'fecha_eliminado' => yii::t('app', 'date_deleted'),
            'creado_por' => yii::t('app', 'created_by'),
            'editado_por' => yii::t('app', 'updated_by'),
            'eliminado_por' => yii::t('app', 'deleted_by'),
        ];
    }

    public function getRbacRolesOpciones() {
        return $this->hasMany(RBACRolesOpciones::className(), ['id_rbac_rol' => 'id_rbac_rol']);
    }

    public function getRbacUsuarios() {
        return $this->hasMany(RBACUsuarios::className(), ['id_rbac_rol' => 'id_rbac_rol']);
    }

}
