<?php

namespace app\modules\accesos\models;

use Yii;

/**
 * This is the model class for table "rbac_roles_opciones".
 *
 * @property double $id_rbac_rol_opcion
 * @property string $id_rbac_rol
 * @property string $estatus
 * @property string $fecha_creado
 * @property string $fecha_editado
 * @property string $fecha_eliminado
 * @property string $creado_por
 * @property string $editado_por
 * @property string $eliminado_por
 * 
 * @property RbacMenuOpciones $idRbacMenuOpcion
 * @property RbacRoles $idRbacRol
 * @property RbacRolesOpcionesUsuarios[] $rbacRolesOpcionesUsuarios
 */
class RBACRolesOpciones extends \app\components\CActiveRecord
{
  
    public static function tableName()
    {
        return 'rbac_roles_opciones';
    }

    
    public static function getDb(){
        return Yii::$app->db2;
    }
    
    public function rules()
    {
        return [
            [['id_rbac_rol', 'id_rbac_menu_opcion'], 'required'],
            [['id_rbac_rol', 'estatus', 'creado_por', 'editado_por', 'eliminado_por', 'id_rbac_menu_opcion'], 'number'],
            [['fecha_creado', 'fecha_editado', 'fecha_eliminado'], 'safe'],
            [['id_rbac_menu_opcion'], 'exist', 'skipOnError' => true, 'targetClass' => RBACMenuOpciones::className(), 'targetAttribute' => ['id_rbac_menu_opcion' => 'id_rbac_menu_opcion']],
            [['id_rbac_rol'], 'exist', 'skipOnError' => true, 'targetClass' => RBACRoles::className(), 'targetAttribute' => ['id_rbac_rol' => 'id_rbac_rol']],
        ];
    }

    
    public function attributeLabels()
    {
        return [
            'id_rbac_rol_opcion' => yii::t('app', 'id rbac rol opcion'),
            'id_rbac_rol' => yii::t('app', 'id rbac rol'),
            'estatus' => yii::t('app', 'status'),
            'fecha_creado' => yii::t('app', 'created_date'),
            'fecha_editado' => yii::t('app', 'date_edited'),
            'fecha_eliminado' => yii::t('app', 'date_deleted'),
            'creado_por' => yii::t('app', 'created_by'),
            'editado_por' => yii::t('app', 'updated_by'),
            'eliminado_por' => yii::t('app', 'deleted_by'),
        ];
    }

    
    public function getIdRbacMenuOpcion()
    {
        return $this->hasOne(RBACMenuOpciones::className(), ['id_rbac_menu_opcion' => 'id_rbac_menu_opcion']);//->joinWith(['idMenu']);
    }
    
    
    public function getIdMenu()
    {
        return $this->hasMany(RBACMenus::className(), ['id_rbac_menu' => 'id_rbac_menu'])->from(RBACMenuOpciones::tableName());
    }
    
    public function getIdRbacRol()
    {
        return $this->hasOne(RBACRoles::className(), ['id_rbac_rol' => 'id_rbac_rol']);
    }

    
    public function getRbacRolesOpcionesUsuarios()
    {
        return $this->hasMany(RbacRolesOpcionesUsuarios::className(), ['id_rbac_rol_opcion' => 'id_rbac_rol_opcion']);
    }
}
