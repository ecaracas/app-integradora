<?php

namespace app\modules\accesos\models;

use Yii;

/**
 * This is the model class for table "rbac_menus".
 *
 * @property double $id_rbac_menu
 * @property string $descripcion
 * @property string $url
 * @property string $url_tipo
 * @property string $categoria
 * @property string $estatus
 * @property string $orden
 * @property string $icono
 * @property string $modulo
 * @property double $jerarquia
 * @property string $fecha_creado
 * @property string $fecha_editado
 * @property string $fecha_eliminado
 * @property string $creado_por
 * @property string $editado_por
 * @property string $eliminado_por
 * @property integer $nivel
 *
 * @property RbacMenuOpciones[] $rbacMenuOpciones
 */
class RBACMenus extends \app\components\CActiveRecord
{
    public static function tableName()
    {
        return 'rbac_menus';
    }

    public function rules()
    {
        return [
            [['descripcion', 'url', 'url_tipo', 'categoria', 'orden'], 'required'],
            [['url_tipo', 'estatus', 'orden', 'jerarquia', 'creado_por', 'editado_por', 'eliminado_por'], 'number'],
            [['fecha_creado', 'fecha_editado', 'fecha_eliminado'], 'safe'],
            [['nivel'], 'integer'],
            [['descripcion'], 'string', 'max' => 100],
            ['descripcion', 'match',  'pattern' => '/^[a-zA-ZäÄëËïÏöÖüÜáéíóúÁÉÍÓÚñÑ ]*$/'],
            [['url'], 'string', 'max' => 255],
            [['categoria'], 'string', 'max' => 20],
            [['icono'], 'string', 'max' => 50],
            [['modulo'], 'string', 'max' => 30],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_rbac_menu' => yii::t('app', 'id rbac menu'),
            'descripcion' => yii::t('app', 'description'),
            'url' => yii::t('app', 'url'),
            'url_tipo' => yii::t('app', 'url tipo'),
            'categoria' => yii::t('app', 'category'),
            'estatus' => yii::t('app', 'status'),
            'orden' => yii::t('app', 'order'),
            'icono' => yii::t('app', 'icono'),
            'modulo' => yii::t('app', 'module'),
            'jerarquia' => yii::t('app', 'hierarchy'),
            'fecha_creado' => yii::t('app', 'created_date'),
            'fecha_editado' => yii::t('app', 'date_edited'),
            'fecha_eliminado' => yii::t('app', 'date_deleted'),
            'creado_por' => yii::t('app', 'created_by'),
            'editado_por' => yii::t('app', 'updated_by'),
            'eliminado_por' => yii::t('app', 'deleted_by'),
            'nivel' => yii::t('app', 'level'),
        ];
    }

    public function getRbacMenuOpciones()
    {
        return $this->hasMany(RBACMenuOpciones::className(), ['id_rbac_menu' => 'id_rbac_menu']);
    }

    public function getRbacMenu()
    {
        return $this->hasMany(RBACMenus::className(), ['jerarquia' => 'id_rbac_menu'])->andWhere(['<>','estatus',9])->orderBy(['orden'=> SORT_ASC]);
    }

    public function getRbacMenuParentId()
    {
        $menu = $this->hasMany(RBACMenus::className(), ['id_rbac_menu'=>'jerarquia'])->one();
        if ($menu) {
            return $menu->id_rbac_menu;
        }
    }

    public function getMenuMaxOrder()
    {   
        $menu = $this->hasMany(RBACMenus::className(), ['jerarquia' => 'id_rbac_menu'])->orderBy(['orden'=> SORT_DESC])->one();
        if ($menu) {
            return $menu->orden+1;
        }
        $menu = RBACMenus::find()->andWhere(['jerarquia' => 0,'categoria'=>$this->categoria])->orderBy(['orden'=> SORT_DESC])->one();
        if ($menu) {
            return $menu->orden+1;
        }
        return 1;
    }
    
    
    public static function getDb(){
        return Yii::$app->db2;
    } 
}
