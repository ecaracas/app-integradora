<?php

namespace app\modules\accesos\models;

use Yii;

/**
 * This is the model class for table "rbac_roles_opciones_usuarios".
 *
 * @property double $id_rbac_rol_opcion_usuario
 * @property string $id_rbac_usuario
 * @property string $id_rbac_rol_opcion
 * @property string $estatus
 * @property string $fecha_creado
 * @property string $fecha_editado
 * @property string $fecha_eliminado
 * @property string $creado_por
 * @property string $editado_por
 * @property string $eliminado_por
 *
 * @property RbacRolesOpciones $idRbacRolOpcion
 * @property RbacUsuarios $idRbacUsuario
 */
class RBACRolesOpcionesUsuarios extends \app\components\CActiveRecord
{
    
    public static function tableName()
    {
        return 'rbac_roles_opciones_usuarios';
    }

   
    public static function getDb(){
        return Yii::$app->db2;
    }

    
    public function rules()
    {
        return [
            [['id_rbac_usuario', 'id_rbac_rol_opcion'], 'required'],
            [['id_rbac_usuario', 'id_rbac_rol_opcion', 'estatus', 'creado_por', 'editado_por', 'eliminado_por'], 'number'],
            [['fecha_creado', 'fecha_editado', 'fecha_eliminado'], 'safe'],
            [['id_rbac_rol_opcion'], 'exist', 'skipOnError' => true, 'targetClass' => RBACRolesOpciones::className(), 'targetAttribute' => ['id_rbac_rol_opcion' => 'id_rbac_rol_opcion']],
            [['id_rbac_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => RBACUsuarios::className(), 'targetAttribute' => ['id_rbac_usuario' => 'id_rbac_usuario']],
        ];
    }

    
    public function attributeLabels()
    {
        return [
            'id_rbac_rol_opcion_usuario' => yii::t('app', 'id'),
            'id_rbac_usuario' => yii::t('app', 'username'),
            'id_rbac_rol_opcion' => yii::t('app', 'rol_opcion'),
            'estatus' => yii::t('app', 'estatus'),
            'fecha_creado' => yii::t('app', 'created_date'),
            'fecha_editado' => yii::t('app', 'date_edited'),
            'fecha_eliminado' => yii::t('app', 'date_deleted'),
            'creado_por' => yii::t('app', 'created_by'),
            'editado_por' => yii::t('app', 'updated_by'),
            'eliminado_por' => yii::t('app', 'deleted_by'),
        ];
    }

    
    public function getIdRbacRolOpcion()
    {
        return $this->hasOne(RBACRolesOpciones::className(), ['id_rbac_rol_opcion' => 'id_rbac_rol_opcion']);
    }

    
    public function getIdRbacUsuario()
    {
        return $this->hasOne(RBACUsuarios::className(), ['id_rbac_usuario' => 'id_rbac_usuario']);
    }
}
