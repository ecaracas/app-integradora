<?php

namespace app\modules\accesos\models;

use Yii;

/**
 * This is the model class for table "rbac_usuarios".
 *
 * @property double $id_rbac_usuario
 * @property string $id_rbac_rol
 * @property string $nombreusuario
 * @property string $email
 * @property string $nombre
 * @property string $apellido
 * @property string $estatus
 * @property string $dni_numero
 * @property double $telf_local_tipo
 * @property string $telf_local_numero
 * @property string $cambio_clave
 * @property string $lastlogin_time
 * @property string $dni_letra
 * @property string $fecha_creado
 * @property string $fecha_editado
 * @property string $fecha_eliminado
 * @property string $creado_por
 * @property string $editado_por
 * @property string $eliminado_por
 * @property string $id_rbac_ofi_oficina
 * @property string $contrasena
 * @property string $access_token
 * @property string $authkey
 *
 * @property RbacRolesOpcionesUsuarios[] $rbacRolesOpcionesUsuarios
 * @property RbacRoles $rol
 */
class RBACUsuarios extends \app\components\CActiveRecord
{

    public $contrasena_confirmar;
    public $fecha_eliminado;
    
    public static function tableName()
    {
        return 'rbac_usuarios';
    }

    public static function getDb(){
        return Yii::$app->db2;
    }


    
    public function rules()
    {
        return [
            [['id_rbac_rol', 'nombreusuario', 'email', 'dni_numero',  'telf_local_numero'], 'required'],
            [['contrasena'], 'required','on' => 'updated_password'],
            ['nombreusuario', 'unique'],
            [['nombreusuario', 'email'], 'trim'],
            ['nombreusuario', 'match', 'pattern' => '/^[a-z]\w*$/i'],
            [['contrasena', 'contrasena_confirmar'], 'required', 'on' => 'insert'],
            ['contrasena', 'string', 'min' => 8,'tooShort' => Yii::t('app','password_character')],
            ['contrasena_confirmar', 'compare', 'compareAttribute'=>'contrasena', 'skipOnEmpty' => true, 'message'=>Yii::t('app','password_equal')],
            ['contrasena', 'validateSecurity', 'on' => 'insert'],
            [['id_rbac_rol', 'estatus',  'cambio_clave', 'creado_por', 'editado_por', 'eliminado_por'], 'number'],
            [['lastlogin_time', 'fecha_creado', 'fecha_editado', 'fecha_eliminado'], 'safe'],
            [['nombreusuario', 'email', 'nombre', 'apellido', 'authkey'], 'string', 'max' => 100],
            ['email', 'unique'],
            ['email', 'email'],
            [['dni_letra', 'dni_numero'], 'unique', 'targetAttribute' => ['dni_letra', 'dni_numero'], 'message' => 'RIF ya se encuentra registrado.'],
            [['dni_numero'], 'number'],
            ['estatus', 'in', 'range' => [0,1], 'on' => ['insert','update']],
            ['estatus', 'in', 'range' => [9,9], 'on' => 'delete'],
            ['dni_letra', 'in', 'range' => ['V','E','G','J']],
            [['dni_letra'], 'string', 'max' => 1],
            [['id_rbac_rol'], 'exist', 'skipOnError' => true, 'targetClass' => RBACRoles::className(), 'targetAttribute' => ['id_rbac_rol' => 'id_rbac_rol']],
            ['id_rbac_rol', 'validRol'],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id_rbac_usuario' => Yii::t('app','id'),
            'id_rbac_rol' => Yii::t('app','rol'),
            'nombreusuario' => Yii::t('app','username'),
            'email' => Yii::t('app','email'),
            'nombre' => Yii::t('app','name'),
            'apellido' => Yii::t('app','last_name'),
            'estatus' => Yii::t('app','status'),
            'dni_numero' => Yii::t('app','tax_id_number'),
            'telf_local_numero' => Yii::t('app','phone'),
            'cambio_clave' => Yii::t('app','change_of_key'),
            'lastlogin_time' => Yii::t('app','last_login'),
            'dni_letra' => Yii::t('app','letter'),
            'fecha_creado' => Yii::t('app','created_date'),
            'fecha_editado' => Yii::t('app','date_edited'),
            'fecha_eliminado' => Yii::t('app','date_deleted'),
            'creado_por' => Yii::t('app','created_by'),
            'editado_por' => Yii::t('app','updated_by'),
            'eliminado_por' => Yii::t('app','authkey'),
            'contrasena' => Yii::t('app','password'),
            'access_token' => Yii::t('app','token'),
            'authkey' => Yii::t('app','authkey'),
        ];
    }

    public function validateSecurity($attribute, $params)
    {
        if ( ! preg_match('/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/', $this->$attribute) ) {
            $this->addError($attribute, Yii::t('app','password_insecure')); 
        }
    }

    public function setPassword($password) {
        $hash = Yii::$app->getSecurity()->generatePasswordHash($password);
        $this->contrasena = $hash;
        $this->contrasena_confirmar = $hash;
    }

    public function validRol($attribute, $params) {
        if (!$this->hasErrors()) {
            $rol = RBACRoles::find()->where(['id_rbac_rol' => $this->id_rbac_rol])->one();
            if ($rol->estatus != 1) {
                $this->addError('id_rbac_rol', yii::t('app', 'Rol inválido'));
            }
        }
    }

    public function getRbacRolesOpcionesUsuarios()
    {
        return $this->hasMany(RbacRolesOpcionesUsuarios::className(), ['id_rbac_usuario' => 'id_rbac_usuario']);
    }

    
    public function getRol()
    {
        return $this->hasOne(RBACRoles::className(), ['id_rbac_rol' => 'id_rbac_rol']);
    }
}
