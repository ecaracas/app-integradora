<?php

namespace app\modules\accesos\models;

use Yii;

/**
 * This is the model class for table "rbac_menu_opciones".
 *
 * @property double $id_rbac_menu_opcion
 * @property string $id_rbac_menu
 * @property string $descripcion
 * @property string $opcion
 * @property string $url
 * @property string $url_tipo
 * @property string $categoria
 * @property string $estatus
 * @property string $orden
 * @property string $icono
 * @property double $jerarquia
 * @property string $fecha_creado
 * @property string $fecha_editado
 * @property string $fecha_eliminado
 * @property string $creado_por
 * @property string $editado_por
 * @property string $eliminado_por
 *
 * @property RbacCatalogosRolesOpciones[] $rbacCatalogosRolesOpciones
 * @property RbacMenus $idRbacMenu
 */
class RBACMenuOpciones extends \app\components\CActiveRecord
{
    public static function tableName()
    {
        return 'rbac_menu_opciones';
    }

    public static function getDb(){
        return Yii::$app->db2;
    } 

    public function rules()
    {
        return [
            [['id_rbac_menu', 'descripcion', 'opcion', 'url', 'categoria', 'orden', 'fecha_creado', 'creado_por'], 'required'],
            [['id_rbac_menu', 'url_tipo', 'estatus', 'orden', 'jerarquia', 'creado_por', 'editado_por', 'eliminado_por'], 'number'],
            [['fecha_creado', 'fecha_editado', 'fecha_eliminado'], 'safe'],
            [['descripcion', 'opcion'], 'string', 'max' => 100],
            [['url'], 'string', 'max' => 255],
            [['categoria'], 'string', 'max' => 20],
            [['icono'], 'string', 'max' => 50],
            [['id_rbac_menu'], 'exist', 'skipOnError' => true, 'targetClass' => RBACMenus::className(), 'targetAttribute' => ['id_rbac_menu' => 'id_rbac_menu']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_rbac_menu_opcion' => yii::t('app', 'id rbac menu opcion'),
            'id_rbac_menu' => yii::t('app', 'id rbac menu'),
            'descripcion' => yii::t('app', 'description'),
            'opcion' => yii::t('app', 'option'),
            'url' => yii::t('app', 'url'),
            'url_tipo' => yii::t('app', 'url tipo'),
            'categoria' => yii::t('app', 'category'),
            'estatus' => yii::t('app', 'status'),
            'orden' => yii::t('app', 'orden'),
            'icono' => yii::t('app', 'icono'),
            'jerarquia' => yii::t('app', 'hierarchy'),
            'fecha_creado' => yii::t('app', 'created_date'),
            'fecha_editado' => yii::t('app', 'date_edited'),
            'fecha_eliminado' => yii::t('app', 'date_deleted'),
            'creado_por' => yii::t('app', 'created_by'),
            'editado_por' => yii::t('app', 'updated_by'),
                'eliminado_por' => yii::t('app', 'deleted_by'),
        ];
    }

    public function getRbacCatalogosRolesOpciones()
    {
        return $this->hasMany(RbacCatalogosRolesOpciones::className(), ['id_rbac_menu_opcion' => 'id_rbac_menu_opcion']);
    }

    public function getIdMenu()
    {
        return $this->hasOne(RBACMenus::className(), ['id_rbac_menu' => 'id_rbac_menu']);
    }
    
    public function getMenuOpcionMaxOrder()
    {
        $menu = $this->hasMany(RBACMenus::className(), ['jerarquia' => 'id_rbac_menu'])->orderBy(['orden'=> SORT_DESC])->one();
        if ($menu) {
            return $menu->orden+1;
        }
        $menu = RBACMenus::find()->andWhere(['jerarquia' => 0,'categoria'=>$this->categoria])->orderBy(['orden'=> SORT_DESC])->one();
        if ($menu) {
            return $menu->orden+1;
        }
        return 1;
    }
}
