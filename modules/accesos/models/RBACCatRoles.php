<?php

namespace app\modules\accesos\models;

use Yii;

/**
 * This is the model class for table "rbac_catalogos_roles".
 *
 * @property double $id_rbac_catalogo_rol
 * @property string $nombre
 * @property string $descripcion
 * @property double $estatus
 * @property string $fecha_creado
 * @property string $fecha_editado
 * @property string $fecha_eliminado
 * @property string $creado_por
 * @property string $editado_por
 * @property string $eliminado_por
 * @property string $principal
 *
 * @property RbacCatalogosRolesOpciones[] $rbacCatalogosRolesOpciones
 */
class RBACCatRoles extends \app\components\CActiveRecord
{
    public static function getDb(){
        return Yii::$app->db2;
    }

    public static function tableName()
    {
        return 'rbac_catalogos_roles';
    }

    public function rules()
    {
        return [
            [['nombre', 'fecha_creado', 'creado_por'], 'required'],
            [['estatus', 'creado_por', 'editado_por', 'eliminado_por', 'principal'], 'number'],
            [['fecha_creado', 'fecha_editado', 'fecha_eliminado'], 'safe'],
            [['nombre'], 'string', 'max' => 20],
            [['descripcion'], 'string', 'max' => 200],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id_rbac_catalogo_rol' => yii::t('app', 'id'),
            'nombre' => yii::t('app', 'name'),
            'descripcion' => yii::t('app', 'description'),
            'estatus' => yii::t('app', 'status'),
            'fecha_creado' => yii::t('app', 'created_date'),
            'fecha_editado' => yii::t('app', 'date_edited'),
            'fecha_eliminado' => yii::t('app', 'date_deleted'),
            'creado_por' => yii::t('app', 'created_by'),
            'editado_por' => yii::t('app', 'updated_by'),
            'eliminado_por' => yii::t('app', 'deleted_by'),
            'principal' => yii::t('app', 'main'),
        ];
    }

    public function getRbacCatalogosRolesOpciones()
    {
        return $this->hasMany(RbacCatalogosRolesOpciones::className(), ['id_rbac_catalogo_rol' => 'id_rbac_catalogo_rol']);
    }
}
