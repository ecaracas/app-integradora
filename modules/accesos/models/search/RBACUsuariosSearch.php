<?php

namespace app\modules\accesos\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\accesos\models\RBACUsuarios;


class RBACUsuariosSearch extends RBACUsuarios
{
   
    public function rules()
    {
        return [
            [['id_rbac_usuario', 'id_rbac_rol', 'estatus',  'cambio_clave', 'creado_por', 'editado_por', 'eliminado_por'], 'number'],
            [['nombreusuario', 'email', 'nombre', 'apellido', 'dni_numero', 'telf_local_numero', 'lastlogin_time', 'dni_letra', 'fecha_creado', 'fecha_editado', 'fecha_eliminado', 'contrasena', 'access_token', 'authkey'], 'safe'],
        ];
    }

   
    public function scenarios()
    {
        return Model::scenarios();
    }

    
    public function search($params)
    {
        $query = RBACUsuarios::find();
        $query->where(['NOT IN','id_rbac_usuario', Yii::$app->user->identity->id_rbac_usuario]);
        $query->andWhere(['<>','estatus',9]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'pagination' => [
                'pageSize' => Yii::$app->params['grid.pagination.pagesize'],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_rbac_usuario' => $this->id_rbac_usuario,
            'id_rbac_rol' => $this->id_rbac_rol,
            'estatus' => $this->estatus,
            'cambio_clave' => $this->cambio_clave,
            'lastlogin_time' => $this->lastlogin_time,
            'fecha_creado' => $this->fecha_creado,
            'fecha_editado' => $this->fecha_editado,
            'fecha_eliminado' => $this->fecha_eliminado,
            'creado_por' => $this->creado_por,
            'editado_por' => $this->editado_por,
            'eliminado_por' => $this->eliminado_por,
        ]);

        
        $query->andFilterWhere(['like', 'nombreusuario', $this->nombreusuario])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'apellido', $this->apellido])
            ->andFilterWhere(['like', 'dni_numero', $this->dni_numero])
            ->andFilterWhere(['like', 'telf_local_numero', $this->telf_local_numero])
            ->andFilterWhere(['like', 'dni_letra', $this->dni_letra])
            ->andFilterWhere(['like', 'contrasena', $this->contrasena])
            ->andFilterWhere(['like', 'access_token', $this->access_token])
            ->andFilterWhere(['like', 'authkey', $this->authkey]);

        return $dataProvider;
    }
}
