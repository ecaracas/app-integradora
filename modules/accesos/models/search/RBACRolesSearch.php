<?php

namespace app\modules\accesos\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\accesos\models\RBACRoles;


class RBACRolesSearch extends RBACRoles
{
    
    public function rules()
    {
        return [
            [['id_rbac_rol', 'estatus', 'creado_por', 'editado_por', 'eliminado_por', 'principal'], 'number'],
            [['descripcion', 'fecha_creado', 'fecha_editado', 'fecha_eliminado', 'nombre'], 'safe'],
        ];
    }

    
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = RBACRoles::find();
        $query->andWhere(['<>','estatus',9]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'pagination' => [
                'pageSize' => Yii::$app->params['grid.pagination.pagesize'],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_rbac_rol' => $this->id_rbac_rol,
            'estatus' => $this->estatus,
            'fecha_creado' => $this->fecha_creado,
            'fecha_editado' => $this->fecha_editado,
            'fecha_eliminado' => $this->fecha_eliminado,
            'creado_por' => $this->creado_por,
            'editado_por' => $this->editado_por,
            'eliminado_por' => $this->eliminado_por,
            'principal' => $this->principal,
        ]);

        $query->andFilterWhere(['ilike', 'descripcion', $this->descripcion])
            ->andFilterWhere(['ilike', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
