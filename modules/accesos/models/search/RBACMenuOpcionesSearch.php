<?php

namespace app\modules\accesos\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\accesos\models\RBACMenuOpciones;


class RBACMenuOpcionesSearch extends RBACMenuOpciones
{
    public function rules()
    {
        return [
            [['id_rbac_menu_opcion', 'id_rbac_menu', 'url_tipo', 'estatus', 'orden', 'jerarquia', 'creado_por', 'editado_por', 'eliminado_por'], 'number'],
            [['descripcion', 'opcion', 'url', 'categoria', 'icono', 'fecha_creado', 'fecha_editado', 'fecha_eliminado'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = RBACMenuOpciones::find();


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'pagination' => [
                'pageSize' => Yii::$app->params['grid.pagination.pagesize'],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_rbac_menu_opcion' => $this->id_rbac_menu_opcion,
            'id_rbac_menu' => $this->id_rbac_menu,
            'url_tipo' => $this->url_tipo,
            'estatus' => $this->estatus,
            'orden' => $this->orden,
            'jerarquia' => $this->jerarquia,
            'fecha_creado' => $this->fecha_creado,
            'fecha_editado' => $this->fecha_editado,
            'fecha_eliminado' => $this->fecha_eliminado,
            'creado_por' => $this->creado_por,
            'editado_por' => $this->editado_por,
            'eliminado_por' => $this->eliminado_por,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'opcion', $this->opcion])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'categoria', $this->categoria])
            ->andFilterWhere(['like', 'icono', $this->icono]);

        return $dataProvider;
    }
}
