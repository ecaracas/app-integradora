<?php

namespace app\modules\accesos\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\accesos\models\RBACMenus;


class RBACMenuSearch extends RBACMenus
{
    public function rules()
    {
        return [
            [['id_rbac_menu', 'url_tipo', 'estatus', 'orden', 'jerarquia', 'creado_por', 'editado_por', 'eliminado_por', 'nivel'], 'number'],
            [['descripcion', 'url', 'categoria', 'icono', 'modulo', 'fecha_creado', 'fecha_editado', 'fecha_eliminado'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = RBACMenus::find();


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'pagination' => [
                'pageSize' => Yii::$app->params['grid.pagination.pagesize'],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_rbac_menu' => $this->id_rbac_menu,
            'url_tipo' => $this->url_tipo,
            'estatus' => $this->estatus,
            'orden' => $this->orden,
            'jerarquia' => $this->jerarquia,
            'fecha_creado' => $this->fecha_creado,
            'fecha_editado' => $this->fecha_editado,
            'fecha_eliminado' => $this->fecha_eliminado,
            'creado_por' => $this->creado_por,
            'editado_por' => $this->editado_por,
            'eliminado_por' => $this->eliminado_por,
            'nivel' => $this->nivel,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'categoria', $this->categoria])
            ->andFilterWhere(['like', 'icono', $this->icono])
            ->andFilterWhere(['like', 'modulo', $this->modulo]);

        return $dataProvider;
    }
    
    public function searchMenu($params)
    {
        $query = RBACMenus::find();
        


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['orden' => SORT_ASC]],
             'pagination' => [
                'pageSize' => Yii::$app->params['grid.pagination.pagesize'],
            ],
        ]);
        
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        
        $dataProvider->sort->attributes['orden'] = [
            'asc' => ['orden' => SORT_ASC],
        ];

        $query->andFilterWhere([
            'id_rbac_menu' => $this->id_rbac_menu,
            'url_tipo' => $this->url_tipo,
            'estatus' => $this->estatus,
            'orden' => $this->orden,
            'jerarquia' => $this->jerarquia,
            'fecha_creado' => $this->fecha_creado,
            'fecha_editado' => $this->fecha_editado,
            'fecha_eliminado' => $this->fecha_eliminado,
            'creado_por' => $this->creado_por,
            'editado_por' => $this->editado_por,
            'eliminado_por' => $this->eliminado_por,
            'nivel' => $this->nivel,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'categoria', $this->categoria])
            ->andFilterWhere(['like', 'icono', $this->icono])
            ->andFilterWhere(['like', 'modulo', $this->modulo]);

        return $dataProvider;
    }
}
