<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="rbacmenu-opciones-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_rbac_menu_opcion') ?>

    <?= $form->field($model, 'id_rbac_menu') ?>

    <?= $form->field($model, 'descripcion') ?>

    <?= $form->field($model, 'opcion') ?>

    <?= $form->field($model, 'url') ?>


    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
