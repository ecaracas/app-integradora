<?php

use yii\helpers\Html;
use yii\grid\GridView;


$this->title = Yii::t('app',Yii::$app->params['text.admin']) . ' ' . 'rbacmenu opciones';
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->params['MenuModule'] = [
[
'label' => Yii::t('app',Yii::$app->params['text.create']) . ' '.'rbacmenu opciones', 
'url' => ['create'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'create')
]
];
?>
<div class="rbacmenu-opciones-index panel panel-default">

    <div class="panel-heading navbar-tool">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        <div class="menu-tool">
            <div class="menu-items">
                <?=  \app\models\MenuMain::menu(); ?>
            </div>
        </div>          
    </div>
    <div class="panel-body">
        
                            <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
        'columns' => [

                        'id_rbac_menu',
            'descripcion',
            'opcion',
            'url:url',
            'url_tipo:url',
            'categoria',
 [
                        'attribute' => 'estatus',
                        'filter' => $searchModel->getStatusList(),
                        'format' => 'raw',
                        'value' => function($model) {
                            return $model->getActive();
                        }
                    ],            'orden',
            'icono',
            'jerarquia',



            ['class' => 'yii\grid\DataColumn',
            'header' => Yii::t('app', Yii::$app->params['text.view']),
            'format' => 'raw',
            'visible' => Yii::$app->auth->check(Yii::$app->params['module'] , 'view'),
            'value' => function ($data) {
            return Html::a(Yii::$app->params['icon.view'], ['view', 'id' => $data->id_rbac_menu_opcion], ['class' =>Yii::$app->params['btn.view']]);
            },
            ],
            ['class' => 'yii\grid\DataColumn',
            'header' => Yii::t('app', Yii::$app->params['text.update']),
            'format' => 'raw',
            'visible' => Yii::$app->auth->check(Yii::$app->params['module'] , 'update'),
            'value' => function ($data) {
            return Html::a(Yii::$app->params['icon.update'], ['update', 'id' => $data->id_rbac_menu_opcion], ['class' => Yii::$app->params['btn.update']]);
            },
            ],
            ['class' => 'yii\grid\DataColumn',
            'header' => Yii::t('app', Yii::$app->params['text.delete']),
            'format' => 'raw',
            'visible' => Yii::$app->auth->check(Yii::$app->params['module'] , 'delete'),
            'value' => function ($data) {
            return Html::a(Yii::$app->params['icon.delete'], ['delete', 'id' => $data->id_rbac_menu_opcion], ['class' =>'option-delete '. Yii::$app->params['btn.delete']]);
            },
            ],







            ],
            ]); ?>
            
            
       <?php       
if (Yii::$app->auth->check(Yii::$app->params['module'], 'delete')) {
    \Yii::$app->params['show.modal.delete'] = true;
    \Yii::$app->params['show.modal.confirm'] = true;
}
?>
            
            
                    </div>
</div>