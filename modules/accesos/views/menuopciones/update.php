<?php

use yii\helpers\Html;


$this->title = Yii::t('app',Yii::$app->params['text.update']);$this->params['breadcrumbs'][] = ['label' => 'rbacmenu opciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_rbac_menu_opcion, 'url' => ['view', 'id' => $model->id_rbac_menu_opcion]];
$this->params['breadcrumbs'][] = Yii::t('app',Yii::$app->params['text.update']);

Yii::$app->params['MenuModule'] = [
[
'label' => Yii::t('app',Yii::$app->params['text.admin']) . ' '.'rbacmenu opciones', 
'url' => ['index'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'index')
],    
[
'label' => Yii::t('app',Yii::$app->params['text.create']) . ' '.'rbacmenu opciones', 
'url' => ['create'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'create')
],
[
'label' => Yii::t('app',Yii::$app->params['text.view']) . ' '.'rbacmenu opciones', 
'url' => ['view','id' => $model->id_rbac_menu_opcion],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'view')
]
];
?>
<div class="rbacmenu-opciones-update panel panel-default">
  <div class="panel-heading navbar-tool">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        <div class="menu-tool">
                <div class="menu-items">
                   <?=  \app\models\MenuMain::menu(); ?>
                </div>
            </div>          
    </div>
  <div class="panel-body">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    </div>
</div>