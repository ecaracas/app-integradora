<?php

use yii\helpers\Html;



$this->title = Yii::t('app',Yii::$app->params['text.create']) . ' ' . 'rbacmenu opciones';
$this->params['breadcrumbs'][] = ['label' => 'rbacmenu opciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app',Yii::$app->params['text.create']);

Yii::$app->params['MenuModule'] = [
[
'label' => Yii::t('app',Yii::$app->params['text.admin']) . ' '.'rbacmenu opciones', 
'url' => ['index'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'], 'index')
]
];
?>
<div class="rbacmenu-opciones-create panel panel-default">
<div class="panel-heading navbar-tool">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        <div class="menu-tool">
                <div class="menu-items">
                   <?=  \app\models\MenuMain::menu(); ?>
                </div>
            </div>          
    </div>
  <div class="panel-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    </div>
</div>
