<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


$this->title = Yii::t('app',Yii::$app->params['text.view']); $this->params['breadcrumbs'][] = ['label' => 'rbacmenu opciones', 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->params['MenuModule'] = [
[
'label' => Yii::t('app',Yii::$app->params['text.admin']) . ' '.'Rbacmenu Opciones', 
'url' => ['index'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'],'index')
],    
[
'label' => Yii::t('app',Yii::$app->params['text.create']) . ' '.'Rbacmenu Opciones', 
'url' => ['create'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'],'create')
],
[
'label' => Yii::t('app',Yii::$app->params['text.update']) . ' '.'Rbacmenu Opciones', 
'url' => ['update','id' => $model->id_rbac_menu_opcion],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'],'update')
]
];

?>
<div class="rbacmenu-opciones-view  panel panel-default">

    <div class="panel-heading navbar-tool">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        <div class="menu-tool">
            <div class="menu-items">
                <?=  \app\models\MenuMain::menu(); ?>
            </div>
        </div>          
    </div>
    <div class="panel-body">
        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
        ['attribute' => 'id_rbac_menu'],
['attribute' => 'descripcion'],
['attribute' => 'opcion'],
['attribute' => 'url'],
['attribute' => 'url_tipo'],
['attribute' => 'categoria'],
['attribute' => 'estatus', 'format' => 'raw','value' => $model->getActive()],
['attribute' => 'orden'],
['attribute' => 'icono'],
['attribute' => 'jerarquia'],
['attribute' => 'fecha_creado', 'visible' => Yii::$app->params['isAdmin']],
['attribute' => 'fecha_editado', 'visible' => Yii::$app->params['isAdmin']],
['attribute' => 'fecha_eliminado', 'visible' => Yii::$app->params['isAdmin']],
['attribute' => 'creado_por', 'visible' => Yii::$app->params['isAdmin']],
['attribute' => 'editado_por', 'visible' => Yii::$app->params['isAdmin']],
['attribute' => 'eliminado_por', 'visible' => Yii::$app->params['isAdmin']],
        ],
        ]) ?>
    </div>
</div>