<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


$this->title = Yii::t('app',Yii::$app->params['text.view']); $this->params['breadcrumbs'][] = ['label' => yii::t('app', 'roles'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->params['MenuModule'] = [
[
'label' => Yii::t('app',Yii::$app->params['text.admin']) . ' '.Yii::t('app', 'roles'), 
'url' => ['index'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'],'listar_roles')
],    
[
'label' => Yii::t('app',Yii::$app->params['text.create']) . ' '.Yii::t('app', 'rol'), 
'url' => ['create'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'],'crear_rol')
],
[
'label' => Yii::t('app',Yii::$app->params['text.update']) . ' '.Yii::t('app', 'rol'), 
'url' => ['update','id' => $model->id_rbac_rol],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'],'editar_rol')
]
];

?>
<div class="rbacroles-view  panel panel-default">

    <div class="panel-heading navbar-tool">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        <div class="menu-tool">
            <div class="menu-items">
                <?=  \app\models\MenuMain::menu(); ?>
            </div>
        </div>          
    </div>
    <div class="panel-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                ['attribute' => 'descripcion'],
                ['attribute' => 'estatus', 'format' => 'raw','value' => $model->getActive()],
                ['attribute' => 'fecha_creado', 'visible' => Yii::$app->params['isAdmin']],
                ['attribute' => 'fecha_editado', 'visible' => Yii::$app->params['isAdmin']],
                ['attribute' => 'fecha_eliminado', 'visible' => Yii::$app->params['isAdmin']],
                ['attribute' => 'creado_por', 'visible' => Yii::$app->params['isAdmin']],
                ['attribute' => 'editado_por', 'visible' => Yii::$app->params['isAdmin']],
                ['attribute' => 'eliminado_por', 'visible' => Yii::$app->params['isAdmin']],
            ],
        ]) ?>
    </div>
</div>