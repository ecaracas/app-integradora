<?php

use yii\helpers\Html;



$this->title = Yii::t('app',Yii::$app->params['text.create']) . ' ' . yii::t('app', 'rol');
$this->params['breadcrumbs'][] = ['label' => yii::t('app', 'rol'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app',Yii::$app->params['text.create']);

Yii::$app->params['MenuModule'] = [
[
'label' => Yii::t('app',Yii::$app->params['text.admin']) . ' '.yii::t('app', 'roles'), 
'url' => ['index'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'], 'listar_roles')
]
];
?>
<div class="rbacroles-create panel panel-default">
<div class="panel-heading navbar-tool">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        <div class="menu-tool">
                <div class="menu-items">
                   <?=  \app\models\MenuMain::menu(); ?>
                </div>
            </div>          
    </div>
  <div class="panel-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    </div>
</div>
