<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$menuL1 = $data['menuL1'];
$menuL2 = $data['menuL2'];
$menuL3 = $data['menuL3'];

$opcionL1 = $data['opcionL1'];
$opcionL2 = $data['opcionL2'];
$opcionL3 = $data['opcionL3'];
$this->title = Yii::t('app', 'access_rol');
?>
<div class="menu-index panel panel-default">

    <div class="panel-heading navbar-tool">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        <div class="menu-tool">
            <div class="menu-items">
                <?= \app\models\MenuMain::menu(); ?>
                <?= Html::a(Yii::t('app', Yii::$app->params['text.return']), ['/accesos/roles'],[ 'class'=>"btn btn-primary btn-sm cmenuhorizontal",'data-pjax' => '0'] ) ?>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <table class="table">
            <tr>
                <th><?= Yii::t('app', 'rol') ?></th>
                <td><?= $model->descripcion ?></td>
            </tr>
        </table>
    </div>
</div>
                <?= Yii::t('app', 'select_all') ?> <?= Html::checkbox('select_all', null, ['class' => 'cmenuhorizontal js-switch-demo', 'id' => 'chkmenu-all']) ?>
    
<?php $form = ActiveForm::begin(); ?>

<?php foreach ($data['menuL1'] as $m1) : ?>

    <?php
    $idM1 = $m1['menu_id'];
    $itemM1 = 'chk-' . $idM1;
    ?>
    <section class="box">
        <header class="panel_header">
            <h2 class="title pull-left"><?= $m1['menuicon'] . ' ' . $m1['menu'] ?></h2>
            <div class="actions panel_actions pull-right" >
                <?= Html::checkbox('Items['.$m1['opcion_id'].']', $m1['value'], ['class' => 'js-switch-demo main', 'id' => 'chkmenu-' . $idM1,'style'=>'display:none;']) ?>
            </div>
        </header>
        <?php if (array_key_exists((string)$m1['menu_id'],$opcionL1) or array_key_exists((string)$m1['menu_id'],$menuL2)) : ?>
        <div id="contmenu-<?= $idM1 ?>" class="content-body <?= $m1['value'] ? '' : 'hide' ?>">
            <div class="row">
                <div class="col-md-12">
                    <?php if (array_key_exists((string)$m1['menu_id'],$opcionL1)) : ?>
                        <ul class="list-group">
                            <?php foreach ($opcionL1[$m1['menu_id']] as $o1) : ?>
                                <li class="list-group-item">
                                    <?= Html::checkbox('Items['.$o1['opcion_id'].']', $o1['value'], ['class' => 'js-switch-demo ' . $itemM1, 'id' => 'chkopcion-' . $o1['opcion_id'],'style'=>'display:none;']) ?>
                                    <?= $o1['menu'] ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    <?php if (array_key_exists((string)$m1['menu_id'],$menuL2)) : ?>
                        <?php foreach ($menuL2[$m1['menu_id']] as $m2) : ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" style="position: relative;">
                                    <h3 class="panel-title"><?= $m2['menuicon'] . ' ' . $m2['menu'] ?></h3>
                                    <div class="actions panel_actions pull-right" style="margin:0 10px;">
                                        <?= Html::checkbox('Items['.$m2['opcion_id'].']', $m2['value'],['id'=>' option-'.$m2['opcion_id'],'class' => 'js-switch-demo all-1 ' . $itemM1,'style'=>'display:none;']) ?>
                                    </div>
                                </div>
                                <?php if (array_key_exists((string)$m2['menu_id'],$opcionL2)) : ?>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 opcion-menu-<?= $m2['opcion_id'] ?>">
                                                <ul class="list-group" style="border: none;box-shadow: none;margin-bottom: 0;">
                                                    <li class="list-group-item" style="border: none;">
                                                        <?= Html::checkbox('all', 1, ['class' => 'js-switch-demo all','style'=>'display:none;']) ?>
                                                        <?= Yii::t('app','all')  ?>
                                                    </li>
                                                    <?php foreach ($opcionL2[$m2['menu_id']] as $o2) : ?>
                                                        <li class="list-group-item opt" style="border: none;">
                                                            <?= Html::checkbox('Items['.$o2['opcion_id'].']', $o2['value'], ['class' => 'js-switch-demo','style'=>'display:none;']) ?>
                                                            <?= $o2['menu'] ?>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </section>


<?php endforeach; ?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
                <?= Html::submitButton(Yii::t('app',Yii::$app->params['text.save']), ['class' => Yii::$app->params['btn.save']]) ?> &nbsp;
                <?= Html::a(Yii::t('app', Yii::$app->params['text.cancel']), ['index'], ['class' => Yii::$app->params['btn.cancel']]) ?>
            </div>
    </div>
</div>



<?php ActiveForm::end(); ?>

<?php
$this->registerJs("
    var CMPLTADMIN_SETTINGS = window.CMPLTADMIN_SETTINGS || {};
    CMPLTADMIN_SETTINGS.ios7Switchery = function() {

        if ($('.js-switch-demo').length > 0) {

                var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch-demo'));
                var defaults = {
                    color             : '#17a0d9'
                  , secondaryColor    : '#dfdfdf'
                  , jackColor         : '#fff'
                  , jackSecondaryColor: null
                  , className         : 'switchery'
                  , disabled          : false
                  , disabledOpacity   : 0.5
                  , speed             : '0.5s'
                  , size              : 'large'
                }
                var count = 0;
                var colors = ['#f44336','#e91e63','#9c27b0','#673ab7','#3f51b5','#2196f3','#03a9f4','#00bcd4','#009688','#4caf50','#8bc34a','#cddc39','#ffeb3b','#ffc107','#ff9800','#ff5722','#795548','#9e9e9e','#607d8b','#000000'];
                elems.forEach(function(html) {
                    count = count + 1;
                    var size = 'small';
                    var color = colors[9];

                     var defaults = {
                        color             : color
                      , secondaryColor    : '#dfdfdf'
                      , jackColor         : '#fff'
                      , jackSecondaryColor: null
                      , className         : 'switchery'
                      , disabled          : false
                      , disabledOpacity   : 0.5
                      , speed             : '0.5s'
                      , size              : size
                    }

                  var switchery = new Switchery(html,defaults);
                });
        }

    };
     CMPLTADMIN_SETTINGS.ios7Switchery();

");

$this->registerJs("
    changeCheckbox = function(ev) {
        var aux = $(this).prop('checked');
            id_menu = $(this).attr('id'),
            fields = id_menu.split('-'),
            id_menu_num = fields[1];

        if($(this).prop( 'checked' )){
            $('#contmenu-' + id_menu_num).removeClass('hide');
            var elems = $('.chk-' + id_menu_num);
            elems.each(function(e,value) {
                $(value).prop('checked',!aux);
                $(value).trigger('click');
            });
        } else {
            $('#contmenu-' + id_menu_num).addClass('hide');
            var elems = $('.chk-' + id_menu_num);
            elems.each(function(e,value) {
                $(value).prop('checked',!aux);
                $(value).trigger('click');
            });
        }

        $('#contmenu-' + id_menu_num+' .all').each(function(e,value) {
            $(value).prop('checked',!aux);
            $(value).trigger('click');
        })
    };

    changeChkAll = function(ev) {
        if (check) {
            var aux = $(this).prop('checked');

            $(this).parent().parent().find('.opt input').each(function(e,value) {
                $(value).prop('checked',!aux);
                $(value).trigger('click');
            });
        }
    }

    changeChkAll1 = function(ev) {
        if (check) {
            var id_option = $(this).attr('id');
                fields = id_option.split('-'),
                id_menu_num = fields[1],
                aux = $(this).prop('checked'),

            $('.opcion-menu-'+id_menu_num).parent().parent().find('input').each(function(e,value) {
                $(value).prop('checked',!aux);
                $(value).trigger('click');
            });
        }
    }

    validaAll = function(option) {
        var aux = true;
        option.find('.opt input').each(function(e,value) {
            if(!$(value).prop('checked')){
                aux = false;
                return false;
            }
        })
        return aux;
    }

    var check = true;
    allCheckede = function() {
        var aux=true;

        $('.all').each(function(e,value) {
            aux = $(this).prop('checked');
            if (!validaAll($(this).parent().parent())) {
                $(value).prop('checked',true);
                changeSwitchery(value,false);
            } else {
                if (aux==false) {
                    $(value).prop('checked',false);
                    changeSwitchery(value);
                }
            }
        })
    }

    optCheckede = function() {
        var aux=true;

        $(this).parent().find('.all').each(function(e,value) {
            aux = $(this).prop('checked');
            if (!validaAll($(this).parent().parent())) {
                $(value).prop('checked',true);
                changeSwitchery(value,false);
            } else {
                if (aux==false) {
                    $(value).prop('checked',false);
                    changeSwitchery(value);
                }
            }
        });
    }

    changeSwitchery = function(value){
        check = false;
        $(value).click();
        check = true;
    }

    $('.main').change(changeCheckbox);
    $('.all').change(changeChkAll);
    $('.all-1').change(changeChkAll1);
    $('.opt').change(optCheckede);
    allCheckede();
");



?>
