<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use webvimark\extensions\GridPageSize;

$this->title = yii::t('app', 'roles');
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->params['MenuModule'] = [
    [
        'label' => Yii::t('app', Yii::$app->params['text.create']) . ' ' . yii::t('app', 'rol'),
        'url' => ['create'],
        'visible' => Yii::$app->auth->check(Yii::$app->params['module'], 'crear_rol')
    ]
];
?>
<div class="rbacroles-index panel panel-default">

    <div class="panel-heading navbar-tool">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        <div class="menu-tool">
            <div class="menu-items">
                <?= \app\models\MenuMain::menu(); ?>
            </div>
        </div>          
    </div>
    <div class="panel-body">

        <?php Pjax::begin(['timeout' => 7000]); ?>
        <?php echo \nterms\pagesize\PageSize::widget(); ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'descripcion',
                [
                    'attribute' => 'estatus',
                    'filter' => $searchModel->getStatusList(),
                    'format' => 'raw',
                    'value' => function($model) {
                        return $model->getActive();
                    }
                ],
                ['class' => 'yii\grid\DataColumn',
                    'header' => Yii::t('app', Yii::$app->params['text.view']),
                    'format' => 'raw',
                    'visible' => Yii::$app->auth->check(Yii::$app->params['module'], 'ver_rol'),
                    'value' => function ($data) {
                        return Html::a(Yii::$app->params['icon.view'], ['view', 'id' => $data->id_rbac_rol], ['class' => Yii::$app->params['btn.view']]);
                    },
                        ],
                        ['class' => 'yii\grid\DataColumn',
                            'header' => Yii::t('app', 'menu'),
                            'format' => 'raw',
                            'visible' => Yii::$app->auth->check(Yii::$app->params['module'], 'menu_rol'),
                            'value' => function ($data) {
                                return Html::a('<i class="fa fa-navicon"></i>', ['menu', 'id' => $data->id_rbac_rol], ['class' => Yii::$app->params['btn.view']]);
                            },
                                ],
                                ['class' => 'yii\grid\DataColumn',
                                    'header' => Yii::t('app', Yii::$app->params['text.update']),
                                    'format' => 'raw',
                                    'visible' => Yii::$app->auth->check(Yii::$app->params['module'], 'editar_rol'),
                                    'value' => function ($data) {
                                        return Html::a(Yii::$app->params['icon.update'], ['update', 'id' => $data->id_rbac_rol], ['class' => Yii::$app->params['btn.update']]);
                                    },
                                        ],
                                        ['class' => 'yii\grid\DataColumn',
                                            'header' => Yii::t('app', Yii::$app->params['text.delete']),
                                            'format' => 'raw',
                                            'visible' => Yii::$app->auth->check(Yii::$app->params['module'], 'eliminar_rol'),
                                            'value' => function ($data) {
                                                return Html::a(Yii::$app->params['icon.delete'], ['delete', 'id' => $data->id_rbac_rol], ['class' => 'option-delete ' . Yii::$app->params['btn.delete']]);
                                            },
                                                ],
                                            ],
                                        ]);
                                        ?>


                                        <?php
                                        if (Yii::$app->auth->check(Yii::$app->params['module'], 'eliminar_rol')) {
                                            \Yii::$app->params['show.modal.delete'] = true;
                                            \Yii::$app->params['show.modal.confirm'] = true;
                                        }
                                        ?>


                                        <?php Pjax::end(); ?>    </div>
</div>