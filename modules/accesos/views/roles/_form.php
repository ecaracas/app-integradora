<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="rbacroles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estatus')->checkbox(['label'=>'','class'=>'js-switch1'])->label('')?>

    

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app',Yii::$app->params['text.save']), ['class' => Yii::$app->params['btn.save']]) ?> &nbsp;
        <?= Html::a(Yii::t('app', Yii::$app->params['text.cancel']), ['index'], ['class' => Yii::$app->params['btn.cancel']]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
