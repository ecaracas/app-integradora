<?php

use yii\helpers\Html;


$this->title = Yii::t('app',Yii::$app->params['text.update']);$this->params['breadcrumbs'][] = ['label' => yii::t('app', 'rbacroles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_rbac_rol, 'url' => ['view', 'id' => $model->id_rbac_rol]];
$this->params['breadcrumbs'][] = Yii::t('app',Yii::$app->params['text.update']);

Yii::$app->params['MenuModule'] = [
[
'label' => Yii::t('app',Yii::$app->params['text.admin']) . ' '.yii::t('app', 'roles'), 
'url' => ['index'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'listar_roles')
],    
[
'label' => Yii::t('app',Yii::$app->params['text.create']) . ' '.yii::t('app', 'rol'), 
'url' => ['create'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'crear_rol')
],
[
'label' => Yii::t('app',Yii::$app->params['text.view']) . ' '.yii::t('app', 'rol'), 
'url' => ['view','id' => $model->id_rbac_rol],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'ver_rol')
]
];
?>
<div class="rbacroles-update panel panel-default">
  <div class="panel-heading navbar-tool">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        <div class="menu-tool">
                <div class="menu-items">
                   <?=  \app\models\MenuMain::menu(); ?>
                </div>
            </div>          
    </div>
  <div class="panel-body">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    </div>
</div>