<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\modules\accesos\models\RBACRoles;
?>

<div class="usuarios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
        $dataRoles = ArrayHelper::map(RBACRoles::find()->Where(['estatus' => 1])->orderBy('descripcion ASC')->asArray()->all(), 'id_rbac_rol', 'descripcion');

        echo $form->field($model, 'id_rbac_rol')->dropDownList($dataRoles, ['prompt' => Yii::t('app', 'select')]);
    ?>
    
    <?= $form->field($model, 'nombreusuario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellido')->textInput(['maxlength' => true]) ?>

                    <div class="col-sm-2 col-xs-4">
                    <?= $form->field($model, 'dni_letra')->dropDownList(['E' => 'E', 'G' => 'G', 'J' => 'J', 'V' => 'V'], ['prompt' => Yii::t('app', 'select')])->label('RIF'); //textInput(['maxlength' => true])->label('Rif') ?>

                </div>
                <div class="col-sm-10 col-xs-8">
                    <?= $form->field($model, 'dni_numero')->textInput(['maxlength' => 9, 'onkeypress' => 'return isNum(event)'])->label('&nbsp;') ?> 

                </div>

                    <?= $form->field($model, 'estatus')->dropDownList($model->getStatusList()); ?>
                    

  
                    <?= $form->field($model, 'telf_local_numero')->textInput(['maxlength' => 14, 'onkeypress' => 'return isNum(event)', 'data-mask' => Yii::$app->params['format.phone']])->label(\Yii::t('app','phone')) ?> 

               
   

    <?= $form->field($model, 'contrasena')->passwordInput(['maxlength' => true])->label($model->getAttributeLabel('contrasena').'<span class="desc">Por favor, escriba una contraseña con mínimo 8 caracteres alfanúmericos [Az-09] </span>') ?>
    <?= $form->field($model, 'contrasena_confirmar')->passwordInput(['maxlength' => true])->label(\Yii::t('app','confirm_password')) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app',Yii::$app->params['text.save']), ['class' => Yii::$app->params['btn.save']]) ?> &nbsp;
        <?= Html::a(Yii::t('app', Yii::$app->params['text.cancel']), ['index'], ['class' => Yii::$app->params['btn.cancel']]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

  <?php 
$this->registerJs(
        "
        $('#" . Html::getInputId($model, 'email') . "').inputmask({
            mask: '*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]',
            greedy: false,
            onBeforePaste: function (pastedValue, opts) {
              pastedValue = pastedValue.toLowerCase();
              return pastedValue.replace('mailto:', '');
            },
            definitions: {
              '*': {
                validator: \"[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]\",
                cardinality: 1,
                casing: 'lower'
              }
            }
          });
          
        
     "
);
   ?>