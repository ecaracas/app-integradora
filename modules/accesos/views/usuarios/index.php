<?php

use yii\helpers\Html;
use yii\grid\GridView;
use webvimark\extensions\GridPageSize;


$this->title = Yii::t('app', 'users');
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->params['MenuModule'] = [
    [
        'label' => Yii::t('app', 'new_user'),
        'url' => ['create'],
        'visible' => Yii::$app->auth->check(Yii::$app->params['module'], 'crear_usuario')
    ]
];
?>
<div class="usuarios-index panel panel-default">

    <div class="panel-heading navbar-tool">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        <div class="menu-tool">
            <div class="menu-items">
                <?= \app\models\MenuMain::menu(); ?>
            </div>
        </div>          
    </div>
    <div class="panel-body">
        <?php echo \nterms\pagesize\PageSize::widget(); ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'nombreusuario',
                'email:email',
                [
                    'attribute' => 'nombre',
                    'format' => 'raw',
                    'value' => function($model) {
                        return $model->nombre . ' ' . $model->apellido;
                    }
                ],
                [
                    'attribute' => 'estatus',
                    'filter' => $searchModel->getStatusList(),
                    'format' => 'raw',
                    'value' => function($model) {
                        return $model->getActive();
                    }
                ],
                'dni_numero',
                [
                    'attribute' => 'telf_local_numero',
                    'format' => 'raw',
                    'value' => function($model) {
                        return $model->telf_local_numero;
                    }
                ],
                ['class' => 'yii\grid\DataColumn',
                    'header' => Yii::t('app', Yii::$app->params['text.view']),
                    'format' => 'raw',
                    'visible' => Yii::$app->auth->check(Yii::$app->params['module'], 'ver_usuario'),
                    'value' => function ($data) {
                        return Html::a(Yii::$app->params['icon.view'], ['view', 'id' => $data->id_rbac_usuario], ['class' => Yii::$app->params['btn.view']]);
                    },
                        ],
                        ['class' => 'yii\grid\DataColumn',
                            'header' => Yii::t('app', 'menu'),
                            'format' => 'raw',
                            'visible' => Yii::$app->auth->check(Yii::$app->params['module'], 'menu_usuario'),
                            'value' => function ($data) {
                                return Html::a('<i class="fa fa-navicon"></i>', ['menu', 'id' => $data->id_rbac_usuario], ['class' => Yii::$app->params['btn.view']]);
                            },
                                ],
                                ['class' => 'yii\grid\DataColumn',
                                    'header' => Yii::t('app', Yii::$app->params['text.update']),
                                    'format' => 'raw',
                                    'visible' => Yii::$app->auth->check(Yii::$app->params['module'], 'editar_usuario'),
                                    'value' => function ($data) {
                                        return Html::a(Yii::$app->params['icon.update'], ['update', 'id' => $data->id_rbac_usuario], ['class' => Yii::$app->params['btn.update']]);
                                    },
                                        ],
                                        ['class' => 'yii\grid\DataColumn',
                                            'header' => Yii::t('app', Yii::$app->params['text.delete']),
                                            'format' => 'raw',
                                            'visible' => Yii::$app->auth->check(Yii::$app->params['module'], 'eliminar_usuario'),
                                            'value' => function ($data) {
                                                return Html::a(Yii::$app->params['icon.delete'], ['delete', 'id' => $data->id_rbac_usuario], [
                                                            'class' => 'option-delete ' . Yii::$app->params['btn.delete']]);
                                            },
                                                ],
                                            ],
                                        ]);
                                        ?>


                                        <?php
                                        if (Yii::$app->auth->check(Yii::$app->params['module'], 'eliminar_usuario')) {
                                            \Yii::$app->params['show.modal.delete'] = true;
                                            \Yii::$app->params['show.modal.confirm'] = true;
                                        }
                                        ?>


    </div>
</div>