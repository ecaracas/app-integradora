<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = Yii::t('app',Yii::$app->params['text.view']); $this->params['breadcrumbs'][] = ['label' => 'usuarios', 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->params['MenuModule'] = [
[
'label' => Yii::t('app','user_administration'), 
'url' => ['index'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'listar_usuarios')
],    
[
'label' => Yii::t('app','new_user'), 
'url' => ['create'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'crear_usuario')
],
[
'label' => Yii::t('app','update_user'), 
'url' => ['update','id' => $model->id_rbac_usuario],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'],'editar_usuario')
]
];

?>
<div class="usuarios-view  panel panel-default">

    <div class="panel-heading navbar-tool">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        <div class="menu-tool">
            <div class="menu-items">
                <?=  \app\models\MenuMain::menu(); ?>
            </div>
        </div>          
    </div>
    <div class="panel-body">
        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id_rbac_rol',
                'format' => 'raw',
                'value' => function($model) {
                    return $model->rol->descripcion;
                }
            ],
            ['attribute' => 'nombreusuario'],
            [
                'label' => Yii::t('app','name') . ' y ' . Yii::t('app','last_name'),
                'format' => 'raw',
                'value' => function($model) {
                    return $model->nombre. ' '.$model->apellido;
                }
            ],
            ['attribute' => 'email'],
            ['attribute' => 'estatus', 'format' => 'raw','value' => $model->getActive()],
            [
                'attribute' => 'dni_numero',
                'format' => 'raw',
                'value' => function($model) {
                    return $model->dni_letra. '-'.$model->dni_numero;
                }
            ],
            [
                'attribute' => 'telf_local_numero',
                'format' => 'raw',
                'value' => function($model) {
                    return $model->telf_local_numero;
                }
            ],
            ['attribute' => 'fecha_creado', 'visible' => Yii::$app->params['isAdmin']],
            ['attribute' => 'fecha_editado', 'visible' => Yii::$app->params['isAdmin']],
            ['attribute' => 'fecha_eliminado', 'visible' => Yii::$app->params['isAdmin']],
            ['attribute' => 'creado_por', 'visible' => Yii::$app->params['isAdmin']],
            ['attribute' => 'editado_por', 'visible' => Yii::$app->params['isAdmin']],
            ['attribute' => 'eliminado_por', 'visible' => Yii::$app->params['isAdmin']],
        ],
        ]) ?>
    </div>
</div>