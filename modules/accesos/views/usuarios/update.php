<?php

use yii\helpers\Html;


$this->title = Yii::t('app',Yii::$app->params['text.update']);$this->params['breadcrumbs'][] = ['label' => 'usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_rbac_usuario, 'url' => ['view', 'id' => $model->id_rbac_usuario]];
$this->params['breadcrumbs'][] = Yii::t('app',Yii::$app->params['text.update']);

Yii::$app->params['MenuModule'] = [
[
'label' => Yii::t('app','user_administration'), 
'url' => ['index'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'listar_usuarios')
],    
[
'label' => Yii::t('app','new_user'), 
'url' => ['create'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'crear_usuario')
],
[
'label' => Yii::t('app','detail_user'), 
'url' => ['view','id' => $model->id_rbac_usuario],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'ver_usuario')
]
];
?>
<div class="usuarios-update panel panel-default">
  <div class="panel-heading navbar-tool">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        <div class="menu-tool">
                <div class="menu-items">
                   <?=  \app\models\MenuMain::menu(); ?>
                </div>
            </div>          
    </div>
  <div class="panel-body">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    </div>
</div>