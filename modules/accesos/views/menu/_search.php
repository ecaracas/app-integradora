<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="rbacmenus-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_rbac_menu') ?>

    <?= $form->field($model, 'descripcion') ?>

    <?= $form->field($model, 'url') ?>

    <?= $form->field($model, 'url_tipo') ?>

    <?= $form->field($model, 'categoria') ?>


    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
