<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


$this->title = Yii::t('app',Yii::$app->params['text.admin']) . ' ' . 'menú';
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->params['MenuModule'] = [
    [
        'label' => Yii::t('app',Yii::$app->params['text.create']) . ' '.'menú', 
        'url' => ['create'],
        'template' => '<a href="#" id="crear" class="btn btn-primary btn-sm">{label}</a>',
    ]
];
?>

<div class="rbacmenus-index panel panel-default">
    <div class="panel-heading navbar-tool">
        <h3 class="panel-title" style=" height: 41px;"><?= Html::encode($this->title) ?></h3>
        <div class="menu-tool">
            <div class="menu-items">
                <?=  \app\models\MenuMain::menu(); ?>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="col-md-12">
            <?php
                $categorias = \app\modules\accesos\models\RBACMenus::find()->all();
               
                $categorias = ArrayHelper::map($categorias, 'categoria', 'categoria'); 
             ?>
            <ul class="nav nav-tabs primary">
                <?php $active = 1 ?>
                <?php $i=0;?>
                <?php foreach ($categorias as $key => $categoria): ?>
                <?php  
                $i=$i+1;
                $a='Menú'.' ' .$i;
                    ?>
                    <li class="<?= $active===1?'active':'' ?>">
                        <a href="#<?=$categoria ?>" data-toggle="tab" aria-expanded="<?= $active===1?1:0 ?>">
                            <?=$a;  ?>
                        </a>
                    </li>
                    <?php $active = 0 ?>
                <?php endforeach ?>
            </ul>
            <div class="tab-content primary" style="border: none;">
                <?php $active = 1 ?>
                <?php foreach ($categorias as $key => $categoria): ?>
                    <div class="tab-pane fade <?= $active===1?'active':'' ?> in" id="<?=$categoria ?>">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <ul id="menu-sortable-<?=$categoria ?>" class="uk-nestable" data-uk-nestable="{maxDepth:2}">

                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php $active = 0 ?>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>

<?= $this->render('_form_modal', ['model' => $modelCreate]) ?>


<?php $this->registerCssFile('@web/plugins/uikit/css/components/nestable.min.css'); ?>
<?php $this->registerCssFile('@web/plugins/uikit/css/uikit.min.css'); ?>
<?php $this->registerJsFile('@web/plugins/uikit/js/uikit.min.js', ['depends' => [\app\assets\AppAsset::className()]]); ?>
<?php $this->registerJsFile('@web/plugins/uikit/js/components/nestable.min.js', ['depends' => [\app\assets\AppAsset::className()]]); ?>


<?php  
$_csrf              = Yii::$app->request->getCsrfToken();
$text_cargando      = Yii::t('app', 'loading');
$categoria          = Html::getInputId($modelCreate, 'categoria');
$descripcion        = Html::getInputId($modelCreate, 'descripcion');
$icono              = Html::getInputId($modelCreate, 'icono');
$modulo             = Html::getInputId($modelCreate, 'modulo');
$url                = Html::getInputId($modelCreate, 'url');
$update_url         = Url::to(['menu/update']);
$create_url         = Url::to(['menu/create']);
$delete_url         = Url::to(['menu/delete']);
$updateOrder_url    = Url::to(['menu/updateorder']);
$listmenu_url       = Url::to(['menu/listmenu']);
$modulos_url        = Url::to(['menu/modulos']);
$updateMenu_url     = Url::to(['menu/updatemenu']);

$this->registerJs(<<<JS
    jQuery(function($) { 
        'use strict';
        var MENU = window.MENU || {};
        
        MENU._csrf =  '$_csrf';
        MENU.wloading = '$text_cargando';
        MENU.wCategory = '$categoria';
        MENU.wDescription = '$descripcion';
        MENU.wIcono = '$icono';
        MENU.wUrl = '$url';
        MENU.wModulo = '$modulo';
        MENU.wUpdateUrl = '$update_url/';
        MENU.wCreateUrl = '$create_url/';
        MENU.wDeleteUrl = '$delete_url/';
        MENU.wUpdateOrderUrl = '$updateOrder_url/';
        MENU.wListMenuUrl = '$listmenu_url/';
        MENU.wModulosUrl = '$modulos_url/';
        MENU.wUpdateMenuUrl = '$updateMenu_url/';
        
        MENU.navActive = $('.nav li.active a').attr('href').replace('#','');
        MENU.idInicial = '#menu-sortable';
        MENU.id = MENU.idInicial+'-'+MENU.navActive;
        MENU.modalID = '#modal-crear';
        MENU.formID = '#w0';
        MENU.formAction =  $(MENU.formID).attr('action');
        MENU.urlForm =  $(MENU.formID).attr('action');
        MENU.DeleteId =  null;
        MENU.menuSortable = $(MENU.id).data('nestable');


        MENU.create = function(elm){
            var elmDefault = {
                id:0,
                name:'Sin Nombre',
                order:0,
                parent:0,
                children:null
            }

            Object.assign(elmDefault, elm);

            var newElemento = MENU.element(elmDefault);
            $(MENU.id).append(newElemento);
            if (elm.children) {
                for (var i in elm.children){
                    MENU.subElement(newElemento,elm.children[i]);
                }
            }
        }

        MENU.loadingShow = function(){
            var elm = $('<div>',{id : 'loading'}).text(MENU.wloading+'...');
            $('.panel-body').append(elm);
        }

        MENU.loadingHidden = function(){
            $('#loading').remove();
        }

        MENU.element = function(obj){

            var elm = $('<li>',{
                'data-item' : obj.name,
                'data-item-id' : obj.id,
                'data-item-order' : obj.order,
                'data-item-parent' : obj.parent,
                class : 'uk-nestable-list-item' 
            });

            return elm.append($('<div>',{class : 'uk-nestable-item'})
                        .append($('<div>',{class : 'uk-nestable-handle'}))
                        .append($('<div>',{'data-nestable-action' : 'toggle'}))
                        .append($('<div>',{class : 'list-label'}).text(' ' +obj.name).prepend(obj.icono))
                        .append(MENU.options(obj))
            );
        }

        MENU.subElement = function(parent,obj){

            var elm = MENU.element(obj);

            var children = parent.find('.uk-nestable-list');

            if (children.length) {           
                return parent.find('.uk-nestable-list').append(elm);
            }

            parent.addClass('uk-parent');
            
            return parent.append($('<ul>',{ class : 'uk-nestable-list'}).append(elm));

        }
        
        MENU.options = function(obj){

            var elmDefault = {
                id:0,
            }

            Object.assign(elmDefault, obj);

            return $('<div>',{class : 'pull-right dropdown'}).append(
                $('<button>',{
                    class : 'btn btn-default dropdown-toggle',
                    'type' : 'button',
                    'data-toggle' : 'dropdown', 
                    'aria-haspopup' : 'true',
                    'aria-expanded' : 'false'
                }).append($('<span>',{class : 'caret'}))
            ).append(
                $('<ol>',{class : 'dropdown-menu'})
                    .append($('<li>').append($('<a>',{href : elmDefault.id,id : 'editar'}).text('Editar')))
                    .append($('<li>').append($('<a>',{href : elmDefault.id,id : 'eliminar'}).text('Eliminar')))
            );
        } 
        
        MENU.build = function(data){

            for (var i in data){
                var children = null;
                if (data[i].sub) {
                    children = data[i].sub.map(function(obj){ 
                        return {
                            id : obj.id_rbac_menu,
                            name : obj.descripcion,
                            order : obj.orden,
                            icono : '',
                            parent : obj.jerarquia
                        };
                    });
                }
                MENU.create({
                    id : data[i].id_rbac_menu,
                    name : data[i].descripcion,
                    order : data[i].orden,
                    icono : data[i].icono,
                    parent : data[i].jerarquia,
                    children : children
                });
            }

        }

        MENU.buildInicial = function(){
            
            $(MENU.id+' li').remove();
            $(MENU.id+' div').remove();

            $.ajax({
                type: 'GET',
                url: MENU.wListMenuUrl,
                data: {categoria:MENU.navActive},
                beforeSend: function() {
                    MENU.menuSortable = $(MENU.id).data('nestable');
                    MENU.loadingShow();
                },
                success: function(data) {
                    MENU.build(data); 
                    $(MENU.formID)[0].reset();
                },
                complete: function(){
                    MENU.loadingHidden();
                    MENU.menuSortable.collapseAll();
                }
            });

        }

        MENU.getModules = function(){
            var selectModulos = '#'+MENU.wModulo;
            $(selectModulos).parent().show()
            $.ajax({
                type: 'GET',
                url: MENU.wModulosUrl,
                data: {categoria:MENU.navActive},
                beforeSend: function() {
                    MENU.buildBeforeModules(selectModulos);
                },
                success: function(data) {
                    MENU.buildModules(data,selectModulos);
                },
                complete: function(){
                }
            });
        }

        MENU.removeModules = function(){
            $('#'+MENU.wModulo+' option').remove();
            $('#'+MENU.wModulo).parent().hide()
        }

        MENU.setCategory = function(){
            $('#rbacmenus-categoria').val(MENU.navActive);
        }

        MENU.buildModules = function(obj,selectModulos){
            if (!$.isEmptyObject(obj)) {
                
                $(selectModulos).prop("disabled", false).empty();
                $("<option/>", {"value": "","text": 'Todos'}).appendTo(selectModulos);
                $.each(obj, function(index,value){
                    $("<option/>", {
                        "value": index,
                        "text": value
                    }).appendTo(selectModulos);
                });
            }
        }

        MENU.buildBeforeModules = function(id){
            var v_cargando = 'Cargando';
            $(id).prop("disabled", true).empty();
            $("<option/>", {"value": "","text": v_cargando+"..."}).appendTo(id);

        }

        MENU.buttonCreateMenu = function(event){
            event.preventDefault();
            MENU.setCategory();
            MENU.getModules();
            MENU.buttonCancelAlert();
            MENU.urlForm = MENU.formAction+'/create/';
            $(MENU.formID).attr('action',MENU.urlForm);
            $(MENU.formID)[0].reset();
            $(MENU.modalID+' .modal-title').html('Crear');
            $(MENU.modalID).modal('toggle');
        }

        MENU.buttonDeleteMenu = function(event){
            event.preventDefault();
            MENU.buttonCancelAlert();
            var id = $(this).attr('href');
            
            MENU.DeleteId = id;

            var elm = $('<div>',{class:'collapse'}).append(
                $('<div>',{
                    class:'alert alert-danger alert-dismissible fade in',
                    role: 'alert'
                })
                .append($('<h4>').text('Advertencia'))
                .append($('<p>').text('Se eliminara el menu.'))
                .append($('<p>')
                    .append($('<button>',{type : 'button', class : 'btn btn-success',id : 'btn-alert-accept'}).text('Aceptar'))
                    .append($('<button>',{type : 'button', class : 'btn btn-default',id : 'btn-alert-cancel'}).text('Cancelar'))
                )
            );

            $('.collapse').remove();
            $("li[data-item-id='"+id+"']").append(elm);
            elm.show();

        }
        
        MENU.buttonAcceptAlert = function(){
            $.ajax({
                type: 'POST',
                url: MENU.wDeleteUrl+MENU.DeleteId,
                data:{ _method : 'DELETE',_csrf : MENU._csrf},
                success: function(data) {
                    showSuccess(data.mensaje);
                    MENU.buildInicial(MENU.navActive);
                },
                complete: function(){
                    MENU.buttonCancelAlert();
                }
            });
        }

        MENU.buttonCancelAlert = function(){
            $('.collapse').remove();
            MENU.DeleteId = null;
        }

        MENU.buttonUpdateMenu = function(event) {

            event.preventDefault();

            var id = $(this).attr('href');

            MENU.setCategory();
            MENU.removeModules();
            MENU.urlForm = MENU.wUpdateUrl+id;
            $(MENU.formID).attr('action',MENU.urlForm);
            $.ajax({
                type: 'GET',
                url: MENU.wUpdateMenuUrl+id,
                success: function(data) {
                    
                    $(MENU.modalID+' .modal-title').html('Editar')

                    $('#'+MENU.wDescription).val(data.descripcion);
                    $('#'+MENU.wUrl).val(data.url);
                    $('#'+MENU.wCategory).val(data.categoria);
                    $('#'+MENU.wIcono).val(data.icono);
                    $('#'+MENU.wModulo).val(data.modulo);

                },
                complete: function(){
                    $(MENU.modalID).modal('toggle');
                }
            });
        
        }

        MENU.save = function() {
            
            $(MENU.formID).attr('action',MENU.urlForm);
            var form = $(this);

            if (form.find('.has-error').length) {
                return false;
            }
            $.ajax({
                type: 'POST',
                url: form.attr('action'),
                data: form.serialize(),
                success: function(data) {
                    showSuccess(data.mensaje);
                    $(MENU.modalID).modal('toggle');

                    MENU.buildInicial(MENU.navActive);
                },
                error: function(res) {
                    if (res.status==500) {
                        res.responseText = JSON.parse(res.responseText);

                        for (var i in res.responseText){
                            $('.field-'+i).addClass('has-error');
                            $('.field-'+i).removeClass('has-success');
                            $('.field-'+i).find('.help-block').html(res.responseText[i]);
                        }
                    }
                }
            });
            return false;
        }

        MENU.saveChangeOrder = function(ev,a) {
            var str = ''
            var dato = [];
            var nuevo = $(MENU.id).data('nestable').serialize();

            $.each($(MENU.id).data('nestable').serialize(), function(i, obj) {
           
                nuevo[i].itemOrder=i+1;

                var auxId = obj.itemId;

                if (obj.children) {
                    $.each(obj.children, function(j, obj) {
                        if (nuevo[i].children) { 
                            nuevo[i].children[j].itemOrder = j+1;
                            nuevo[i].parent = auxId;
                        } 

                    });
                }
            });
            console.log(nuevo);
            $.ajax({
                type: 'post',
                url: MENU.wUpdateOrderUrl,
                data: {_csrf:MENU._csrf,'menu':JSON.stringify(nuevo)},
                success: function(data) {
                    showSuccess(data.mensaje);
                },
                complete: function(){
                }
            });
        }
        
        $(document).ready(function() {
            
            MENU.buildInicial(MENU.navActive);

            $('ul[id *='+MENU.idInicial.replace('#','')+' ]').on('change.uk.nestable',MENU.saveChangeOrder);

            $(document).on('click','#crear',MENU.buttonCreateMenu); 
            $(document).on('click','#editar',MENU.buttonUpdateMenu); 
            $(document).on('click','#eliminar',MENU.buttonDeleteMenu); 
            $(document).on('click','#btn-alert-accept',MENU.buttonAcceptAlert); 
            $(document).on('click','#btn-alert-cancel',MENU.buttonCancelAlert);
            $(document).on('beforeSubmit',MENU.formID,MENU.save).on('submit', function(e){
                e.preventDefault();
            });

            $(MENU.formID).on('shown.bs.modal', function () {
            });
            $(MENU.formID).on('hidden.bs.modal', function () {
            });

            $('.nav li').on('shown.bs.tab', function (e) {
                var category = $(e.target).attr('href').replace('#','');
                MENU.id = MENU.idInicial+'-'+category;
                MENU.navActive = category;
                MENU.buildInicial(category);
            })
        });
    });
JS
);
?>
<?php 
$crsf_token =  Yii::$app->request->getCsrfToken();
$this->registerJs(<<<JS
    jQuery(function($) {
        $('#rbacmenus-modulo').change(function(){
            if($('#rbacmenus-modulo').val() != ''){
                $.ajax({
                    type:'GET',
                    url:'menu/opciones',   
                    data:{menu_id: $('#rbacmenus-modulo').val(), _csrf: '$crsf_token'},                    
                    dataType:'json',
                    contentType: "application/json; charset=utf-8",    
                }).done(function(response){
                    $('#rbacmenus-rbacmenuopciones').get(0).options.length = 0;
                    $('#rbacmenus-rbacmenuopciones').get(0).options[0] = new Option("Seleccione", "0");        
                    
                    $.each(response, function(key,value) {
                        $('#rbacmenus-rbacmenuopciones').append('<option value=' + value.id_rbac_menu + '>' + value.descripcion + '</option>');
                    });
                })
                $('#menu_opciones').show();
            }else{               
                $('#rbacmenus-rbacmenuopciones').val("0");
                $('#menu_opciones').hide();
            }
        });
    });
JS
);
?>