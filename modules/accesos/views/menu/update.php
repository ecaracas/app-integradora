<?php

use yii\helpers\Html;


$this->title = Yii::t('app',Yii::$app->params['text.update']);$this->params['breadcrumbs'][] = ['label' => 'rbacmenuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_rbac_menu, 'url' => ['view', 'id' => $model->id_rbac_menu]];
$this->params['breadcrumbs'][] = Yii::t('app',Yii::$app->params['text.update']);

Yii::$app->params['MenuModule'] = [
[
'label' => Yii::t('app',Yii::$app->params['text.admin']) . ' '.'rbacmenus', 
'url' => ['index'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'index')
],    
[
'label' => Yii::t('app',Yii::$app->params['text.create']) . ' '.'rbacmenus', 
'url' => ['create'],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'create')
],
[
'label' => Yii::t('app',Yii::$app->params['text.view']) . ' '.'rbacmenus', 
'url' => ['view','id' => $model->id_rbac_menu],
'visible'=>Yii::$app->auth->check(Yii::$app->params['module'] , 'view')
]
];
?>
<div class="rbacmenus-update panel panel-default">
  <div class="panel-heading navbar-tool">
        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        <div class="menu-tool">
                <div class="menu-items">
                   <?=  \app\models\MenuMain::menu(); ?>
                </div>
            </div>          
    </div>
  <div class="panel-body">


    <?= $this->render('_form_modal', [
        'model' => $model,
    ]) ?>

    </div>
</div>