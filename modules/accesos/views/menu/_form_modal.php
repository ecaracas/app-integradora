<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\accesos\models\RBACMenus;
use yii\helpers\ArrayHelper;

?>

<?php $form = ActiveForm::begin(); ?>


<div class="modal fade col-xs-12" id="modal-crear"  tabindex="-1" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Crear Menu</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="rbacmenus-form">

                        <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'categoria')->hiddenInput()->label(false); ?>

                        <?= $form->field($model, 'modulo', ['labelOptions' => [ 'class' => 'control-label']])->dropDownList([],['prompt' =>  yii::t('app', 'all')]);
                        ?>

                        <?= $form->field($model, 'icono')->textInput(['maxlength' => true]) ?>

                        <div style="display: none;" id="menu_opciones">
                            <div class="form-group">
                                <label class="control-label" for="rbacmenus-descripcion">Sub Menú</label>
                                <?= $form->field($model, 'rbacMenuOpciones')->dropDownList(ArrayHelper::map($model->getRbacMenuOpciones(),'id_rbac_menu','descripcion')); ?>
                                <div class="help-block"></div>
                            </div>
                            <div class="form-group field-rbacmenus-descripcion required">
                                <label class="control-label" for="rbacmenus-descripcion">Sub Menú Opción</label>
                                <input type="text" id="rbacmenus_opcion-opcion" class="form-control" name="RBACMenus_opcion[opcion]" maxlength="100" aria-required="true" aria-invalid="true">
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app',Yii::$app->params['text.save']), ['class' => Yii::$app->params['btn.save'],'id'=>'modal-crear-guardar']) ?> &nbsp;
                    <?= Html::a(Yii::t('app', Yii::$app->params['text.cancel']), ['index'], ['class' => Yii::$app->params['btn.cancel'],'data-dismiss'=>"modal"]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>