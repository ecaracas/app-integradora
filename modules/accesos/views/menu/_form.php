<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="rbacmenus-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url_tipo')->textInput() ?>

    <?= $form->field($model, 'categoria')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estatus')->textInput() ?>

    <?= $form->field($model, 'orden')->textInput() ?>

    <?= $form->field($model, 'icono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'modulo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jerarquia')->textInput() ?>

    <?= $form->field($model, 'nivel')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app',Yii::$app->params['text.save']), ['class' => Yii::$app->params['btn.save']]) ?> &nbsp;
        <?= Html::a(Yii::t('app', Yii::$app->params['text.cancel']), ['index'], ['class' => Yii::$app->params['btn.cancel']]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
