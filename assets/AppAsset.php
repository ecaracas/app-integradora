<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'plugins/switchery/dist/switchery.css',
        'plugins/jquery-ui/smoothness/jquery-ui.min.css',
        'plugins/select2/select2.css',
        'css/bootstrap-chosen.css',
        'plugins/pace/pace-theme-flash.css',
        'plugins/bootstrap/css/bootstrap.min.css',
        'plugins/bootstrap/css/bootstrap-theme.min.css',
        'fonts/font-awesome/css/font-awesome.css',
        'css/animate.min.css',
        'plugins/perfect-scrollbar/perfect-scrollbar.css',
        'plugins/jvectormap/jquery-jvectormap-2.0.1.css',
        'plugins/messenger/css/messenger.css',
        'plugins/messenger/css/messenger-theme-future.css',
        'plugins/messenger/css/messenger-theme-flat.css',
        'css/style.css',
        'css/responsive.css',
        'css/style_customize.css',
    ];
    public $js = [
        /* CORE JS FRAMEWORK - START */
        //'js/jquery-1.11.2.min.js',        
        'plugins/bootstrap/js/bootstrap.min.js',
        'plugins/pace/pace.min.js',
        'plugins/perfect-scrollbar/perfect-scrollbar.min.js',
        'plugins/viewport/viewportchecker.js',
        'plugins/switchery/dist/switchery.js',
        'plugins/messenger/js/messenger.min.js',       
        'plugins/messenger/js/messenger-theme-future.js',       
        'plugins/messenger/js/messenger-theme-flat.js',      
        'plugins/select2/select2.min.js',
        'plugins/select2/select2_locale_es.js',
        'plugins/inputmask/min/jquery.inputmask.bundle.min.js',
        'js/messenger.js',       
        'js/scripts.js',
        'js/propios.js',
        'js/chosen.jquery.min.js',
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
